﻿*** Keywords ***

Goto Afterpayment Editname and Address PRE_Production Shop_10 
      
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'แก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี')])
    click element       (//a[contains(.,'แก้ไขชื่อ และที่อยู่ในใบเสร็จรับเงิน/ใบกำกับภาษีเต็มรูป')])
    sleep       4s
    Select window           title=TRUE Retail Management System
    maximize browser window
    sleep   3s


Goto Afterpayment Brief to full recepit PRE_Production Shop_10 
      
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'แก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี')])
    click element       (//a[contains(.,'ออกใบกำกับภาษีเต็มรูป แทนอย่างย่อ')])
    sleep       4s
    Select window           title=TRUE Retail Management System
    maximize browser window
    sleep   3s



Goto Afterpayment Cancel recepit PRE_Production Shop_10 
      
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'ชำระค่าบริการ')])
    click element       (//a[contains(.,'ยกเลิกรายการชำระค่าบริการ')])
    sleep       4s
    Select window           title=TRUE Retail Management System
    maximize browser window
    sleep   3s



Goto Reprint recepit PRE_Production Shop_10 
      
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'แก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี')])
    click element       (//a[contains(.,'พิมพ์ใบเสร็จรับเงิน/ใบกำกับภาษีซ้ำ')])
    sleep       4s
    Select window           title=TRUE Retail Management System
    maximize browser window
    sleep   3s
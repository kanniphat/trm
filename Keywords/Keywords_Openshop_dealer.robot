﻿*** Keywords ***

Shop Dealer Page by Manager
      
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains         User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Manager}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Manager} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1
    Capture Page Screenshot

# -----------------------------------------------------------------------------------------------------------------------------------------------

Shop Dealer SD119 Page by Cashier
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains         User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Cashier}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Cashier} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1
    Capture Page Screenshot

# -----------------------------------------------------------------------------------------------------------------------------------------------

Close Shop Dealer Crossday by Manager
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains        User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Manager}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Manager} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1

    click element                    //div[@class='div-menu']//div[2]/div[.='การจัดการร้านShop Administration']
    Wait Until Element Is Visible    //a[.='ปิดร้าน']
    click element                    //a[.='ปิดร้าน']
    Wait Until Element Is Visible    //div[@class='modal is-active']//div[2]/input[@class='input']       10
    Selenium2Library.input text      //div[@class='modal is-active']//div[2]/input[@class='input']       ${User_Manager}   
    Selenium2Library.input text      //div[@class='modal is-active']//div[3]/input[@class='input']       ${Password_Manager}
    Click element                    //button[.='ตกลง'] 
    Wait Until Element Is Visible       ${Reason_Closeshop_crossday}   
    Click element                       ${Reason_Closeshop_crossday}
    Capture Page Screenshot
    click element                    //button[.='ปิดร้าน']  
    Wait Until Page Contains Element        ${OK_Button} 
    Click element                           ${OK_Button} 
    Sleep   1
    Capture Page Screenshot
    Click element                           ${Back_Button}



Close Shop Dealer Sameday by Manager
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains        User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Manager}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Manager} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1

    click element                    //div[@class='div-menu']//div[2]/div[.='การจัดการร้านShop Administration']
    Wait Until Element Is Visible    //a[.='ปิดร้าน']
    click element                    //a[.='ปิดร้าน']
    Wait Until Element Is Visible    //div[@class='modal is-active']//div[2]/input[@class='input']       10
    Selenium2Library.input text      //div[@class='modal is-active']//div[2]/input[@class='input']       ${User_Manager}   
    Selenium2Library.input text      //div[@class='modal is-active']//div[3]/input[@class='input']       ${Password_Manager}
    Click element                    //button[.='ตกลง'] 
    Wait Until Element Is Visible       ${Reason_Closeshop_Sameday}   
    Click element                       ${Reason_Closeshop_Sameday} 
    Capture Page Screenshot
    click element                    //button[.='ปิดร้าน']  
    Wait Until Page Contains Element        ${OK_Button} 
    Click element                           ${OK_Button} 
    Sleep   1
    Capture Page Screenshot
    Click element                           ${Back_Button}
# -----------------------------------------------------------------------------------------------------------------------------------------------

Open Shop Dealer by Manager
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains        User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Manager}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Manager} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1
    click element                    //div[@class='div-menu']//div[2]/div[.='การจัดการร้านShop Administration']
    Wait Until Element Is Visible    //a[.='เปิดร้าน']
    click element                    //a[.='เปิดร้าน']
    Wait Until Element Is Visible    //div[@class='modal is-active']//div[2]/input[@class='input']       10
    Selenium2Library.input text      //div[@class='modal is-active']//div[2]/input[@class='input']       ${User_Manager}   
    Selenium2Library.input text      //div[@class='modal is-active']//div[3]/input[@class='input']       ${Password_Manager}
    Click element                    //button[.='ตกลง'] 
    Wait Until Page Contains        ตรวจสอบงานประจำวันก่อนเปิดร้าน
    Capture Page Screenshot
    click element                           //button[.='เปิดร้าน']  
    Wait Until Page Contains Element        ${OK_Button} 
    Click element                           ${OK_Button} 
    Sleep   1
    Capture Page Screenshot
    Click element                           ${Back_Button}

# -----------------------------------------------------------------------------------------------------------------------------------------------   

Open Shitf by Cashier
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains        User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Cashier}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Cashier} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1
    Capture Page Screenshot
    Wait Until Page Contains                เงินทอน
    Selenium2Library.input text             ${Change_cash}          1000
    click element                           ${Save_Change_cash}
    Wait Until Page Contains Element        ${OK_Button} 
    Click element                           ${OK_Button} 
    sleep   1
    Capture Page Screenshot
    Click element                           ${Back_Button}

# -----------------------------------------------------------------------------------------------------------------------------------------------

Check Shop Dealer Still Opend 
    click element                    //div[@class='div-menu']//div[2]/div[.='การจัดการร้านShop Administration']
    Wait Until Element Is Visible    //a[.='ปิดร้าน']
    click element                    //a[.='ปิดร้าน']
    Wait Until Element Is Visible    //div[@class='modal is-active']//div[2]/input[@class='input']       10
    Selenium2Library.input text      //div[@class='modal is-active']//div[2]/input[@class='input']       ${User_Manager}   
    Selenium2Library.input text      //div[@class='modal is-active']//div[3]/input[@class='input']       ${Password_Manager}
    Click element                   //button[.='ตกลง'] 
    Wait Until Page Contains         ไม่เรียบร้อย           10
    Capture Page Screenshot 
    click element                   //button[@class='button is-dark']

# -----------------------------------------------------------------------------------------------------------------------------------------------

Send Cash Close Shitf by Cashier
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Capture Page Screenshot
    Click element                   //a[.='นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Selenium2Library.input text     //section[@class='modal-card-body']/div[2]/input[@class='input']        ${User_Cashier}  
    Selenium2Library.input text     //section[@class='modal-card-body']/div[3]/input[@class='input']        ${Password_Cashier}
    Click element                   //button[.='ตกลง'] 
    
    # กรณีใส่ user ของ Manager ในการส่งเงินตอนปิดกะ
    # Wait Until Page Contains        ท่านไม่สามารถเข้าสู่ระบบเพื่อนำส่งเงินได้ เนื่องจากไม่ได้รับสิทธิ์ในการนำส่งเงินตอนปิดกะ

    # กรณีแคชเชียร์ปิดร้านภายในวันที่เปิด
    # Wait Until Page Contains          ท่านไม่สามารถเข้าสู่ระบบได้ เนื่องจากท่านยังไม่ได้ปิดกะ 

    Wait Until Page Contains            นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Capture Page Screenshot
    Click element                       ${Verify_send_cash}
    ${Cash}             Get Value       ${Cash} 
    ${Credit_card}      Get Value       ${Credit_card}
    ${Cheque}           Get Value       ${Cheque}
    ${Voucher}          Get Value       ${Voucher}
    ${Tax}              Get Value       ${Tax}    
    ${Ohter}            Get Value       ${Ohter}

    Click element       ${Back_Button} 

    Wait Until Page Contains Element        //input[@id='recCashAmt']                   
    Selenium2Library.input text             //input[@id='recCashAmt']           ${Cash}   
    Selenium2Library.input text             //input[@id='recCreditAmt']         ${Credit_card}
    Selenium2Library.input text             //input[@id='recChequeAmt']         ${Cheque} 
    Selenium2Library.input text             //input[@id='recCouponAmt']         ${Voucher} 
    Selenium2Library.input text             //input[@id='recWhtAmt']            ${Tax} 
    Selenium2Library.input text             //input[@id='recOthersAmt']         ${Ohter}
    Click element                           ${Verify_send_cash}
    Click element                           ${Back_Button} 
    Wait Until Page Contains Element        ${Save_Button}
    Click element                           ${Save_Button}           
    Wait Until Page Contains Element        ${OK_Button} 
    Click element                           ${OK_Button} 
    Sleep   1
    Capture Page Screenshot
    Click element                           ${Back_Button}



Send Cash before Close Shitf by Cashier
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Capture Page Screenshot
    Click element                   //a[.='นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Selenium2Library.input text     //section[@class='modal-card-body']/div[2]/input[@class='input']        ${User_Cashier}  
    Selenium2Library.input text     //section[@class='modal-card-body']/div[3]/input[@class='input']        ${Password_Cashier}
    Click element                   //button[.='ตกลง'] 
    Wait Until Page Contains          กรุณาไปปิดกะที่เครื่องรับชำระของท่านก่อน              10s
    sleep   1
    Capture Page Screenshot
    Click element                   ${OK_Button}  

    
# -----------------------------------------------------------------------------------------------------------------------------------------------

Send Cash Close Shitf by Cashier Already
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Capture Page Screenshot
    Click element                   //a[.='นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Selenium2Library.input text     //section[@class='modal-card-body']/div[2]/input[@class='input']        ${User_Cashier}  
    Selenium2Library.input text     //section[@class='modal-card-body']/div[3]/input[@class='input']        ${Password_Cashier}
    Click element                   //button[.='ตกลง'] 

# -----------------------------------------------------------------------------------------------------------------------------------------------

Send Cash Endday by Manager
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)
    Capture Page Screenshot
    Click element                   //a[.='นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)']
    Wait Until Page Contains        นำส่งเงินตอนสิ้นวัน (โดยผู้จัดการร้าน)  
    Selenium2Library.input text     //section[@class='modal-card-body']/div[2]/input[@class='input']        ${User_Manager}   
    Selenium2Library.input text     //section[@class='modal-card-body']/div[3]/input[@class='input']        ${Password_Manager}
    Click element                   //button[.='ตกลง'] 

    Wait Until Page Contains            นำส่งเงินตอนสิ้นวัน (โดยผู้จัดการร้าน)
    Capture Page Screenshot
    Click element                       ${Verify_send_cash}
    ${Cash}             Get Value       ${Cash} 
    ${Credit_card}      Get Value       ${Credit_card}
    ${Cheque}           Get Value       ${Cheque}
    ${Voucher}          Get Value       ${Voucher}
    ${Tax}              Get Value       ${Tax}    
    ${Ohter}            Get Value       ${Ohter}

    Click element       ${Back_Button} 
    Wait Until Page Contains Element        //input[@id='recCashAmt']                   
    Selenium2Library.input text             //input[@id='recCashAmt']           ${Cash}   
    Selenium2Library.input text             //input[@id='recCreditAmt']         ${Credit_card}
    Selenium2Library.input text             //input[@id='recChequeAmt']         ${Cheque} 
    Selenium2Library.input text             //input[@id='recCouponAmt']         ${Voucher} 
    Selenium2Library.input text             //input[@id='recWhtAmt']            ${Tax} 
    Selenium2Library.input text             //input[@id='recOthersAmt']         ${Ohter}
    Click element                           ${Verify_send_cash}
    Click element                           ${Back_Button} 
    Wait Until Page Contains Element        ${Save_Button}
    Click element                           ${Save_Button}           
    Wait Until Page Contains Element        ${OK_Button} 
    Click element                           ${OK_Button} 
    Sleep   1
    Capture Page Screenshot
    Click element                           ${Back_Button}
  # -----------------------------------------------------------------------------------------------------------------------------------------------

Send Cash Endday by Manager Already
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)
    Capture Page Screenshot
    Click element                   //a[.='นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)']
    Wait Until Page Contains        นำส่งเงินตอนสิ้นวัน (โดยผู้จัดการร้าน)  
    Selenium2Library.input text     //section[@class='modal-card-body']/div[2]/input[@class='input']        ${User_Manager}   
    Selenium2Library.input text     //section[@class='modal-card-body']/div[3]/input[@class='input']        ${Password_Manager}
    Click element                   //button[.='ตกลง'] 
# -----------------------------------------------------------------------------------------------------------------------------------------------
   
Send Cash Close Shitf by Manager
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Capture Page Screenshot
    Click element                   //a[.='นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)']
    Wait Until Page Contains        นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)
    Selenium2Library.input text     //section[@class='modal-card-body']/div[2]/input[@class='input']        ${User_Manager}  
    Selenium2Library.input text     //section[@class='modal-card-body']/div[3]/input[@class='input']        ${Password_Manager}
    Click element                   //button[.='ตกลง'] 


# -----------------------------------------------------------------------------------------------------------------------------------------------

Send Cash Endday by Cashier
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)
    Capture Page Screenshot
    Click element                   //a[.='นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)']
    Wait Until Page Contains        นำส่งเงินตอนสิ้นวัน (โดยผู้จัดการร้าน)  
    Selenium2Library.input text     //section[@class='modal-card-body']/div[2]/input[@class='input']        ${User_Cashier}   
    Selenium2Library.input text     //section[@class='modal-card-body']/div[3]/input[@class='input']        ${Password_Cashier}
    Click element                   //button[.='ตกลง'] 

    # Wait Until Page Contains            นำส่งเงินตอนสิ้นวัน (โดยผู้จัดการร้าน)
    Capture Page Screenshot
    


# -----------------------------------------------------------------------------------------------------------------------------------------------

Close Shop Dealer by Cashier
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains        User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Manager}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Manager} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1

    click element                    //div[@class='div-menu']//div[2]/div[.='การจัดการร้านShop Administration']
    Wait Until Element Is Visible    //a[.='ปิดร้าน']
    click element                    //a[.='ปิดร้าน']
    Wait Until Element Is Visible    //div[@class='modal is-active']//div[2]/input[@class='input']       10
    Selenium2Library.input text      //div[@class='modal is-active']//div[2]/input[@class='input']       ${User_Cashier}   
    Selenium2Library.input text      //div[@class='modal is-active']//div[3]/input[@class='input']       ${Password_Cashier}
    Click element                    //button[.='ตกลง']    


Close Shitf by Cashier
    open browser    ${URL_Shop_Dealer}        ie
    maximize browser window          
    Wait Until Page Contains         User Login          10
    Selenium2Library.input text      //section[@class='modal-card-body']//div[1]/input[@class='input']      ${User_Cashier}      
    Selenium2Library.input text      //section[@class='modal-card-body']//div[2]/input[@class='input']      ${Password_Cashier} 
    click element                    //button[@class='button is-primary is-fullwidth']
    Wait Until Page Contains         Retail Management System        10
    sleep   1
    Capture Page Screenshot
    Wait Until Page Contains        การบริหารการเงิน
    Click element                   //div[@class='div-menu']//div[@class='D-5']/div[3]/div[.='การบริหารการเงินCash Management']
    Wait Until Page Contains        ปิดกะ
    Capture Page Screenshot
    Click element                  ${closeshift_Button}
    Wait Until Element Is Visible    //div[@class='modal is-active']//div[2]/input[@class='input']       10
    Selenium2Library.input text      //div[@class='modal is-active']//div[2]/input[@class='input']       ${User_Cashier}   
    Selenium2Library.input text      //div[@class='modal is-active']//div[3]/input[@class='input']       ${Password_Cashier}
    Click element                    //button[.='ตกลง']
    Wait Until Page Contains Element        ${OK_Button} 
    Click element                           ${OK_Button} 
    Sleep   1
    Capture Page Screenshot
    Click element                           ${Back_Button} 




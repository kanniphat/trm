﻿*** Keywords ***


Goto Sales PRE_Production Shop_10      
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'ขายสินค้า')])[1] 
    click element       (//a[contains(.,'ขายสินค้า')])[2]
    sleep   2s
    SikuliLibrary.Press Special Key           F5
    sleep   1
    Select window           title=TRM-SALE
    maximize browser window
    sleep   3s



Goto Cancel Sales PRE_Production Shop_10      
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'ขายสินค้า')])[1] 
    click element       (//a[contains(.,'ยกเลิกเอกสารเกี่ยวกับการขายสินค้า')])
    sleep   2s
    SikuliLibrary.Press Special Key           F5
    sleep   1
    Select window           title=TRM-AFTER SALE
    maximize browser window
    sleep   3s

Goto change pin PRE_Production Shop_10 
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'ขายสินค้า')])[1] 
    click element       (//a[contains(.,'เปลี่ยนสินค้า')])
    sleep   2s
    SikuliLibrary.Press Special Key           F5
    sleep   1
    Select window           title=TRM-AFTER SALE
    maximize browser window
    sleep   3s


Goto change pin PRE_Production Shop_10 Chrome
    open browser    ${URL}          gc
    maximize browser window
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     tsmtrn010003
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                20s
    click image     css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    click element   css=div.z-toolbarbutton[title="Payment"]
    Wait Until Element Is Visible       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    Wait Until Element Is Visible       (//a[contains(.,'ขายสินค้า')])[1] 
    click element       (//a[contains(.,'ขายสินค้า')])[1] 
    Wait Until Element Is Visible       (//a[contains(.,'เปลี่ยนสินค้า')])
    click element       (//a[contains(.,'เปลี่ยนสินค้า')])
    sleep   3
    # SikuliLibrary.Press Special Key           F5
    # sleep   1
    Select window           title=TRM-AFTER SALE
    maximize browser window
    sleep   4

# Goto Auto Receipt Chrome
#   open browser    http://sff-uat.true.th:18771/trm-autoreceipt-web/?shop_code=80000000&queue_id=_&channel=INTRANET          gc
#   maximize browser window
#   Wait Until Element Is Visible         xpath=id("moreInfoContainer")/a[1] 
#   click element                         xpath=id("moreInfoContainer")/a[1]
#   Wait Until Element Is Visible         id=overridelink
#   click element                         id=overridelink
#   Wait Until Element Is Visible    id=IDToken1        10s   
#   Selenium2Library.input text      id=IDToken1     paweena_has
#   Selenium2Library.input text      id=IDToken2     P@ssw0rd    
#   click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
#   sleep   2





# Goto Auto Receipt 
#     open browser    ${URL}          ie
#     maximize browser window
#     click element   xpath=id("moreInfoContainer")/a[1]
#     click element   id=overridelink
#     Wait Until Element Is Visible    id=IDToken1        10s   
#     Selenium2Library.input text      id=IDToken1     paweena_has
#     Selenium2Library.input text      id=IDToken2     P@ssw0rd    
#     click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
#     Wait Until Page Contains        Shop                20s
#     click image     css=div.z-toolbarbutton-cnt > img
#     Wait Until Page Contains     Information            20s
#     click element   css=div.z-toolbarbutton[title="Payment"]
#     click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
#     click element       (//a[contains(.,'ชำระค่าบริการ')])
#     click element       (//a[contains(.,'ชำระค่าบริการ Auto Receipt')])
#     sleep   2s
#     # click       ${pic_more_button} 
#     # click       ${not_recom_button}
#     # click       ${expand_button} 
#     # sleep   5s
#     # Select window           title= https://sff-uat.true.th:12443/?shop_code=80000000&queue_id=_&channel=INTRANET - Autoreceipt - Internet Explorer
#     # sleep   5s


Goto Auto Receipt 
    open browser    https://sff-uat.true.th:12443/trm-autoreceipt-web/?shop_code=80000000&queue_id=_&channel=INTRANET          ie
    maximize browser window
    Wait Until Element Is Visible           xpath=id("moreInfoContainer")/a[1]
    click element                           xpath=id("moreInfoContainer")/a[1]
    Wait Until Element Is Visible           id=overridelink
    click element                           id=overridelink
    Wait Until Element Is Visible           xpath=id("moreInfoContainer")/a[1]
    click element   xpath=id("moreInfoContainer")/a[1]
    Wait Until Element Is Visible           id=overridelink             10s
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1        10s   
    Selenium2Library.input text      id=IDToken1     paweena_has
    Selenium2Library.input text      id=IDToken2     P@ssw0rd    
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    select window       title= Autoreceipt
    sleep   3
    
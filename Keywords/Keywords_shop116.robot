﻿*** Keywords ***
Goto Shop 116 by Manager      
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1     10s   
    Selenium2Library.input text      id=IDToken1     tsme2e7
    click element                    id=IDToken2   
    Clear Element Text               id=IDToken2   
    sleep   1
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN     
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                10s

    Wait Until Element Is Visible       //i[@class='z-combobox-btn'] 
    click element       //i[@class='z-combobox-btn']       
    sleep   1
    click element        //td[contains(.,'80000116')]
    click image          css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\top.png
    
# ---------------------------------------------------------------------------------------------------------------------------------------  

Allocate Cash for Change Ratthanasuda for Close 
    Select window           title=TSM-Lite
    Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]     20s
    Click element                              css=div.z-toolbarbutton[title="Cash"]
    SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

    Execute JavaScript    window.scrollTo(0, 1500)
    click                 C:\\TRM\\sikulipicture\\Allocate.png
  
    sleep    4
    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible                //div[@class="wrap-form-input"]//input[@name="8000011206refund_amount"]         
    Click Element                                //div[@class="wrap-form-input"]//input[@name="8000011206refund_amount"]
    
    Wait Until Element Is Visible               //button[@class="main__button-styles-ok___2y3Js"]                       
    Click Element                               //button[@class="main__button-styles-ok___2y3Js"]                       

    sleep       4
    Select window           title=This site isn’t secure 
    Maximize Browser Window

    Wait Until Element Is Visible       //span[@id="moreInfoContainer"]
    click element                       //span[@id="moreInfoContainer"]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1     10s   
    Selenium2Library.input text      id=IDToken1     trmpay18
    click element                    id=IDToken2   
    Clear Element Text               id=IDToken2   
    sleep   1
    Selenium2Library.input text      id=IDToken2     P@ssw0rdTRM    
    click element       xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    sleep   4            
    click               C:\\TRM\\sikulipicture\\Approve.png
    sleep   1 
    click               C:\\TRM\\sikulipicture\\ok.png

# ---------------------------------------------------------------------------------------------------------------------------------------   

Allocate Cash for Change Jithaporn for Close
    # Select window           title=TSM-Lite
    # Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]     20s
    # Click element                css=div.z-toolbarbutton[title="Cash"]
    # SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

    # Execute JavaScript    window.scrollTo(0, 1500)
    # click                 C:\\TRM\\sikulipicture\\Allocate.png
  
    # sleep    4


    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible                //div[@class="wrap-form-input"]//input[@name="EM0007refund_amount"]            20    
    Click Element                                //div[@class="wrap-form-input"]//input[@name="EM0007refund_amount"]
    
    Wait Until Element Is Visible               //button[@class="main__button-styles-ok___2y3Js"]                       
    Click Element                               //button[@class="main__button-styles-ok___2y3Js"]                       

    sleep       4
    Select window           title=This site isn’t secure 
    Maximize Browser Window

    Wait Until Element Is Visible       //span[@id="moreInfoContainer"]
    click element                       //span[@id="moreInfoContainer"]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1     10s   
    Selenium2Library.input text      id=IDToken1     tsme2e7
    click element                    id=IDToken2   
    Clear Element Text               id=IDToken2   
    sleep   1
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element       xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    sleep   4            
    click               C:\\TRM\\sikulipicture\\Approve.png
    sleep   1 
    click               C:\\TRM\\sikulipicture\\ok.png

# ---------------------------------------------------------------------------------------------------------------------------------------  

End shift Ratthanasuda
        Select window           title=TSM-Lite  
        Wait Until Element Is Visible           css=div.z-toolbarbutton[title="Cash"]               20s
        Click element                           css=div.z-toolbarbutton[title="Cash"]
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

        Execute JavaScript    window.scrollTo(0, 1500)
        click                 C:\\TRM\\sikulipicture\\endshift.png
        sleep    4
        Select window           title=Cash Management
        Maximize Browser Window

        Wait Until Element Is Visible           //input[@class="main__text-box___6mmfX text-center"]            20s
        Selenium2Library.Input text             //input[@class="main__text-box___6mmfX text-center"]             8000011206
        Click element                           //button[@class="main__button-styles-view___1Oqq2"]
        sleep    2
        Click element                           //button[@class="main__button-styles-approve___1RqFF"]
        sleep   1 
        click               C:\\TRM\\sikulipicture\\ok.png

# --------------------------------------------------------------------------------------------------------------------------------------- 

End shift Jithaporn  
        Select window           title=TSM-Lite      
        Wait Until Element Is Visible           css=div.z-toolbarbutton[title="Cash"]               20s
        Click element                           css=div.z-toolbarbutton[title="Cash"]
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

        Execute JavaScript    window.scrollTo(0, 1500)
        click                 C:\\TRM\\sikulipicture\\endshift.png
        sleep    4
        Select window           title=Cash Management
        Maximize Browser Window

        Wait Until Element Is Visible           //input[@class="main__text-box___6mmfX text-center"]            20s
        Selenium2Library.Input text             //input[@class="main__text-box___6mmfX text-center"]            EM0007
        Click element                           //button[@class="main__button-styles-view___1Oqq2"]
        sleep    2
        Click element                           //button[@class="main__button-styles-approve___1RqFF"]
        sleep   1 
        click               C:\\TRM\\sikulipicture\\ok.png

# --------------------------------------------------------------------------------------------------------------------------------------- 

End Day Reconcile
        Select window           title=TSM-Lite      
        Wait Until Element Is Visible           css=div.z-toolbarbutton[title="Cash"]               10s
        Click element                           css=div.z-toolbarbutton[title="Cash"]
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

        Execute JavaScript    window.scrollTo(0, 1500)
        click                 C:\\TRM\\sikulipicture\\enddayrecon.png
        sleep    4
        Select window           title=Cash Management
        Maximize Browser Window

        Wait Until Element Is Visible       ${verify}           10s
        click element                       ${verify}  
        Select window           title=Cash Management
        Maximize Browser Window
        Wait Until Element Is Visible       ${cash_fromsystem_endday}             10s
        Wait Until Element Is Visible       ${credit_TDS_fromsystem}      10s
        Wait Until Element Is Visible       ${credit_TDS_quantity}            10s

        ${cash_fromsystem_endday}              Selenium2Library.Get Text        ${cash_fromsystem_endday}  
        ${cash_fromsystem_endday}              Remove String                    ${cash_fromsystem_endday}      ,
        ${cash_fromsystem_endday}              Convert To Number                ${cash_fromsystem_endday}  
        ${cash_fromsystem_endday}              Evaluate      "%.2f" % ${cash_fromsystem_endday} 
        
        ${credit_TDS_fromsystem_endday}       Selenium2Library.Get Text        ${credit_TDS_fromsystem_endday} 
        ${credit_TDS_fromsystem_endday}       Remove String                    ${credit_TDS_fromsystem_endday}     ,
        ${credit_TDS_fromsystem_endday}       Convert To Number                ${credit_TDS_fromsystem_endday} 
        ${credit_TDS_fromsystem_endday}       Evaluate      "%.2f" % ${credit_TDS_fromsystem_endday} 

        ${credit_TDS_quantity_endday}       Selenium2Library.Get Text        ${credit_TDS_quantity_endday}
        ${credit_TDS_quantity_endday}       Convert To Number                ${credit_TDS_quantity_endday}
        ${credit_TDS_quantity_endday}       Evaluate      "%.0f" % ${credit_TDS_quantity_endday}

        
        ${credit_TMN_fromsystem_endday}       Selenium2Library.Get Text        ${credit_TMN_fromsystem_endday} 
        ${credit_TMN_fromsystem_endday}       Remove String                    ${credit_TMN_fromsystem_endday}     ,
        ${credit_TMN_fromsystem_endday}       Convert To Number                ${credit_TMN_fromsystem_endday} 
        ${credit_TMN_fromsystem_endday}       Evaluate      "%.2f" % ${credit_TMN_fromsystem_endday} 

        ${credit_TMN_quantity_endday}       Selenium2Library.Get Text        ${credit_TMN_quantity_endday}
        ${credit_TMN_quantity_endday}       Convert To Number                ${credit_TMN_quantity_endday}
        ${credit_TMN_quantity_endday}       Evaluate      "%.0f" % ${credit_TMN_quantity_endday}




        
        Wait Until Element Is Visible           ${back_button} 
        click element                           ${back_button} 
        Wait Until Element Is Visible           ${onebaht}
        Clear Element Text                      ${onebaht}          
        Selenium2Library.input text             ${onebaht}          ${cash_fromsystem_endday} 
        
        Wait Until Element Is Visible           ${credit_confirm_tab}   
        click element                           ${credit_confirm_tab}
        Wait Until Element Is Visible           ${TDS_quantity}
        Selenium2Library.input text             ${TDS_quantity}            ${credit_TDS_quantity_endday} 

        Wait Until Element Is Visible           ${TDS_amount}  
        Selenium2Library.input text             ${TDS_amount}              ${credit_TDS_fromsystem_endday} 

        Wait Until Element Is Visible           ${TMN_quantity}
        Selenium2Library.input text             ${TMN_quantity}            ${credit_TMN_quantity_endday} 

        Wait Until Element Is Visible           ${TMN_amount}  
        Selenium2Library.input text             ${TMN_amount}              ${credit_TMN_fromsystem_endday} 



        Wait Until Element Is Visible           ${Reconcile_button} 
        click element                           ${Reconcile_button}    
        Wait Until Element Is Visible           ${ok}
        click element                           ${ok}  
        sleep   2 
        Wait Until Element Is Visible           ${ok}
        click element                           ${ok} 
        Wait Until Page Contains                End Day Reconcile        10s



# ---------------------------------------------------------------------------------------------------------------------------------------  

Close Shop
       
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\top.png 
        Select window           title=TSM-Lite
        Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]               10s
        Click element                              css=div.z-toolbarbutton[title="Cash"]
        sleep    2
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\ShopManagement.png
    
        sleep    1
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\openshop.png
        sleep    2
        click                                      C:\\TRM\\sikulipicture\\closeshop.png
        sleep    4
        Select window           title=Cash Management
        Maximize Browser Window

        Wait Until Element Is Visible           //button[@class="main__button-styles-approve___1RqFF font-weight-bold"]           20 
        Click element                           //button[@class="main__button-styles-approve___1RqFF font-weight-bold"]
        sleep   1 
        click               C:\\TRM\\sikulipicture\\ok.png

# ---------------------------------------------------------------------------------------------------------------------------------------      
            
Open Shop
        Select window           title=TSM-Lite
        Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]               10s
        Click element                              css=div.z-toolbarbutton[title="Cash"]
        sleep    2
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\ShopManagement.png
    
        sleep    3
        SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\openshop.png
        click                                      C:\\TRM\\sikulipicture\\openshop_modernize.png
        sleep    4
        Select window           title=Cash Management
        Maximize Browser Window

        Wait Until Element Is Visible           //input[@id="pettyCashForm"]          20 
        Selenium2Library.Input text             //input[@id="pettyCashForm"]          6000
       
        Wait Until Element Is Visible           //input[@id="floatingCashForm"]       20 
        Selenium2Library.Input text             //input[@id="floatingCashForm"]       40000

        Wait Until Element Is Visible           //button[@class="main__button-styles-approve___1RqFF font-weight-bold"]         10s
        Click element                           //button[@class="main__button-styles-approve___1RqFF font-weight-bold"]
        
        sleep   1 
        click               C:\\TRM\\sikulipicture\\ok.png

# --------------------------------------------------------------------------------------------------------------------------------------- 

Allocate Cash for Change Ratthanasuda for Open
    Select window           title=TSM-Lite 
    Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]     10s
    Click element                              css=div.z-toolbarbutton[title="Cash"]
    SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

    Execute JavaScript    window.scrollTo(0, 1500)
    click                 C:\\TRM\\sikulipicture\\Allocate.png
  
    sleep    4
    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible                //input[@id="8000011206withdraw_amount"]        
    Click Element                                //input[@id="8000011206withdraw_amount"]
    
    Wait Until Element Is Visible               //input[@id="modalAmount"]              10s
    Selenium2Library.input text                 //input[@id="modalAmount"]              1000

    Wait Until Element Is Visible               //button[@class="main__button-styles-ok___2y3Js"]                       
    Click Element                               //button[@class="main__button-styles-ok___2y3Js"]                       

    sleep       4
    Select window           title=This site isn’t secure 
    Maximize Browser Window

    Wait Until Element Is Visible       //span[@id="moreInfoContainer"]
    click element                       //span[@id="moreInfoContainer"]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1     10s   
    Selenium2Library.input text      id=IDToken1     trmpay18
    click element                    id=IDToken2   
    Clear Element Text               id=IDToken2   
    sleep   1
    Selenium2Library.input text      id=IDToken2     P@ssw0rdTRM    
    click element       xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    sleep   4            
    click                   C:\\TRM\\sikulipicture\\Approve.png
    click                   C:\\TRM\\sikulipicture\\Approve.png               

    sleep   1 
    click               C:\\TRM\\sikulipicture\\ok.png

# ---------------------------------------------------------------------------------------------------------------------------------------  

Allocate Cash for Change Jithaporn for Open 
    Select window           title=TSM-Lite
    Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]     10s
    Click element                              css=div.z-toolbarbutton[title="Cash"]
    SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

    Execute JavaScript    window.scrollTo(0, 1500)
    click                 C:\\TRM\\sikulipicture\\Allocate.png
  
    sleep    4
    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible                   //input[@id="EM0007withdraw_amount"]        
    Click Element                                   //input[@id="EM0007withdraw_amount"]

    Wait Until Element Is Visible               //input[@id="modalAmount"]              10s
    Selenium2Library.input text                 //input[@id="modalAmount"]              1000
    
    Wait Until Element Is Visible               //button[@class="main__button-styles-ok___2y3Js"]                       
    Click Element                               //button[@class="main__button-styles-ok___2y3Js"]                       

    sleep       4
    Select window           title=This site isn’t secure 
    Maximize Browser Window

    Wait Until Element Is Visible       //span[@id="moreInfoContainer"]
    click element                       //span[@id="moreInfoContainer"]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1     10s   
    Selenium2Library.input text      id=IDToken1     tsme2e7
    click element                    id=IDToken2   
    Clear Element Text               id=IDToken2   
    sleep   1
    Selenium2Library.input text      id=IDToken2     P@$$w0rdTRN    
    click element       xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    sleep   4            
    click               C:\\TRM\\sikulipicture\\Approve.png
    sleep   1 
    click               C:\\TRM\\sikulipicture\\ok.png

# --------------------------------------------------------------------------------------------------------------------------------------- 

Confirm Reconcile Ratanasuda
    Select window           title=TSM-Lite
    Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]     10s
    Click element                              css=div.z-toolbarbutton[title="Cash"]
    SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

    Execute JavaScript    window.scrollTo(0, 1500)
    click                 C:\\TRM\\sikulipicture\\confirmreconcile.png
    sleep    3
    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible       ${User_Ratanasuda}  10s
    click element                       ${User_Ratanasuda}
    Wait Until Element Is Visible       ${onebaht}
    Selenium2Library.input text         ${onebaht}          1
    Wait Until Element Is Visible       ${verify}           10s
    click element                       ${verify}  
    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible       ${cash_fromsystem}            10s
    Wait Until Element Is Visible       ${credit_TDS_fromsystem}      10s
    Wait Until Element Is Visible       ${credit_TDS_quantity}        10s

    ${cash_fromsystem}             Selenium2Library.Get Text        ${cash_fromsystem} 
    ${cash_fromsystem}             Remove String                    ${cash_fromsystem}     ,
    ${cash_fromsystem}             Convert To Number                ${cash_fromsystem} 
    ${cash_fromsystem}             Evaluate      "%.2f" % ${cash_fromsystem}
    
    ${credit_TDS_fromsystem}       Selenium2Library.Get Text        ${credit_TDS_fromsystem}
    ${credit_TDS_fromsystem}       Remove String                    ${credit_TDS_fromsystem}    ,
    ${credit_TDS_fromsystem}       Convert To Number                ${credit_TDS_fromsystem} 
    ${credit_TDS_fromsystem}       Evaluate      "%.2f" % ${credit_TDS_fromsystem}

    ${credit_TDS_quantity}        Selenium2Library.Get Text        ${credit_TDS_quantity} 
    ${credit_TDS_quantity}        Convert To Number                ${credit_TDS_quantity} 
    ${credit_TDS_quantity}        Evaluate      "%.0f" % ${credit_TDS_quantity}

    # ---------------------------------------------TMN------------------------------------------------------------ 
    ${credit_TMN_fromsystem}       Selenium2Library.Get Text        ${credit_TMN_fromsystem}
    ${credit_TMN_fromsystem}       Remove String                    ${credit_TMN_fromsystem}    ,
    ${credit_TMN_fromsystem}       Convert To Number                ${credit_TMN_fromsystem} 
    ${credit_TMN_fromsystem}       Evaluate      "%.2f" % ${credit_TMN_fromsystem}

    ${credit_TMN_quantity}        Selenium2Library.Get Text        ${credit_TMN_quantity} 
    ${credit_TMN_quantity}        Convert To Number                ${credit_TMN_quantity} 
    ${credit_TMN_quantity}        Evaluate      "%.0f" % ${credit_TMN_quantity} 
    # ------------------------------------------------------------------------------------------------------------- 
    
    Wait Until Element Is Visible           ${back_button} 
    click element                           ${back_button} 
    Wait Until Element Is Visible           ${onebaht}
    Clear Element Text                      ${onebaht} 
    Selenium2Library.input text             ${onebaht}          ${cash_fromsystem} 
    
    Wait Until Element Is Visible           ${credit_confirm_tab}   
    click element                           ${credit_confirm_tab}

    Wait Until Element Is Visible           ${TDS_quantity}
    Selenium2Library.input text             ${TDS_quantity}             ${credit_TDS_quantity} 

    Wait Until Element Is Visible           ${TDS_amount}  
    Selenium2Library.input text             ${TDS_amount}               ${credit_TDS_fromsystem}

    Wait Until Element Is Visible           ${TMN_quantity}
    Selenium2Library.input text             ${TMN_quantity}             ${credit_TMN_quantity} 

    Wait Until Element Is Visible           ${TMN_amount}  
    Selenium2Library.input text             ${TMN_amount}               ${credit_TMN_fromsystem}


    Wait Until Element Is Visible           ${back_button}          
    Wait Until Element Is Visible           ${confirm_send}
    click element                           ${confirm_send} 	   
    Wait Until Element Is Visible           ${ok}
    click element                           ${ok}  
    sleep   2  
    Wait Until Element Is Visible           ${ok}
    click element                           ${ok} 
    Wait Until Element Is Visible           ${back_button} 
    click element                           ${back_button} 
    Wait Until Page Contains                Search Employee             10s

# ---------------------------------------------------------------------------------------------------------------------------------

Confirm Reconcile Jittaporn

# ------------------------------------- กรณีทำ Confirm ต่อเนื่องต้อง COMMENT CODE นี้ไว้ -----------------------------------------------
    # Select window           title=TSM-Lite
    # Wait Until Element Is Visible              css=div.z-toolbarbutton[title="Cash"]     10s
    # Click element                              css=div.z-toolbarbutton[title="Cash"]
    # SikuliLibrary.Mouse Move                   C:\\TRM\\sikulipicture\\CashManagement.png

    # Execute JavaScript    window.scrollTo(0, 1500)
    # click                 C:\\TRM\\sikulipicture\\confirmreconcile.png
    # sleep    3
# ---------------------------------------------------------------------------------------------------------


    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible       ${User_Jittaporn}  10s
    click element                       ${User_Jittaporn}
    Wait Until Element Is Visible       ${onebaht}
    Selenium2Library.input text         ${onebaht}          1
    Wait Until Element Is Visible       ${verify}           10s
    click element                       ${verify}  
    Select window           title=Cash Management
    Maximize Browser Window
    Wait Until Element Is Visible       ${cash_fromsystem}            10s
    Wait Until Element Is Visible       ${credit_TDS_fromsystem}      10s
    Wait Until Element Is Visible       ${credit_TDS_quantity}            10s

    ${cash_fromsystem}             Selenium2Library.Get Text        ${cash_fromsystem} 
    ${cash_fromsystem}             Remove String                    ${cash_fromsystem}     ,
    ${cash_fromsystem}             Convert To Number                ${cash_fromsystem} 
    ${cash_fromsystem}             Evaluate      "%.2f" % ${cash_fromsystem}
    
    ${credit_TDS_fromsystem}       Selenium2Library.Get Text        ${credit_TDS_fromsystem}
    ${credit_TDS_fromsystem}       Remove String                    ${credit_TDS_fromsystem}    ,
    ${credit_TDS_fromsystem}       Convert To Number                ${credit_TDS_fromsystem} 
    ${credit_TDS_fromsystem}       Evaluate      "%.2f" % ${credit_TDS_fromsystem}

    ${credit_TDS_quantity}       Selenium2Library.Get Text        ${credit_TDS_quantity}
    ${credit_TDS_quantity}       Convert To Number                ${credit_TDS_quantity}
    ${credit_TDS_quantity}       Evaluate      "%.0f" % ${credit_TDS_quantity}
    

    # ---------------------------------------------TMN------------------------------------------------------------ 
    ${credit_TMN_fromsystem}       Selenium2Library.Get Text        ${credit_TMN_fromsystem}
    ${credit_TMN_fromsystem}       Remove String                    ${credit_TMN_fromsystem}    ,
    ${credit_TMN_fromsystem}       Convert To Number                ${credit_TMN_fromsystem} 
    ${credit_TMN_fromsystem}       Evaluate      "%.2f" % ${credit_TMN_fromsystem}

    ${credit_TMN_quantity}        Selenium2Library.Get Text        ${credit_TMN_quantity} 
    ${credit_TMN_quantity}        Convert To Number                ${credit_TMN_quantity} 
    ${credit_TMN_quantity}        Evaluate      "%.0f" % ${credit_TMN_quantity} 
    # ------------------------------------------------------------------------------------------------------------- 
    Wait Until Element Is Visible           ${back_button} 
    click element                           ${back_button} 
    Wait Until Element Is Visible           ${onebaht}
    Clear Element Text                      ${onebaht} 
    Selenium2Library.input text             ${onebaht}          ${cash_fromsystem} 
    
    Wait Until Element Is Visible           ${credit_confirm_tab}   
    click element                           ${credit_confirm_tab}

    Wait Until Element Is Visible           ${TDS_quantity}
    Selenium2Library.input text             ${TDS_quantity}             ${credit_TDS_quantity} 

    Wait Until Element Is Visible           ${TDS_amount}  
    Selenium2Library.input text             ${TDS_amount}               ${credit_TDS_fromsystem}

    Wait Until Element Is Visible           ${TMN_quantity}
    Selenium2Library.input text             ${TMN_quantity}             ${credit_TMN_quantity} 

    Wait Until Element Is Visible           ${TMN_amount}  
    Selenium2Library.input text             ${TMN_amount}               ${credit_TMN_fromsystem}


    Wait Until Element Is Visible           ${back_button}          
    Wait Until Element Is Visible           ${confirm_send}
    click element                           ${confirm_send} 	   
    Wait Until Element Is Visible           ${ok}
    click element                           ${ok}  
    sleep   2  
    Wait Until Element Is Visible           ${ok}
    click element                           ${ok} 
    Wait Until Element Is Visible           ${back_button} 
    click element                           ${back_button} 
    Wait Until Page Contains                Search Employee             10s
# ---------------------------------------------------------------------------------------------------------------------------------
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase verify add 2bill 1 receipt
    Goto Bill Payment Page PRE_Production Shop_10                
#  --------------------------------------------------------add bill Page 1-----------------------------------------------------------------  

    click element                       //a[@class='btn btn-trmadd ']
    # click element                       css=div.modal-table tr:nth-of-type(2) .table-radio
    click element                       //div[@class='table-radio']
    selenium2Library.input text         css=[name='card']                 1103700432678
    click element                       css=.Select-placeholder
   
    FOR    ${index}    IN RANGE    23
           Press Special Key        C_DOWN
    END
    
    Press Special Key           	    ENTER
    sleep       2s
    click element                       //button[@class='btn-submit full']
# --------------------------------------------------------------------------------------------------------------------------------------    


# #  --------------------------------------------------------add bill Page 2 edit customer------------------------------------------------   
    
    click element                       css=.Select-control
    Press Special Key           	    C_DOWN   
    Press Special Key           	    ENTER
    
    Selenium2Library.input text         css=input[maxlength='255']              กำพล ทองเอก
    
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='.']                    47904
    Selenium2Library.input text         css=input[maxlength='55']               108/45
    Selenium2Library.input text         css=input[maxlength='30']               27
    Selenium2Library.input text         css=input[maxlength='60']               รุ่งกิจวิลลา
    Selenium2Library.input text         css=input[maxlength='110']              ใบหยก
    Selenium2Library.input text         css=div.floor .form-input               5
    Selenium2Library.input text         css=div.room_no .form-input             687
    Selenium2Library.input text         css=div.soi .form-input                 98
    Selenium2Library.input text         css=div.street .form-input              คนเดิน
    click element                       css=div.all-input-wrapper div:nth-of-type(1) > .form-value
    Selenium2Library.input text         //input[@class='form-control']          เชียงใหม่
    sleep           3s
    Press Special Key           	    C_DOWN
    Press Special Key           	    C_DOWN
    Press Special Key           	    ENTER
    click Button                       //button[@class='btn-submit full']
    sleep               2s


#  --------------------------------------------------------add bill Page 3 ----------------------------------------------- 
    click element                       css=.form-select
    FOR    ${index}    IN RANGE    16
           Press Special Key        C_DOWN
    END  
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002067                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         123456678
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             02/03/2019 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               31/03/2019 
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          200
    click Button                       //button[@class='btn-submit full']


# -------------------------------------------------------------add another bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder
    
    FOR    ${index}    IN RANGE    23
           Press Special Key        C_DOWN
    END
    
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']

#  --------------------------------------------------------add bill Page 3.1 ----------------------------------------------- 
    click element                       css=.form-select
    FOR    ${index}    IN RANGE    14
           Press Special Key        C_DOWN
    END  

    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002052                                 
    Selenium2Library.input text         css=div.select-nopadding .form-input                    NF0190032I
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         123456678
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             02/03/2019 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               31/03/2019 
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          500
    click Button                       //button[@class='btn-submit full']

# --------------------------------------------------edit bill + print -----------------------------------------------------------------------------                   

   
    Selenium2Library.input text         ${editcashbill}             900
    Selenium2Library.input text         ${editcashbill2}            500

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       5s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    sleep   4s
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    

# ------------------------------------------------------------------------------------------------------------------------------------
 
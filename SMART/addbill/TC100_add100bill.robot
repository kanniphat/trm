﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         Dialogs

Test Teardown       Run Keyword If Test Failed      Pause Execution


Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

                
*** Variables ***
${BAN}  200051632



*** Test Cases ***


# *************************************************************************************************************************************
Add 1    
    Goto Bill Payment Page Production Shop_10             
#  --------------------------------------------------------add bill Page 1-----------------------------------------------------------------  

    click element                       //a[@class='btn btn-trmadd ']
    click element                       //div[@class='table-radio']
    selenium2Library.input text         css=[name='card']                 1103700432678
    click element                       css=.Select-placeholder
    Loop add bill select TU  
    Press Special Key           	    ENTER
    sleep       2s
    click element                       //button[@class='btn-submit full']
# --------------------------------------------------------------------------------------------------------------------------------------    


# #  --------------------------------------------------------add bill Page 2 edit customer------------------------------------------------   
    
    click element                       css=.Select-control
    Press Special Key           	    C_DOWN   
    Press Special Key           	    ENTER
    
    Selenium2Library.input text         css=input[maxlength='255']              กำพล ทองเอก
    
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='.']                    47904
    Selenium2Library.input text         css=input[maxlength='55']               108/45
    Selenium2Library.input text         css=input[maxlength='30']               27
    Selenium2Library.input text         css=input[maxlength='60']               รุ่งกิจวิลลา
    Selenium2Library.input text         css=input[maxlength='110']              ใบหยก
    Selenium2Library.input text         css=div.floor .form-input               5
    Selenium2Library.input text         css=div.room_no .form-input             687
    Selenium2Library.input text         css=div.soi .form-input                 98
    Selenium2Library.input text         css=div.street .form-input              คนเดิน
    click element                       css=div.all-input-wrapper div:nth-of-type(1) > .form-value
    Selenium2Library.input text         //input[@class='form-control']          เชียงใหม่
    sleep           3s
    Press Special Key           	    C_DOWN
    Press Special Key           	    C_DOWN
    Press Special Key           	    ENTER
    click Button                       //button[@class='btn-submit full']
    sleep               2s


#  --------------------------------------------------------add bill Page 3 ----------------------------------------------- 
  
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                 
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         1
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          1
    click Button                       //button[@class='btn-submit full']


Add 2 
# -------------------------------------------------------------add bill 2---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         2
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          2
    click Button                       //button[@class='btn-submit full']


Add 3 
# -------------------------------------------------------------add bill 3---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full'] 
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         3
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          3
    click Button                       //button[@class='btn-submit full']
  

Add 4   
# -------------------------------------------------------------add bill 4---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         4
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          4
    click Button                       //button[@class='btn-submit full']


Add 5 
# -------------------------------------------------------------add bill 5---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         5
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          5
    click Button                       //button[@class='btn-submit full']



Add 6 
# -------------------------------------------------------------add bill 6---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         6
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          6
    click Button                       //button[@class='btn-submit full']


Add 7 
# -------------------------------------------------------------add bill 7---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         7
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          7
    click Button                       //button[@class='btn-submit full']


Add 8 
# -------------------------------------------------------------add bill 8---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         8
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          8
    click Button                       //button[@class='btn-submit full']

Add 9 
# -------------------------------------------------------------add bill 9---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         9
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          9
    click Button                       //button[@class='btn-submit full']

   
Add 10 
# -------------------------------------------------------------add bill 10---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input        10
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          10
    click Button                       //button[@class='btn-submit full']

Add 11 
# -------------------------------------------------------------add bill 11---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         11
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          11
    click Button                       //button[@class='btn-submit full']

Add 12 
# -------------------------------------------------------------add bill 12---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         12
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          12
    click Button                       //button[@class='btn-submit full']


Add 13 
# -------------------------------------------------------------add bill 13 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         13
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          13
    click Button                       //button[@class='btn-submit full']


Add 14 
# -------------------------------------------------------------add bill 14 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         14
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          14
    click Button                       //button[@class='btn-submit full']

Add 15 
# -------------------------------------------------------------add bill 15 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         15
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          15
    click Button                       //button[@class='btn-submit full']

Add 16 
# -------------------------------------------------------------add bill 16 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         16
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          16
    click Button                       //button[@class='btn-submit full']


Add 17 
# -------------------------------------------------------------add bill 17 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         17
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          17
    click Button                       //button[@class='btn-submit full']

Add 18 
# -------------------------------------------------------------add bill 18 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         18
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          18
    click Button                       //button[@class='btn-submit full']

Add 19 
# -------------------------------------------------------------add bill 19 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         19
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          19
    click Button                       //button[@class='btn-submit full']

Add 20 
# -------------------------------------------------------------add bill 20 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         20
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          20
    click Button                       //button[@class='btn-submit full']

Add 21 
# -------------------------------------------------------------add bill 21 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         21
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          21
    click Button                       //button[@class='btn-submit full']

Add 22 
# -------------------------------------------------------------add bill 22 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         22
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          22
    click Button                       //button[@class='btn-submit full']

Add 23 
# -------------------------------------------------------------add bill 23 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         23
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          23
    click Button                       //button[@class='btn-submit full']

Add 24 
# -------------------------------------------------------------add bill 24 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         24
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2010
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2010
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          24
    click Button                       //button[@class='btn-submit full']

Add 25   
# -------------------------------------------------------------add bill 25  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         25
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          25
    click Button                       //button[@class='btn-submit full']


Add 26 
# -------------------------------------------------------------add bill 26 ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         26
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          26
    click Button                       //button[@class='btn-submit full']


Add 27  
# -------------------------------------------------------------add bill 27  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         27
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          27
    click Button                       //button[@class='btn-submit full']


Add 28  
# -------------------------------------------------------------add bill 28  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         28
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          28
    click Button                       //button[@class='btn-submit full']


Add 29   
# -------------------------------------------------------------add bill 29  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         29
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          29
    click Button                       //button[@class='btn-submit full']


Add 30  
# -------------------------------------------------------------add bill 30  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         30
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          30
    click Button                       //button[@class='btn-submit full']


Add 31  
# -------------------------------------------------------------add bill 31  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         31
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          31
    click Button                       //button[@class='btn-submit full']


Add 32   
# -------------------------------------------------------------add bill 32  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         32
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          32


Add 33  
# -------------------------------------------------------------add bill 33  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         33
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          33
    click Button                       //button[@class='btn-submit full']


Add 34 
# -------------------------------------------------------------add bill 34  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         34
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          34
    click Button                       //button[@class='btn-submit full']


Add 35     
# -------------------------------------------------------------add bill 35  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         35
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          35
    click Button                       //button[@class='btn-submit full']


Add 36     
# -------------------------------------------------------------add bill 36  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         36
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2011
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2011
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          36
    click Button                       //button[@class='btn-submit full']


Add 37     
# -------------------------------------------------------------add bill 37  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         37
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          37
    click Button                       //button[@class='btn-submit full']


Add 38     
# -------------------------------------------------------------add bill 38  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         38
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          38
    click Button                       //button[@class='btn-submit full']


Add 39    
# -------------------------------------------------------------add bill 39  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         39
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          39
    click Button                       //button[@class='btn-submit full']

Add 40    
# -------------------------------------------------------------add bill 40  ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         40
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          40
    click Button                       //button[@class='btn-submit full']


Add 41    
# -------------------------------------------------------------add bill ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         41
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          41
    click Button                       //button[@class='btn-submit full']


Add 42   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         42
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          42
    click Button                       //button[@class='btn-submit full']


Add 43    
# -------------------------------------------------------------add bill ---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         43
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          43
    click Button                       //button[@class='btn-submit full']


Add 44    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         44
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          44
    click Button                       //button[@class='btn-submit full']

Add 45    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         45
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          45
    click Button                       //button[@class='btn-submit full']


Add 46    
# -------------------------------------------------------------add bill--------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         46
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          46
    click Button                       //button[@class='btn-submit full']


Add 47   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         47
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          47
    click Button                       //button[@class='btn-submit full']


Add 48   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         48
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2012
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2012
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          48
    click Button                       //button[@class='btn-submit full']


Add 49   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         49
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          49
    click Button                       //button[@class='btn-submit full']




Add 50    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         50
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          50
    click Button                       //button[@class='btn-submit full']




Add 51    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         51
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          51
    click Button                       //button[@class='btn-submit full']



Add 52    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         52
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          52
    click Button                       //button[@class='btn-submit full']


Add 53    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         53
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          53
    click Button                       //button[@class='btn-submit full']


Add 54   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         54
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          54
    click Button                       //button[@class='btn-submit full']


Add 55   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         55
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          55
    click Button                       //button[@class='btn-submit full']


Add 56    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         56
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          56
    click Button                       //button[@class='btn-submit full']


Add 57    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         57
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          57
    click Button                       //button[@class='btn-submit full']




Add 58    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         58
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          58
    click Button                       //button[@class='btn-submit full']

Add 59    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         59
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          59
    click Button                       //button[@class='btn-submit full']

Add 60   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         60
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2013
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2013
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          60
    click Button                       //button[@class='btn-submit full']



Add 61    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         61
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          61
    click Button                       //button[@class='btn-submit full']



Add 62  
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         62
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          62
    click Button                       //button[@class='btn-submit full']

Add 63   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         63
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          63
    click Button                       //button[@class='btn-submit full']

Add 64   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         64
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          64
    click Button                       //button[@class='btn-submit full']

Add 65  
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         65
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          65
    click Button                       //button[@class='btn-submit full']

Add 66  
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         66
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          66
    click Button                       //button[@class='btn-submit full']

Add 67   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         67
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          67
    click Button                       //button[@class='btn-submit full']

Add 68   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         68
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          68
    click Button                       //button[@class='btn-submit full']

Add 69  
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         69
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          69
    click Button                       //button[@class='btn-submit full']

Add 70    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         70
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          70
    click Button                       //button[@class='btn-submit full']

Add 71    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         71
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          71
    click Button                       //button[@class='btn-submit full']

Add 72    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         72
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2014
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2014
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          72
    click Button                       //button[@class='btn-submit full']


Add 73   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         73
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          73
    click Button                       //button[@class='btn-submit full']




Add 74   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         74
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          74
    click Button                       //button[@class='btn-submit full']

Add 75   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         75
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          75
    click Button                       //button[@class='btn-submit full']

Add 76    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         76
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          76
    click Button                       //button[@class='btn-submit full']

Add 77    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         77
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          77
    click Button                       //button[@class='btn-submit full']

Add 78    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         78
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          78
    click Button                       //button[@class='btn-submit full']

Add 79    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         79
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          79
    click Button                       //button[@class='btn-submit full']

Add 80    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         80
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          80
    click Button                       //button[@class='btn-submit full']

Add 81    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         81
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          81
    click Button                       //button[@class='btn-submit full']

Add 82    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         82
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          82
    click Button                       //button[@class='btn-submit full']

Add 83    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         83
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          83
    click Button                       //button[@class='btn-submit full']

Add 84    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         84
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2015
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2015
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          84
    click Button                       //button[@class='btn-submit full']



Add 85   
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         85
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/012016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          85
    click Button                       //button[@class='btn-submit full']


Add 86    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         86
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          86
    click Button                       //button[@class='btn-submit full']



Add 87    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         87
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          87
    click Button                       //button[@class='btn-submit full']



Add 88    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         88
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          88
    click Button                       //button[@class='btn-submit full']



Add 89    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         89
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/05/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/05/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          89
    click Button                       //button[@class='btn-submit full']



Add 90    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         90
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/06/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/06/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          90
    click Button                       //button[@class='btn-submit full']



Add 91    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         91
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/07/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/07/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          91
    click Button                       //button[@class='btn-submit full']



Add 92    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         92
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/08/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/08/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          92
    click Button                       //button[@class='btn-submit full']



Add 93    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         93
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/09/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/09/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          93
    click Button                       //button[@class='btn-submit full']



Add 94    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         94
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/10/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/10/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          94
    click Button                       //button[@class='btn-submit full']



Add 95    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         95
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/11/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/11/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          95
    click Button                       //button[@class='btn-submit full']



Add 96    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         96
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/12/2016
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/12/2016
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          96
    click Button                       //button[@class='btn-submit full']




Add 97    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         97
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2017
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2017
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          97
    click Button                       //button[@class='btn-submit full']




Add 98    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         98
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2017
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2017
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          98
    click Button                       //button[@class='btn-submit full']

Add 99    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         99 
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2017
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2017
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          99
    click Button                       //button[@class='btn-submit full']

Add 100    
# -------------------------------------------------------------add bill---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               ${BAN}                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         100
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/04/2017
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/04/2017
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          100
    click Button                       //button[@class='btn-submit full']
    








# # --------------------------------------------------print -----------------------------------------------------------------------------   
#     Print add bill
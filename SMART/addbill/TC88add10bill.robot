﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         Dialogs

Test Teardown       Run Keyword If Test Failed      Pause Execution

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

                

*** Test Cases ***


# *************************************************************************************************************************************
Add1    
    Goto Bill Payment Page Production Shop_10             
#  --------------------------------------------------------add bill Page 1-----------------------------------------------------------------  

    click element                       //a[@class='btn btn-trmadd ']
    click element                       //div[@class='table-radio']
    selenium2Library.input text         css=[name='card']                 1103700432678
    click element                       css=.Select-placeholder
    Loop add bill select TU  
    Press Special Key           	    ENTER
    sleep       2s
    click element                       //button[@class='btn-submit full']
# --------------------------------------------------------------------------------------------------------------------------------------    


# #  --------------------------------------------------------add bill Page 2 edit customer------------------------------------------------   
    
    click element                       css=.Select-control
    Press Special Key           	    C_DOWN   
    Press Special Key           	    ENTER
    
    Selenium2Library.input text         css=input[maxlength='255']              กำพล ทองเอก
    
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='.']                    47904
    Selenium2Library.input text         css=input[maxlength='55']               108/45
    Selenium2Library.input text         css=input[maxlength='30']               27
    Selenium2Library.input text         css=input[maxlength='60']               รุ่งกิจวิลลา
    Selenium2Library.input text         css=input[maxlength='110']              ใบหยก
    Selenium2Library.input text         css=div.floor .form-input               5
    Selenium2Library.input text         css=div.room_no .form-input             687
    Selenium2Library.input text         css=div.soi .form-input                 98
    Selenium2Library.input text         css=div.street .form-input              คนเดิน
    click element                       css=div.all-input-wrapper div:nth-of-type(1) > .form-value
    Selenium2Library.input text         //input[@class='form-control']          เชียงใหม่
    sleep           3s
    Press Special Key           	    C_DOWN
    Press Special Key           	    C_DOWN
    Press Special Key           	    ENTER
    click Button                       //button[@class='btn-submit full']
    sleep               2s


#  --------------------------------------------------------add bill Page 3 ----------------------------------------------- 
  
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002067                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         1
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/01/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/01/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          1
    click Button                       //button[@class='btn-submit full']


Add2 
# -------------------------------------------------------------add bill 2---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full']
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002067                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         2
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/02/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/02/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          2
    click Button                       //button[@class='btn-submit full']


Add3 
# -------------------------------------------------------------add bill 3---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full'] 
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002067                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         3
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          3
    click Button                       //button[@class='btn-submit full']
  

Add4 
# -------------------------------------------------------------add bill 4---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full'] 
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='FFSVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002067                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         3
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          3
    click Button                       //button[@class='btn-submit full']


Add5 
# -------------------------------------------------------------add bill 5---------------------------------------------
    click element                       //a[@class='btn btn-trmadd ']
    click element                       css=div.modal-table tr:nth-of-type(1) .table-radio
    click element                       css=.Select-placeholder  
    Loop add bill select TU 
    Press Special Key           	    ENTER
    click element                       //button[@class='btn-submit full'] 
    sleep       2s
    click element                       //button[@class='btn-submit full']
#  --------------------------------------------------------add bill detail ----------------------------------------------- 
    Wait Until Element Is Visible       //option[@value='SVTUC016']             10s
    click element                       //option[@value='SVTUC016']
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002067                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         3
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             01/03/2009 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               28/03/2009
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          3
    click Button                       //button[@class='btn-submit full']

# --------------------------------------------------print -----------------------------------------------------------------------------   
    Print add bill
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot


*** Test Cases ***

Edit Oldreceipt

    Goto Afterpayment Editname and Address PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          ${Receipt_number_old} 
    SikuliLibrary.Press Special Key         F8
    sleep   3s
    Page Should Contain                    เนื่องจากใบเสร็จดังกล่าวได้ถูกเปลี่ยนไปเป็นใบเสร็จเลขที่
    Capture Page Screenshot
    Close All Browsers


    
    
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot


*** Test Cases ***

Reprint Receipt Success 3 times
    
    FOR    ${index}    IN RANGE    3
           LOOP Full receipt Success     
    END

     
*** Keywords***
LOOP Full receipt Success
    Goto Reprint recepit PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD_Reprint}          ${Receipt_number_reprint2}
    # Wait Until Element Is Visible           ${calendar_button} 
    # Click Element                           ${calendar_button} 
    # Wait Until Element Is Visible           ${calendar_previous_month}
    # Click Element                           ${calendar_previous_month}
    # Click Element                           ${calendar_previous_month}
    # Wait Until Element Is Visible           ${calendar_select_date_1Mar}
    # Click Element                           ${calendar_select_date_1Mar}
    SikuliLibrary.Press Special Key         F8
    Wait Until Element Is Visible           ${Checkbox_Reprint}              10s
    Click Element                           ${Checkbox_Reprint}
    Wait Until Element Is Visible           ${Reason_Reprint} 
    Click Element                           ${Reason_Reprint} 

    FOR    ${index}    IN RANGE    2
           Press Special Key        C_DOWN
    END

    SikuliLibrary.Press Special Key                 ENTER
    sleep  1s

    Click Element        ${Checkbox_Reprint_auto} 

    SikuliLibrary.Press Special Key                 F11
    sleep  3s
    SikuliLibrary.Press Special Key                ENTER
    sleep  1s
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    sleep  2s
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}

    sleep  3s
    Page Should Contain                            Successful  
    Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt
    Move File       C:\\TRM\\Receipt\\Java Printing.pdf                        C:\\TRM\\Receipt\\${Receipt_number_reprint2}.pdf
    Close All Browsers
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

                

*** Test Cases ***

Reprint All Employee
   
    Goto Reprint recepit PRE_Production Shop_10
#     Wait Until Element Is Visible           ${calendar_button} 
#     Click Element                           ${calendar_button} 
#     Wait Until Element Is Visible           ${calendar_previous_month}
#     Click Element                           ${calendar_previous_month}
#     Click Element                           ${calendar_previous_month}
#     Wait Until Element Is Visible           ${calendar_select_date_1Mar}
#     Click Element                           ${calendar_select_date_1Mar}  
    Click Element       ${All_Document}             
    Click Element       ${All_Employee} 
    SikuliLibrary.Press Special Key                 F8   
    sleep            4s 
    Click Element       ${Row_reprint}    

    FOR    ${index}    IN RANGE    5
           Press Special Key        C_DOWN
    END
    
    Press Special Key        ENTER
    
    Capture Page Screenshot
    Page should contain     laddawan_wis
    Page should contain     apinya_thr
    Page should contain     duangkamol_hon   
    Close All Browsers
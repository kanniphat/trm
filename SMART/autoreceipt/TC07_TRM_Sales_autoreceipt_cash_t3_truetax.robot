﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary
Library         Pdf2TextLibrary



Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***
Auto Receipt Cash Type 3 True Send Tax

  Goto Auto Receipt 
  Wait Until Page Contains      TRM         30
  click element     ${select_KSC}
  sleep   1  
  click element     ${select_CCBS_Type3}           
  Choose File       ${file_selection}    C:\\TRM\\Data_Autoreceipt\\RM_TYPE_3.txt

  Wait Until Element Is Visible   ${true_sendtax}
  sleep   2 
  click element       ${true_sendtax} 
  ${total_amount_autoreceipt}   Selenium2Library.GetValue   ${total_amount_autoreceipt}
  sleep   2 

  click element   ${input_cash_autoreceipt}
  SikuliLibrary.Press Special Key           BACKSPACE  

  sleep   1
  Selenium2Library.input text   ${input_cash_autoreceipt}   ${total_amount_autoreceipt} 
  Wait Until Element Is Visible   ${verifydata_autoreceipt}       
  click element   ${verifydata_autoreceipt}

  #  ------------------------------------------------------------------------------------------------------------------ 

  sleep   3
  Wait Until Element Is Visible   ${submit_autoreceipt}     20  
  click element                   ${submit_autoreceipt} 
  Wait Until Element Is Visible    ${AdobePDF}              60  
  click element                    ${AdobePDF} 
  Wait Until Element Is Visible    ${print_autoreceipt}  
  click element                    ${print_autoreceipt} 
  sleep   3

  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat} 
  sleep     4

  ${data}      Convert Pdf To Txt   C:\\Users\\Beer\\Documents\\Java Printing.pdf 
  ${data}      Decode Bytes To String       ${data}       UTF-8 
  ${data}      Split String    ${data}
  ${data}      Set Variable    ${data[31]}
  Log    ${data}
  Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\${data}.pdf    


 
  




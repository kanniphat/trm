﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay by cheque success

    Goto Bill Payment Page PRE_Production Shop_10           
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}          10s
    click element                       ${close_alert}
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_2}

# ************************* Pay by Cheque **********************************************************************   
    click element   css=div.tab-tax > label:nth-of-type(3)
    sleep   2s
    Selenium2Library.input text           css=input[maxlength='8']          12345678
    Selenium2Library.input text           css=input[maxlength='3']          001
    Selenium2Library.input text           css=.cheque-branch-no             0001
    Selenium2Library.input text           css=[placeholder='__/__/____']            02022019

# ****************************************************************************************************************  
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Wait Until Element Is Visible       ${close_alert}          10s
    click element                       ${close_alert}
    # FOR    ${index}    IN RANGE    1
    #        Loop pay cheque
    # END

 
# *************************************************************************************************************************************

# Testcase Pay by cheque Chequenumber Fail

#     Goto Bill Payment Page PRE_Production Shop_10            
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert}
#     click element                       ${clear_checkbox}      
#     click element                       ${clear_checkbox}
#     click element                       ${checkbox_2} 

# # ************************* Pay by Cheque **********************************************************************   
#     click element   css=div.tab-tax > label:nth-of-type(3)
#     sleep   2s
#     Selenium2Library.input text           css=input[maxlength='8']          12345
#     sleep   5S
#     page should Contain                   กรอกเลขที่เช็คยังไม่ครบ
#     sleep   2s
#     Capture Page Screenshot

#     Selenium2Library.input text           css=input[maxlength='3']          001
#     Selenium2Library.input text           css=.cheque-branch-no             0001
#     Sleep   4

# # **************************************************************************************************************** 

# Testcase Pay by cheque Banknumber Fail

#     Goto Bill Payment Page PRE_Production Shop_10             
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert}
#     click element                       ${clear_checkbox}      
#     click element                       ${clear_checkbox}
#     click element                       ${checkbox_2}

# # ************************* Pay by Cheque **********************************************************************   
#     click element   css=div.tab-tax > label:nth-of-type(3)
#     sleep   2s
#     Selenium2Library.input text           css=input[maxlength='8']          12345678
#     Selenium2Library.input text           css=input[maxlength='3']          899
#     page should Contain                   รหัสธนาคารไม่ถูกต้อง
#     sleep   2s
#     Capture Page Screenshot
#     Sleep   4

# # **************************************************************************************************************** 


# Testcase Pay by cheque Branchnumber Fail

#     Goto Bill Payment Page PRE_Production Shop_10            
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert}
#     click element                       ${clear_checkbox}      
#     click element                       ${clear_checkbox}
#     click element                       ${checkbox_2}

# # ************************* Pay by Cheque **********************************************************************   
#     click element   css=div.tab-tax > label:nth-of-type(3)
#     sleep   2s
#     Selenium2Library.input text           css=input[maxlength='8']          12345678
#     Selenium2Library.input text           css=input[maxlength='3']          001
#     Selenium2Library.input text           css=.cheque-branch-no             0002
#     Capture Page Screenshot
#     page should Contain                   รหัสสาขาไม่ถูกต้อง
#     sleep   
#     Capture Page Screenshot
#     Sleep   4

# # **************************************************************************************************************** 

# Testcase Pay by cheque advancedate Fail

#     Goto Bill Payment Page PRE_Production Shop_10          
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert}
#     click element                       ${clear_checkbox}      
#     click element                       ${clear_checkbox}
#     click element                       ${checkbox_2}

# # ************************* Pay by Cheque **********************************************************************   
#     click element   css=div.tab-tax > label:nth-of-type(3)
#     sleep   2s
#     Selenium2Library.input text           css=input[maxlength='8']          12345678
#     Selenium2Library.input text           css=input[maxlength='3']          001
#     Selenium2Library.input text           css=.cheque-branch-no             0001
#     Selenium2Library.input text           css=[placeholder='__/__/____']            02082020
#     page should Contain                   วันที่เช็คล่วงหน้า
#     Sleep   2
#     Capture Page Screenshot
#     Sleep   4


# # **************************************************************************************************************** 
# Testcase Pay by cheque Backdate Fail

#     Goto Bill Payment Page PRE_Production Shop_10             
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert}
#     click element                       ${clear_checkbox}      
#     click element                       ${clear_checkbox}
#     click element                       ${checkbox_2}

# # ************************* Pay by Cheque **********************************************************************   
#     click element   css=div.tab-tax > label:nth-of-type(3)
#     sleep   2s
#     Selenium2Library.input text           css=input[maxlength='8']          12345678
#     Selenium2Library.input text           css=input[maxlength='3']          001
#     Selenium2Library.input text           css=.cheque-branch-no             0001
#     Selenium2Library.input text           css=[placeholder='__/__/____']            02082018
#     page should Contain                   วันที่เช็คหมดอายุ
#     Sleep   2
#     Capture Page Screenshot
#     Sleep   4


# # # **************************************************************************************************************** 

# ***Keywords***
# Loop pay cheque  
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}          10s
#     click element                       ${close_alert}
#     click element                       ${clear_checkbox}      
#     click element                       ${clear_checkbox}
#     click element                       ${checkbox_2}

# # ************************* Pay by Cheque **********************************************************************   
#     click element   css=div.tab-tax > label:nth-of-type(3)
#     sleep   2s
#     Selenium2Library.input text           css=input[maxlength='8']          12345678
#     Selenium2Library.input text           css=input[maxlength='3']          001
#     Selenium2Library.input text           css=.cheque-branch-no             0001
#     Selenium2Library.input text           css=[placeholder='__/__/____']            02022019

# # ****************************************************************************************************************  
#     click element   ${print_button} 
#     sleep       3s
#     SikuliLibrary.Press Special Key           ENTER
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
#     Wait Until Element Is Visible       ${close_alert}          10s
#     click element                       ${close_alert}
  
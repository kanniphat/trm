﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***

Topup 100 Baht
  Goto Sales PRE_Production Shop_10 
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   1
  Click                             ${select_topup}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${dropdown_topup}
  click element                    ${dropdown_topup}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}             10s
  click element                    ${cost}


  Selenium2Library.input text      ${cost}             100
  Selenium2Library.input text      ${mobile}           0867120024

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}        60s
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  sleep   5s
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_Topup_L1_100Baht.pdf
  sleep   3
    
# ----------------------------------------------------------------------------------------------------------------------------------
Topup 150 Baht
  click                             ${pic_topup_F8}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   1
  Click                             ${select_topup}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${dropdown_topup}
  click element                    ${dropdown_topup}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}             10s
  click element                    ${cost}


  Selenium2Library.input text      ${cost}             150
  Selenium2Library.input text      ${mobile}           0867120024

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}        60s
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  sleep   5s
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_Topup_L1_150Baht.pdf
  sleep   3


# ------------------------------------------------------------------------------------------------------------------------     
Topup 200 Baht
  click                             ${pic_topup_F8}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   1
  Click                             ${select_topup}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${dropdown_topup}
  click element                    ${dropdown_topup}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}             10s
  click element                    ${cost}


  Selenium2Library.input text      ${cost}             200
  Selenium2Library.input text      ${mobile}           0867120024

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}        60s
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  sleep   5s
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_Topup_L1_200Baht.pdf
  sleep   3

   ##----------------------------------------------------------------------------------------------------------------------------------
Topup 250 Baht
  click                             ${pic_topup_F8}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   1
  Click                             ${select_topup}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${dropdown_topup}
  click element                    ${dropdown_topup}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}             10s
  click element                    ${cost}


  Selenium2Library.input text      ${cost}             200
  Selenium2Library.input text      ${mobile}           0867120024

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}        60s
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  sleep   5s
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_Topup_L1_250Baht.pdf
  sleep   3


   ##----------------------------------------------------------------------------------------------------------------------------------
Topup 300 Baht
  click                             ${pic_topup_F8}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   1
  Click                             ${select_topup}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${dropdown_topup}
  click element                    ${dropdown_topup}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}             10s
  click element                    ${cost}


  Selenium2Library.input text      ${cost}             200
  Selenium2Library.input text      ${mobile}           0867120024

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}        60s
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  sleep   5s
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_Topup_L1_300Baht.pdf
  sleep   3
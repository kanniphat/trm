﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***

e-Wallet less than limit [20 Baht]
  Goto Sales PRE_Production Shop_10 
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   2
  Click                             ${select_e-Wallet}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${e-Wallet}
  click element                    ${e-Wallet}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}   
  Selenium2Library.input text      ${cost}            20 
  Selenium2Library.input text      ${mobile}          0891039020
  Page Should Contain              ท่านระบุจำนวนเงินไม่ถูกต้อง จำนวนเงินที่สามารถเติมได้ตั้งแต่ 50 - 20000 บาท กรุณาระบุจำนวนเงินอีกครั้ง
  sleep   3
  SikuliLibrary.Press Special Key           F5
  sleep   2 
# ----------------------------------------------------------------------------------------------------------------------------------
e-Wallet over limit [25000 Baht]
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   2
  Click                             ${select_e-Wallet}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${e-Wallet}
  click element                    ${e-Wallet}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}   
  Selenium2Library.input text      ${cost}            25000 
  Selenium2Library.input text      ${mobile}          0891039020
  Page Should Contain              ท่านระบุจำนวนเงินไม่ถูกต้อง จำนวนเงินที่สามารถเติมได้ตั้งแต่ 50 - 20000 บาท กรุณาระบุจำนวนเงินอีกครั้ง
  sleep   3
  SikuliLibrary.Press Special Key           F5
  sleep   2 
# ----------------------------------------------------------------------------------------------------------------------------------

e-Wallet not divided by ten [9Baht]
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   2
  Click                             ${select_e-Wallet}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${e-Wallet}
  click element                    ${e-Wallet}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}   
  Selenium2Library.input text      ${cost}            9
  Selenium2Library.input text      ${mobile}          0891039020
  Page Should Contain              ท่านใส่จำนวนเงินไม่ถูกต้อง จำนวนเงินที่ต้องการเติมต้องหาร 10 ลงตัว (กรุณาใส่จำนวนเงินให้ถูกต้อง ก่อนกดปุ่มตรวจสอบสินค้าและบริการ)
  sleep   3
  SikuliLibrary.Press Special Key           F5
  sleep   2 
# ----------------------------------------------------------------------------------------------------------------------------------
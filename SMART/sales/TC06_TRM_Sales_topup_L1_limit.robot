﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***

Topup L1 less than limit [20 Baht]
  Goto Sales PRE_Production Shop_10 
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   1
  Click                             ${select_topup}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${dropdown_topup}
  click element                    ${dropdown_topup}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}             10s
  click element                    ${cost}


  Selenium2Library.input text      ${cost}             20
  Selenium2Library.input text      ${mobile}           0867120024
  Wait Until Page Contains      Direct top up ต้องมีค่าระหว่าง 50-2000 บาท สำหรับลูกค้าทั่วไป : 300-3000 บาท สำหรับลูกค้า Mobile Top up-L2      10s
  Capture Page Screenshot 
  Page Should Contain           Direct top up ต้องมีค่าระหว่าง 50-2000 บาท สำหรับลูกค้าทั่วไป : 300-3000 บาท สำหรับลูกค้า Mobile Top up-L2
  sleep   3
  SikuliLibrary.Press Special Key           F5
  sleep   5  
# ----------------------------------------------------------------------------------------------------------------------------------

Topup L1 over limit [2500 Baht]
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   1
  Click                             ${select_topup}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${dropdown_topup}
  click element                    ${dropdown_topup}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}             10s
  click element                    ${cost}


  Selenium2Library.input text      ${cost}             2500
  Selenium2Library.input text      ${mobile}           0867120024
  click element                    ${check_stock}  
  Wait Until Page Contains        ราคาต่อหน่วยของ Direct Topup ต้องมีค่าระหว่าง 50 - 2000 บาท         10s
  Capture Page Screenshot 
  Page Should Contain             ราคาต่อหน่วยของ Direct Topup ต้องมีค่าระหว่าง 50 - 2000 บาท
 

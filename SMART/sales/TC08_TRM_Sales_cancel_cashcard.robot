﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
# Suite Teardown   close browser
                


*** Variables ***
${application}             C:\\Users\\Beer\\Desktop\\GenCode.exe
${Document_number1}        ALA1F0414
${Document_number2}        ALA1F0373
${Document_number3}        ALA1F0374
${Document_number4}        ALA1F0375
${Document_number5}        ALA1F0376


*** Test Cases ***

# *************************************************************************************************************************************
Cancel Cashcard Receipt 1
  Goto Cancel Sales PRE_Production Shop_10  
  Wait Until Element Is Visible             ${Input_document_number}                10s
  Selenium2Library.input text               ${Input_document_number}                ${Document_number1}
  Wait Until Element Is Visible             ${Find_document_cancel}                 10s
  click element                             ${Find_document_cancel}
  Wait Until Element Is Visible     ${serial}                                       10s  
  ${get_serial}           Selenium2Library.Get Text         ${serial}
  LOG     ${get_serial}

# *******************************************open application******************************************************************************************
 
  AutoItLibrary.Run    ${application}  
  Sleep    1
  Wait For Active Window    Form1
  Capture Page Screenshot
  Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
  Sleep    1
  Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
  Sleep    1
  ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
  log    ${PIN}
  Win Close     Form1
  
# *******************************************Cancel cash card***************************************************
   Wait Until Element Is Visible             ${click_input_password}          10s
   Sleep  2        
   click element                             ${click_input_password}
   Selenium2Library.input text               ${input_password}                ${PIN}
   Press Special Key        ENTER
   click element                             ${reason_cancel} 
   Press Special Key        C_DOWN
   Press Special Key        ENTER
   click element                            ${comfirm_cancel}
   Sleep    2
   SikuliLibrary.Press Special Key        ENTER
   Sleep    2
#    SikuliLibrary.Press Special Key         ESC
#    SikuliLibrary.Press Special Key         F2 
# *************************************************************************************************************************************


# Cancel Cashcard Receipt 2
 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number2}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Wait Until Element Is Visible     ${serial}  
#   ${get_serial}           Selenium2Library.Get Text          ${serial}
#   LOG     ${get_serial}

# # *******************************************open application******************************************************************************************
 
#   AutoItLibrary.Run    ${application}  
#   Sleep    1
#   Wait For Active Window    Form1
#   Capture Page Screenshot
#   Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
#   Sleep    1
#   Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
#   Sleep    1
#   ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
#   log    ${PIN}
#   Win Close     Form1
  
# # *******************************************Cancel cash card***************************************************
#    Wait Until Element Is Visible             ${click_input_password} 
#    Sleep  2        
#    click element                             ${click_input_password}
#    Selenium2Library.input text               ${input_password}                ${PIN}
#    Press Special Key        ENTER
#    click element                             ${reason_cancel} 
#    Press Special Key        C_DOWN
#    Press Special Key        ENTER
#    click element                            ${comfirm_cancel}
#    Sleep    2
#    SikuliLibrary.Press Special Key        ENTER
#    Sleep    2
#    SikuliLibrary.Press Special Key         ESC
#    SikuliLibrary.Press Special Key         F2  

# # *************************************************************************************************************************************

# Cancel Cashcard Receipt 3
 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number3}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Wait Until Element Is Visible     ${serial}  
#   ${get_serial}           Selenium2Library.Get Text          ${serial}
#   LOG     ${get_serial}

# # *******************************************open application******************************************************************************************
 
#   AutoItLibrary.Run    ${application}  
#   Sleep    1
#   Wait For Active Window    Form1
#   Capture Page Screenshot
#   Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
#   Sleep    1
#   Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
#   Sleep    1
#   ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
#   log    ${PIN}
#   Win Close     Form1
  
# # *******************************************Cancel cash card***************************************************
#    Wait Until Element Is Visible             ${click_input_password} 
#    Sleep  2        
#    click element                             ${click_input_password}
#    Selenium2Library.input text               ${input_password}                ${PIN}
#    Press Special Key        ENTER
#    click element                             ${reason_cancel} 
#    Press Special Key        C_DOWN
#    Press Special Key        ENTER
#    click element                            ${comfirm_cancel}
#    Sleep    2
#    SikuliLibrary.Press Special Key        ENTER
#    Sleep    2
#    SikuliLibrary.Press Special Key         ESC
#    SikuliLibrary.Press Special Key         F2  

# # *************************************************************************************************************************************

# Cancel Cashcard Receipt 4
 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number4}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Wait Until Element Is Visible     ${serial}  
#   ${get_serial}           Selenium2Library.Get Text          ${serial}
#   LOG     ${get_serial}

# # *******************************************open application******************************************************************************************
 
#   AutoItLibrary.Run    ${application}  
#   Sleep    1
#   Wait For Active Window    Form1
#   Capture Page Screenshot
#   Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
#   Sleep    1
#   Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
#   Sleep    1
#   ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
#   log    ${PIN}
#   Win Close     Form1
  
# # *******************************************Cancel cash card***************************************************
#    Wait Until Element Is Visible             ${click_input_password} 
#    Sleep  2        
#    click element                             ${click_input_password}
#    Selenium2Library.input text               ${input_password}                ${PIN}
#    Press Special Key        ENTER
#    click element                             ${reason_cancel} 
#    Press Special Key        C_DOWN
#    Press Special Key        ENTER
#    click element                            ${comfirm_cancel}
#    Sleep    2
#    SikuliLibrary.Press Special Key        ENTER
#    Sleep    2
#    SikuliLibrary.Press Special Key         ESC
#    SikuliLibrary.Press Special Key         F2  

# *************************************************************************************************************************************

# Cancel Cashcard Receipt 5
 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number5}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Wait Until Element Is Visible     ${serial}  
#   ${get_serial}           Selenium2Library.Get Text          ${serial}
#   LOG     ${get_serial}

# # *******************************************open application******************************************************************************************
 
#   AutoItLibrary.Run    ${application}  
#   Sleep    1
#   Wait For Active Window    Form1
#   Capture Page Screenshot
#   Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
#   Sleep    1
#   Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
#   Sleep    1
#   ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
#   log    ${PIN}
#   Win Close     Form1
  
# # *******************************************Cancel cash card***************************************************
#    Wait Until Element Is Visible             ${click_input_password} 
#    Sleep  2        
#    click element                             ${click_input_password}
#    Selenium2Library.input text               ${input_password}                ${PIN}
#    Press Special Key        ENTER
#    click element                             ${reason_cancel} 
#    Press Special Key        C_DOWN
#    Press Special Key        ENTER
#    click element                            ${comfirm_cancel}
#    Sleep    2
#    SikuliLibrary.Press Special Key        ENTER
#    Sleep    2
#    SikuliLibrary.Press Special Key         ESC
#    SikuliLibrary.Press Special Key         F2  
   
# ************************************check assure cancel *************************************************************************************************
# check assure cancel
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number1}
#   Page Should Contain                       ถูกยกเลิกไปแล้ว
#   Sleep    2      
#   SikuliLibrary.Press Special Key         F2 

#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number2}
#   Page Should Contain                       ถูกยกเลิกไปแล้ว
#   Sleep    2      
#   SikuliLibrary.Press Special Key         F2 

#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number3}
#   Page Should Contain                       ถูกยกเลิกไปแล้ว
#   Sleep    2      
#   SikuliLibrary.Press Special Key         F2 

#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number4}
#   Page Should Contain                       ถูกยกเลิกไปแล้ว
#   Sleep    2      
#   SikuliLibrary.Press Special Key         F2 

#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number5}
#   Page Should Contain                       ถูกยกเลิกไปแล้ว
#   Sleep    2      
#   SikuliLibrary.Press Special Key         F2 
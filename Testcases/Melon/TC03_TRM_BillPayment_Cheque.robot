﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay by cheque success

    Goto Bill Payment Page Shop_10 Melon  
    Wait Until Element Is Visible       ${SEARCH_FIELD}        10s  
    Selenium2Library.input text         ${SEARCH_FIELD}        200000000001
    sleep   1
    click                               ${melonban} 
    Wait Until Page Contains            Corporate             60s  
    click element                       ${checkbox_1_TUC1}  
    Selenium2Library.input text         ${editcashbill}             1.50

    sleep   1s
    click element   ${taxcheckbox_billpayment}


# ************************* Pay by Cheque **********************************************************************   
    click element   css=div.tab-tax > label:nth-of-type(3)
    sleep   2s
    Selenium2Library.input text           css=input[maxlength='8']          12345678
    Selenium2Library.input text           css=input[maxlength='3']          001
    Selenium2Library.input text           css=.cheque-branch-no             0001
    Selenium2Library.input text           css=[placeholder='__/__/____']            02072019

# ****************************************************************************************************************  
    sleep    2 
    Click element   ${print_button} 
    Wait For Active Window        Save Print Output As
    ControlFocus        Save Print Output As      ${EMPTY}        Edit1
    Control Send        Save Print Output As       ${EMPTY}       Edit1       1
    Sleep    1
    ControlFocus        Save Print Output As        &Save          Button2
    ControlClick        Save Print Output As        &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
        
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
   

    ${text}=    extract pdf text       C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    log  ${text}
    should contain          ${text}     ${receiptnumber[1]}
    Remove File             C:\\TRM\\Testcases\\Melon\\receipt.jpeg   
  
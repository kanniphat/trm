﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase verify edit customer
    Goto Bill Payment Page Shop_10 Melon  
    Wait Until Element Is Visible       ${SEARCH_FIELD}        10s  
    Selenium2Library.input text         ${SEARCH_FIELD}        200000000001
    sleep   1
    click                               ${melonban} 

    Wait Until Page Contains            Corporate                   90s
   
    click element                       ${checkbox_1_TUC1}
    Selenium2Library.input text         ${editcashbill}             1.50

    sleep   1s
    click element   ${taxcheckbox_billpayment}


#  --------------------------------------------------------edit customer-----------------------------------------------------------------   
    click element                       ${edit_customer_button}
    click element                       css=.Select-control
    Press Special Key           	    C_DOWN   
    Press Special Key           	    ENTER
    
    Selenium2Library.input text         css=input[maxlength='255']              สำราญโภคทรัพย์ มั่งมีศรีรัตนโนรมย์  
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='00000']                47904
    Selenium2Library.input text         css=input[maxlength='55']               108/45
    Selenium2Library.input text         css=input[maxlength='30']               27
    Selenium2Library.input text         css=input[maxlength='60']               พญาคชสาร
    Selenium2Library.input text         css=input[maxlength='110']              ตึกทุเรียน
    Selenium2Library.input text         css=div.floor .form-input               5
    Selenium2Library.input text         css=div.room_no .form-input             687
    Selenium2Library.input text         css=div.soi .form-input                 98
    Selenium2Library.input text         css=div.street .form-input              ธรรมชาติ
    click element                       css=div.all-input-wrapper div:nth-of-type(1) > .form-value
    Selenium2Library.input text         //input[@class='form-control']          เชียงใหม่
    sleep           3s
    Press Special Key           	    C_DOWN
    Press Special Key           	    C_DOWN
    Press Special Key           	    ENTER
    click Button                       //button[@class='btn-submit full']



  ${Name}     Selenium2Library.Get text         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div[1]
  should contain                ${Name}     สำราญโภคทรัพย์


  ${address}    Selenium2Library.Get text       //div[@class='D-4 M-10']//div[@class='dt-value break-word']
  should contain   ${address}   เชียงใหม่    
  should contain   ${address}   ตึกทุเรียน

# -------------------------------------------------------------------------------------------------------------------------------------------------------
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    
    
    
    Sleep    1
    click element   ${print_button} 
    sleep    2 
    Wait For Active Window        Save Print Output As
    ControlFocus        Save Print Output As      ${EMPTY}        Edit1
    Control Send        Save Print Output As       ${EMPTY}       Edit1       1
    Sleep    1
    ControlFocus        Save Print Output As        &Save          Button2
    ControlClick        Save Print Output As        &Save          Button2

   
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
         
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 


    ${text}=    extract pdf text       C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    log  ${text}
    should contain          ${text}     ${receiptnumber[1]}
    Remove File             C:\\TRM\\Testcases\\Melon\\receipt.jpeg   
  
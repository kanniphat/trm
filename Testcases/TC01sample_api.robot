*** Settings ***
Library     RequestsLibrary
Library     JSONLibrary
Library     Collections
Library     Selenium2Library
Library     String


*** Variable ***
${Base_URL}          http://172.16.2.192:8080/v1/INTATOM/PaymentService/payment/getCustomerProfile/
${fe_email}          PYMNT_TRM 
${fe_password}       TRMOPER1

*** Test Case ***


TC_002_Post_Request

   Create Session      viewdata     ${Base_URL}
   &{data}=            Create Dictionary      email=${fe_email}    password=${fe_password}
   ${body}=  create dictionary  accountStatus=OPENORDEBT    certificate=0125546004991     correlateId=357a-ed0a4aca855a  pageSize=15   
   ${header}=  create dictionary  Content-Type=application/json
   ${response}=   Post Request     viewdata   ${Base_URL}  data=${body}  headers=${header}

   Log To Console   ${response.status_code}
   Log To Console   ${response.content}
   ${Return Code}=  Convert To String  ${response.status_code}
   Should Be Equal    ${Return Code}           200


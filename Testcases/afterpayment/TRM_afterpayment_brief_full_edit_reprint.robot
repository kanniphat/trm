﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

Suite teardown  Close All Browsers 
                

*** Test Cases ***
Brief Full Edit Reprint

    Goto Afterpayment Brief to full recepit PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          ${Receipt_number_brief5} 
    SikuliLibrary.Press Special Key         F8
    Wait Until Element Is Visible           ${Checkbox}
    Click Element                           ${Checkbox}
    Wait Until Element Is Visible           ${Edit_button2}
    Click Element                           ${Edit_button2}
    Wait Until Element Is Visible           ${Edit_titlename2}
    Selenium2Library.input text             ${Edit_titlename2}         พลตำรวจเอก ดร.นพ.
    Selenium2Library.input text             ${Edit_name2}              พีรธราดล
   
    Selenium2Library.input text     ${Edit_building2}            อาคารสูงเฉียดฟ้า
    Selenium2Library.input text     ${Edit_homenumber2}          999/456
    Selenium2Library.input text     ${Edit_soi2}                 พุทธบูชา
    Selenium2Library.input text     ${Edit_road2}                นาคนิเวศน์
    Selenium2Library.input text     ${Edit_district2}            ลาดกระบัง
    Selenium2Library.input text     ${Edit_subdistrict2}         คลองสองต้นนุ่น
    Selenium2Library.input text     ${Edit_province2}            กรุงเทพมหานคร
    Selenium2Library.input text     ${Edit_postalcode2}          10520
    SikuliLibrary.Press Special Key         F11
    sleep   3s
    SikuliLibrary.Press Special Key           ENTER
    sleep   2s
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}
    
    SikuliLibrary.Press Special Key           ENTER
    Wait Until Page Contains    (กรุณาตรวจสอบความถูกต้อง ก่อนส่งเอกสารให้กับลูกค้า)          10s
    Capture Page Screenshot
    sleep   3s
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
    LOG     ${receiptnumber}   
    
    ${receiptnumber}        Remove String           ${receiptnumber}        -เลขที่ใบเสร็จรับเงิน/ใบกำกับภาษีหมายเลข
    ${receiptnumber}        Remove String           ${receiptnumber}        ได้ถูกเปลี่ยนเป็นเลขที่ใบเสร็จรับเงิน/ใบกำกับภาษีใบใหม่
    ${receiptnumber}        Remove String           ${receiptnumber}        คือ
    ${receiptnumber}        Remove String           ${receiptnumber}        (กรุณาตรวจสอบความถูกต้อง ก่อนส่งเอกสารให้กับลูกค้า)
    ${receiptnumber}        Split String            ${receiptnumber}        ${SPACE}
    LOG     ${receiptnumber}
    ${fullreceipt_number}          Set Variable     ${receiptnumber[4]}
    
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt
    Move File      C:\\TRM\\Receipt\\Java Printing.pdf         C:\\TRM\\Receipt\\ ${fullreceipt_number}.pdf
    Close Window




    # Edit receipt page
    Select Window       title=TSM-Lite
    click element       css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'แก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี')])
    click element       (//a[contains(.,'แก้ไขชื่อ และที่อยู่ในใบเสร็จรับเงิน/ใบกำกับภาษีเต็มรูป')])
    sleep       4s
    Select window           title=TRUE Retail Management System
    maximize browser window
    sleep   3s

    # Edit receipt 
    Selenium2Library.input text             ${SEARCH_FIELD}          ${fullreceipt_number} 
    SikuliLibrary.Press Special Key         F8
    Wait Until Element Is Visible           ${Checkbox}         10s
    Capture Page Screenshot

    Click Element                           ${Checkbox}
    Wait Until Element Is Visible           ${Edit_button}
    Click Element                           ${Edit_button}
    Wait Until Element Is Visible           ${Edit_titlename}
    Selenium2Library.input text             ${Edit_titlename}         พลตำรวจเอก ดร.นพ.
    Selenium2Library.input text             ${Edit_name}              พีรธราดล
   
    Selenium2Library.input text     ${Edit_building}            อาคารสูงเฉียดฟ้า
    Selenium2Library.input text     ${Edit_homenumber}          999/456
    Selenium2Library.input text     ${Edit_soi}                 พุทธบูชา
    Selenium2Library.input text     ${Edit_road}                นาคนิเวศน์
    Selenium2Library.input text     ${Edit_district}            ลาดกระบัง
    Selenium2Library.input text     ${Edit_subdistrict}         คลองสองต้นนุ่น
    Selenium2Library.input text     ${Edit_province}            กรุงเทพมหานคร
    Selenium2Library.input text     ${Edit_postalcode}          10520
    SikuliLibrary.Press Special Key         F11

    sleep   5s
    SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       1
    SikuliLibrary.Press Special Key           ENTER
    sleep   2s
    SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                      2
    SikuliLibrary.Press Special Key           ENTER
    sleep   3s
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
    SikuliLibrary.Press Special Key           ENTER
    Wait Until Page Contains    (กรุณาตรวจสอบความถูกต้อง ก่อนส่งเอกสารให้กับลูกค้า)          10s
    Capture Page Screenshot
    sleep   3s
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
    LOG     ${receiptnumber}   
    
    ${receiptnumber}        Remove String           ${receiptnumber}        -เลขที่ใบเสร็จรับเงิน/ใบกำกับภาษีหมายเลข
    ${receiptnumber}        Remove String           ${receiptnumber}        ได้ถูกเปลี่ยนเป็นเลขที่ใบเสร็จรับเงิน/ใบกำกับภาษีใบใหม่
    ${receiptnumber}        Remove String           ${receiptnumber}        คือ
    ${receiptnumber}        Remove String           ${receiptnumber}        (กรุณาตรวจสอบความถูกต้อง ก่อนส่งเอกสารให้กับลูกค้า)
    ${receiptnumber}        Split String            ${receiptnumber}        ${SPACE}
    LOG     ${receiptnumber}  
    ${editreceipt_number}          Set Variable     ${receiptnumber[4]}

   
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt
    Move File       C:\\TRM\\Receipt\\1.pdf         C:\\TRM\\Receipt\\${editreceipt_number}.pdf 

    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\2.pdf              C:\\TRM\\Receipt 
    Move File      C:\\TRM\\Receipt\\2.pdf         C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
     Close Window
   

#   Reprint receipt page
    Select Window                                   title=TSM-Lite
    click element   css=div.z-toolbarbutton[title="Payment"]
    click element       //a[contains(.,'Smart TRM (=PRE Production)')] 
    click element       (//a[contains(.,'แก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี')])
    click element       (//a[contains(.,'พิมพ์ใบเสร็จรับเงิน/ใบกำกับภาษีซ้ำ')])
    sleep       4s
    Select window           title=TRUE Retail Management System
    maximize browser window
    sleep   3s


   #   Reprint receipt 
    Selenium2Library.input text             ${SEARCH_FIELD_Reprint}          ${editreceipt_number}
    Wait Until Element Is Visible           ${calendar_button} 
    Click Element                           ${calendar_button} 
    Wait Until Element Is Visible           ${calendar_previous_month}
    Click Element                           ${calendar_previous_month}
    Click Element                           ${calendar_previous_month}
    Wait Until Element Is Visible           ${calendar_select_date_1Mar}
    Click Element                           ${calendar_select_date_1Mar}
    SikuliLibrary.Press Special Key         F8
    Wait Until Element Is Visible           ${Checkbox_Reprint}              10s
    Click Element                           ${Checkbox_Reprint}
    Wait Until Element Is Visible           ${Reason_Reprint} 
    Click Element                           ${Reason_Reprint} 

    FOR    ${index}    IN RANGE    2
           Press Special Key        C_DOWN
    END

    SikuliLibrary.Press Special Key                 ENTER
    sleep  1s

    Click Element        ${Checkbox_Reprint_auto} 
    sleep  1s
    SikuliLibrary.Press Special Key                 F11
    sleep  3s
    SikuliLibrary.Press Special Key                ENTER
    sleep  1s
    SikuliLibrary.Press Special Key                ENTER
    sleep  2s
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}

    sleep  3s
    Capture Page Screenshot
    Page Should Contain                            Successful  
    Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt
    Move File       C:\\TRM\\Receipt\\Java Printing.pdf                        C:\\TRM\\Receipt\\Reprint_${editreceipt_number}.pdf
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

Suite teardown  Close All Browsers 
                

*** Test Cases ***
Brief Full Require Input

    Goto Afterpayment Brief to full recepit PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          ${Receipt_number_brief6}
    SikuliLibrary.Press Special Key         F8
    Wait Until Element Is Visible           ${Checkbox}         10s
    Click Element                           ${Checkbox}
    Wait Until Element Is Visible           ${Edit_button2}
    Click Element                           ${Edit_button2}


    Wait Until Element Is Visible           ${Edit_titlename2}       1
    SikuliLibrary.Press Special Key        BACKSPACE 
    Selenium2Library.input text             ${Edit_titlename2}       1 
    SikuliLibrary.Press Special Key        BACKSPACE         
    Selenium2Library.input text             ${Edit_name2}            1  
    SikuliLibrary.Press Special Key        BACKSPACE     
   
    Selenium2Library.input text             ${Edit_building2}        1 
    SikuliLibrary.Press Special Key        BACKSPACE            
    Selenium2Library.input text             ${Edit_homenumber2}      1 
    SikuliLibrary.Press Special Key        BACKSPACE        
    Selenium2Library.input text             ${Edit_soi2}             1 
    SikuliLibrary.Press Special Key        BACKSPACE            
    Selenium2Library.input text             ${Edit_road2}            1 
    SikuliLibrary.Press Special Key        BACKSPACE           
    Selenium2Library.input text             ${Edit_district2}        1   
    SikuliLibrary.Press Special Key        BACKSPACE       
    Selenium2Library.input text             ${Edit_subdistrict2}     1   
    SikuliLibrary.Press Special Key        BACKSPACE       
    Selenium2Library.input text             ${Edit_province2}        1  
    SikuliLibrary.Press Special Key        BACKSPACE          
    Selenium2Library.input text             ${Edit_postalcode2}      1
    SikuliLibrary.Press Special Key        BACKSPACE  

   
    Selenium2Library.input text           ${ID_tax2}               1
    SikuliLibrary.Press Special Key        BACKSPACE
    
    Selenium2Library.input text           ${ID_branch2}            1
    SikuliLibrary.Press Special Key        BACKSPACE 
   
    SikuliLibrary.Press Special Key         F11
    sleep       2s
    Capture Page Screenshot

    Page Should Contain         กรุณาป้อนชื่อจริง
    Page Should Contain         กรุณาป้อนตำบล/แขวง
    Page Should Contain         กรุณาป้อนอำเภอ/เขต
    Page Should Contain         กรุณาป้อนจังหวัด
    Page Should Contain         กรุณาป้อนรหัสไปรษณีย์
    Page Should Contain         กรุณาป้อนรหัสประจำตัวผู้เสียภาษี
    Page Should Contain         กรุณาป้อนชื่อสาขา/สาขาที่
    Close All Browsers 

﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot
              
                

*** Test Cases ***
Cancel Brief receipt
    Goto Afterpayment Cancel recepit PRE_Production Shop_10 
    Click Element                                   ${Doc_typebutton} 
    Click Element                                   ${Doc_type_selectreceipt02} 
    Selenium2Library.input text                     ${Doc_number}               ${Receipt_number_brief}
    SikuliLibrary.Press Special Key                 F8
    Wait Until Element Is Visible                   ${Checkbox2}
    Click Element                                   ${Checkbox2} 

    Wait Until Element Is Visible                   ${Reason_cancel} 
    Click Element                                   ${Reason_cancel} 
    
    FOR    ${index}    IN RANGE    3

           Press Special Key        C_DOWN
    END

    SikuliLibrary.Press Special Key                 ENTER
    sleep  2s
    SikuliLibrary.Press Special Key                 F11
    sleep  2s
    SikuliLibrary.Press Special Key                 ENTER
    sleep  2s
    SikuliLibrary.Press Special Key                 ENTER
    sleep  3s
    Page Should Contain                             ยกเลิกรายการรับชำระเรียบร้อยแล้ว
    Capture Page Screenshot
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}
    sleep  3s
    Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt
    Move File       C:\\TRM\\Receipt\\Java Printing.pdf                        C:\\TRM\\Receipt\\Cancel_${Receipt_number_brief}.pdf
    Close All Browsers


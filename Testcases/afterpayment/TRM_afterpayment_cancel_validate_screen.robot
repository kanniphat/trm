﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

Suite teardown  Close All Browsers                 
                

*** Test Cases ***
Validate Screen Company TU

    Goto Afterpayment Cancel recepit PRE_Production Shop_10 
    Selenium2Library.input text                 //div[@class='search-content w-75']/div[3]/div[2]//input[1]                   TUC
    SikuliLibrary.Press Special Key         F8
    sleep            4s                    
    Page Should Contain                             TU
    Capture Page Screenshot
    SikuliLibrary.Press Special Key         F2

   
Validate Screen Company TVG
    Selenium2Library.input text                 //div[@class='search-content w-75']/div[3]/div[2]//input[1]                   TVG
    SikuliLibrary.Press Special Key         F8
    sleep            4s                    
    Page Should Contain                   TVG
    Capture Page Screenshot
    SikuliLibrary.Press Special Key         F2

   
Validate Screen Company RMV
    Selenium2Library.input text                 //div[@class='search-content w-75']/div[3]/div[2]//input[1]                   RMV
    SikuliLibrary.Press Special Key         F8
    sleep            4s                    
    Page Should Contain                   RMV
    Capture Page Screenshot
    SikuliLibrary.Press Special Key         F2

Validate Screen Cashier Laddawan

    Click element                               //div[@class='search-content w-75']//div[4]//button[.='...']
    Selenium2Library.input text                 //body[@class='modal-open modal-open']/div[4]//div[2]/div[5]//input[1]         ลัดดาวรรณ         
    SikuliLibrary.Press Special Key         F8 
    Click element                               //body[@class='modal-open modal-open']/div[4]//div[@class='rt-tr -odd']/div[2]
    sleep     2s
    SikuliLibrary.Press Special Key         F8 
    sleep            4s                    
    Page Should Contain                   ลัดดาวรรณ
    Capture Page Screenshot
    SikuliLibrary.Press Special Key         F2

Validate Screen Cashier Duangkamol

    Click element                               //div[@class='search-content w-75']//div[4]//button[.='...']
    Selenium2Library.input text                 //body[@class='modal-open modal-open']/div[4]//div[2]/div[5]//input[1]         ดวงกมล          
    SikuliLibrary.Press Special Key         F8 
    Wait Until Element Is Visible               //body[@class='modal-open modal-open']/div[4]//div[@class='rt-tr -odd']/div[2]
    Click element                               //body[@class='modal-open modal-open']/div[4]//div[@class='rt-tr -odd']/div[2]
    sleep     2s
    SikuliLibrary.Press Special Key         F8 
    sleep            4s                           
    Page Should Contain                   ดวงกมล
    Capture Page Screenshot
  

    

﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

                

*** Test Cases ***
Cancel Receipt Crossday

    Goto Afterpayment Cancel recepit PRE_Production Shop_10 
    Click Element                                   ${Doc_typebutton} 
    Click Element                                   ${Doc_type_selectreceipt02} 
    Selenium2Library.input text                     ${Doc_number}               SAL1000011
    Wait Until Element Is Visible           ${calendar_button} 
    Click Element                           ${calendar_button} 
    Wait Until Element Is Visible           ${calendar_previous_month}
    Click Element                           ${calendar_previous_month}
    Wait Until Element Is Visible           ${calendar_previous_month}
    Click Element                           ${calendar_previous_month}
    Wait Until Element Is Visible           ${calendar_previous_month}
    Click Element                           ${calendar_previous_month}
    Wait Until Element Is Visible           ${calendar_select_date_1Mar}
    Click Element                           ${calendar_select_date_1Mar}
    SikuliLibrary.Press Special Key                 F8
    sleep   3s
    Page Should Contain                             ไม่สามารถสร้างรายการข้ามวันได้      เนื่องจากเอกสารดังกล่าวมีวันที่ออกเอกสารไม่ใช่วันที่ปัจจุบัน 
    Capture Page Screenshot
    Close All Browsers                   
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot


*** Test Cases ***
Cancel Receipt Notfound

    Goto Afterpayment Cancel recepit PRE_Production Shop_10 
    Click Element                                   ${Doc_typebutton} 
    Click Element                                   ${Doc_type_selectreceipt02} 
    Selenium2Library.input text                     ${Doc_number}               SAL10000105
    SikuliLibrary.Press Special Key                 F8
    sleep   3s
    Page Should Contain                             ไม่พบข้อมูลที่ท่านต้องการ
    Capture Page Screenshot
    Close All Browsers
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

*** Test Cases ***
Edit Receipt Input Fault

    Goto Afterpayment Editname and Address PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          ASWPIUUOO
    SikuliLibrary.Press Special Key         F8
    sleep   6s
    Page Should Contain                    กรุณาตรวจสอบความถูกต้อง
    Capture Page Screenshot
    Close All Browsers  


    
    
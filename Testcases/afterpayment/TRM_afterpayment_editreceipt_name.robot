﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot


*** Test Cases ***



Edit Receipt Name

    Goto Afterpayment Editname and Address PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          ${Receipt_number_edit}     
    SikuliLibrary.Press Special Key         F8
    Wait Until Element Is Visible           ${Checkbox}              20s   
    Click Element                           ${Checkbox}
    Wait Until Element Is Visible           ${Edit_button}
    Click Element                           ${Edit_button}
    Wait Until Element Is Visible           ${Edit_titlename}
    Selenium2Library.input text             ${Edit_titlename}         พระองค์เจ้า
    Selenium2Library.input text             ${Edit_name}              แจ่มฟ้านภาผ่อง
    SikuliLibrary.Press Special Key         F11

    SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       1
    SikuliLibrary.Press Special Key           ENTER
    sleep   2s
    SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       2
    SikuliLibrary.Press Special Key           ENTER
    sleep   3s
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
    SikuliLibrary.Press Special Key           ENTER
    Wait Until Page Contains    (กรุณาตรวจสอบความถูกต้อง ก่อนส่งเอกสารให้กับลูกค้า)          10s
    sleep   3s
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
    LOG     ${receiptnumber}   
    
    ${receiptnumber}        Remove String           ${receiptnumber}        -เลขที่ใบเสร็จรับเงิน/ใบกำกับภาษีหมายเลข
    ${receiptnumber}        Remove String           ${receiptnumber}        ได้ถูกเปลี่ยนเป็นเลขที่ใบเสร็จรับเงิน/ใบกำกับภาษีใบใหม่
    ${receiptnumber}        Remove String           ${receiptnumber}        คือ
    ${receiptnumber}        Remove String           ${receiptnumber}        (กรุณาตรวจสอบความถูกต้อง ก่อนส่งเอกสารให้กับลูกค้า)
    ${receiptnumber}        Split String            ${receiptnumber}        ${SPACE}
    LOG     ${receiptnumber}  

    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt
    Move File       C:\\TRM\\Receipt\\1.pdf         C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\2.pdf              C:\\TRM\\Receipt 
    Move File       C:\\TRM\\Receipt\\2.pdf         C:\\TRM\\Receipt\\${receiptnumber[4]}.pdf
    Close All Browsers 

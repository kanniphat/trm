﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot


Suite teardown  Close All Browsers 



*** Test Cases ***
Edit Receipt Require Input
 
    Goto Afterpayment Editname and Address PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          ${Receipt_number_edit6} 
    SikuliLibrary.Press Special Key         F8
    Wait Until Element Is Visible           ${Checkbox}         10s
    Click Element                           ${Checkbox}
    Wait Until Element Is Visible           ${Edit_button}
    Click Element                           ${Edit_button}


    Wait Until Element Is Visible           ${Edit_titlename}       1
    SikuliLibrary.Press Special Key        BACKSPACE 
    Selenium2Library.input text             ${Edit_titlename}       1 
    SikuliLibrary.Press Special Key        BACKSPACE         
    Selenium2Library.input text             ${Edit_name}            1  
    SikuliLibrary.Press Special Key        BACKSPACE     
   
    Selenium2Library.input text             ${Edit_building}        1 
    SikuliLibrary.Press Special Key        BACKSPACE            
    Selenium2Library.input text             ${Edit_homenumber}      1 
    SikuliLibrary.Press Special Key        BACKSPACE        
    Selenium2Library.input text             ${Edit_soi}             1 
    SikuliLibrary.Press Special Key        BACKSPACE            
    Selenium2Library.input text             ${Edit_road}            1 
    SikuliLibrary.Press Special Key        BACKSPACE           
    Selenium2Library.input text             ${Edit_district}        1   
    SikuliLibrary.Press Special Key        BACKSPACE       
    Selenium2Library.input text             ${Edit_subdistrict}     1   
    SikuliLibrary.Press Special Key        BACKSPACE       
    Selenium2Library.input text             ${Edit_province}        1  
    SikuliLibrary.Press Special Key        BACKSPACE          
    Selenium2Library.input text             ${Edit_postalcode}      1
    SikuliLibrary.Press Special Key        BACKSPACE  

   
    Selenium2Library.input text           ${ID_tax}               1
    SikuliLibrary.Press Special Key        BACKSPACE 
    
    Selenium2Library.input text           ${ID_branch}            1
    SikuliLibrary.Press Special Key        BACKSPACE 
   
    SikuliLibrary.Press Special Key         F11
    sleep       2s
    Capture Page Screenshot

    Page Should Contain         กรุณาป้อนชื่อจริง
    Page Should Contain         กรุณาป้อนตำบล/แขวง
    Page Should Contain         กรุณาป้อนอำเภอ/เขต
    Page Should Contain         กรุณาป้อนจังหวัด
    Page Should Contain         กรุณาป้อนรหัสไปรษณีย์
    Page Should Contain         กรุณาป้อนรหัสประจำตัวผู้เสียภาษี
    Page Should Contain         กรุณาป้อนชื่อสาขา/สาขาที่
    Close All Browsers 

    
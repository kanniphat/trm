﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot


*** Test Cases ***
Full Receipt Already
    Goto Afterpayment Brief to full recepit PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          ${Receipt_number_full_already}
    SikuliLibrary.Press Special Key         F8
    sleep   3s
    Page Should Contain                    เนื่องจากเอกสารดังกล่าวเป็นแบบเต็มรูปอยู่แล้ว
    sleep   2s
    Capture Page Screenshot
    Close All Browsers


    
    
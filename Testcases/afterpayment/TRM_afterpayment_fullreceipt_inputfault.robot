﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

*** Test Cases ***
Full Receipt Input Fault

    Goto Afterpayment Brief to full recepit PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD}          SAL009494
    SikuliLibrary.Press Special Key         F8
    sleep   3s
    Page Should Contain                    กรุณาตรวจสอบความถูกต้อง
    sleep   2s
    Capture Page Screenshot
    Close All Browsers 


    
    
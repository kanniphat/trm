﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Afterpayment.robot
Resource        ..\\..\\Variables\\Variables_Data_Afterpayment.robot

                

*** Test Cases ***

Reprint Type Fault
    
    Goto Reprint recepit PRE_Production Shop_10
    Selenium2Library.input text             ${SEARCH_FIELD_Reprint}          RAL100277
    # Wait Until Element Is Visible           ${calendar_button} 
    # Click Element                           ${calendar_button} 
    # Wait Until Element Is Visible           ${calendar_previous_month}
    # Click Element                           ${calendar_previous_month}
    # Click Element                           ${calendar_previous_month}
    # Wait Until Element Is Visible           ${calendar_select_date_1Mar}
    # Click Element                           ${calendar_select_date_1Mar}
    Wait Until Element Is Visible           ${Doc_typebutton_reprint} 
    Click Element                           ${Doc_typebutton_reprint} 
    Wait Until Element Is Visible           ${Doc_type_selectreceipt04}
    Click Element                           ${Doc_type_selectreceipt04}
    SikuliLibrary.Press Special Key         F8
    sleep   2s
    Capture Page screenshot
    Page should contain                     ไม่พบข้อมูลที่ท่านต้องการ
    Close All Browsers
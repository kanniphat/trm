﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary
Library         Pdf2TextLibrary


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***
Auto Receipt Cheque Type 3 True Send Tax

  Goto Auto Receipt 
  Wait Until Page Contains      TRM         30
  click element     ${select_KSC} 
  sleep   1  
  click element     ${select_CCBS_Type3}           
  Choose File       ${file_selection}    C:\\TRM\\Data_Autoreceipt\\RM_TYPE_3.txt

  Wait Until Element Is Visible   ${true_sendtax}
  sleep   2 
  click element       ${true_sendtax} 
  
  Wait Until Element Is Visible   ${cheque_autoreceipt} 
  click element                   ${cheque_autoreceipt}           
  Wait Until Element Is Visible   ${cheque_num_autoreceipt}  
  Selenium2Library.input text     ${cheque_num_autoreceipt}         12345678    
  Selenium2Library.input text     ${cheque_bank_autoreceipt}        001
  Selenium2Library.input text     ${cheque_branch_autoreceipt}      0001
  Wait Until Element Is Visible   ${verifydata_autoreceipt}       
  click element   ${verifydata_autoreceipt}
 
 #------------------------------------------------------------------------------------------------------------------ 

  sleep   3
  Wait Until Element Is Visible   ${submit_autoreceipt}     20  
  click element                   ${submit_autoreceipt} 
  Wait Until Element Is Visible    ${AdobePDF}              60  
  click element                    ${AdobePDF} 
  Wait Until Element Is Visible    ${print_autoreceipt}  
  click element                    ${print_autoreceipt} 
  sleep   3

  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat} 
  sleep     4

  ${data}      Convert Pdf To Txt   C:\\Users\\Beer\\Documents\\Java Printing.pdf 
  ${data}      Decode Bytes To String       ${data}       UTF-8 
  ${data}      Split String    ${data}
  ${data}      Set Variable    ${data[31]}
  Log    ${data}
  Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\${data}.pdf

 
  




﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary
Library         Pdf2TextLibrary


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***
Autoreceipt Type1 Wrong Company

  Goto Auto Receipt 
  Wait Until Page Contains      TRM         30
  click element     ${select_KSC}
  sleep   1       
  Choose File       ${file_selection}    C:\\TRM\\Data_Autoreceipt\\RM_TYPE_1.txt
  sleep  1
  click element     ${edit_button_autoreceipt}
  
  Selenium2Library.input text   ${title_name}           พลเอก              
  Selenium2Library.input text   ${name}                 พงศพัศ       
  Selenium2Library.input text   ${tax_number}           1103700432678          
  Selenium2Library.input text   ${branch}               0001
  Selenium2Library.input text   ${home_number}          106/11
  Selenium2Library.input text   ${moo}                  5
  Selenium2Library.input text   ${village}              สวนสน
  Selenium2Library.input text   ${building}             ใบโพธิ์
  Selenium2Library.input text   ${floor}                21
  Selenium2Library.input text   ${room}                 A009
  Selenium2Library.input text   ${soi}                  40
  Selenium2Library.input text   ${road}                 สุขุมวิท

  Wait Until Element Is Visible   ${address_button}
  click element     ${address_button}              
  Selenium2Library.input text     ${input_zipcode}             10520
  Wait Until Element Is Visible   ${click_row2}       20
  click element     ${click_row2}
  click element     ${confirm_bn_autoreceipt} 

  ${total_amount_autoreceipt}   Selenium2Library.GetValue   ${total_amount_autoreceipt}
  sleep   2 

  click element   ${input_cash_autoreceipt}
  SikuliLibrary.Press Special Key           BACKSPACE  

  
  Selenium2Library.input text   ${input_cash_autoreceipt}   ${total_amount_autoreceipt} 
  Wait Until Element Is Visible   ${verifydata_autoreceipt}       
  click element   ${verifydata_autoreceipt} 
  sleep  2
  Page should Contain     เลือกรหัสโครงข่ายไม่ตรงกับไฟล์ข้อมูล
  Capture Page Screenshot




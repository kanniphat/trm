﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***
Auto Receipt T2 Wrong Withholding Tax

  Goto Auto Receipt 
  Wait Until Page Contains      TRM         30
  click element     ${select_KSC} 
  sleep   1
  click element     ${select_CCBS_Type2}         
  Choose File       ${file_selection}    C:\\TRM\\Data_Autoreceipt\\RM_TYPE_2.txt
  click element     ${edit_button_autoreceipt}
  
  Selenium2Library.input text   ${title_name}           พลเอก              
  Selenium2Library.input text   ${name}                 พงศพัศ       
  Selenium2Library.input text   ${tax_number}           1103700432678          
  Selenium2Library.input text   ${branch}               0001
  Selenium2Library.input text   ${home_number}          106/11
  Selenium2Library.input text   ${moo}                  5
  Selenium2Library.input text   ${village}              สวนสน
  Selenium2Library.input text   ${building}             ใบโพธิ์
  Selenium2Library.input text   ${floor}                21
  Selenium2Library.input text   ${room}                 A009
  Selenium2Library.input text   ${soi}                  40
  Selenium2Library.input text   ${road}                 สุขุมวิท

  Wait Until Element Is Visible   ${address_button}
  sleep   2
  click element     ${address_button}              
  Selenium2Library.input text     ${input_zipcode}             10520
  Wait Until Element Is Visible   ${click_row2}       20
  click element     ${click_row2}
  sleep   2
  click element     ${confirm_bn_autoreceipt} 

  sleep   2 
  click element     ${customer_sendtax}
  Wait Until Element Is Visible     ${69tawi}
  click element     ${69tawi}
  Wait Until Element Is Visible   ${input_withholdingtax_autoreceipt}
  Wait Until Element Is Visible   ${withholdingtax_number}
  Selenium2Library.input text     ${withholdingtax_number}          12345678

  click element   ${input_withholdingtax_autoreceipt}
  SikuliLibrary.Press Special Key           BACKSPACE 
  Selenium2Library.input text     ${input_withholdingtax_autoreceipt}     224.00
  Page Should Contain             จำนวนเงินไม่เท่ากับค่าใน file
  Capture Page Screenshot
  sleep   2


  

 
  





 
  



  
  






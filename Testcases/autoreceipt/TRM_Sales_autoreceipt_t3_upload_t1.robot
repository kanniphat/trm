﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary
Library         Pdf2TextLibrary


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***
Autoreceipt Type3 but upload Type1

  Goto Auto Receipt 
  Wait Until Page Contains      TRM         30
  click element     ${select_KSC}
  sleep  1 
  click element     ${select_CCBS_Type3}         
  Choose File       ${file_selection}    C:\\TRM\\Data_Autoreceipt\\RM_TYPE_1.txt
  sleep  2
  Page should Contain     รูปแบบข้อมูลในไฟล์ไม่ตรงกับประเภทข้อมูลที่เลือก
  Capture Page Screenshot 


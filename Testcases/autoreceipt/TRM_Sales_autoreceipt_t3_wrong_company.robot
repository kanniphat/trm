﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary
Library         Pdf2TextLibrary


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***
Autoreceipt Type3 Wrong Company

  Goto Auto Receipt 
  Wait Until Page Contains      TRM         30
  click element     ${select_RMV} 
  sleep   1 
  click element     ${select_CCBS_Type3}           
  Choose File       ${file_selection}    C:\\TRM\\Data_Autoreceipt\\RM_TYPE_3.txt

  Wait Until Element Is Visible     ${69tawi}
  click element     ${69tawi}
  Wait Until Element Is Visible   ${input_withholdingtax_autoreceipt}
  Wait Until Element Is Visible   ${withholdingtax_number}
  Selenium2Library.input text     ${withholdingtax_number}          12345678

  click element   ${input_withholdingtax_autoreceipt}
  SikuliLibrary.Press Special Key           BACKSPACE 
  Selenium2Library.input text     ${input_withholdingtax_autoreceipt}     1464.00

  ${total_amount_autoreceipt}   Selenium2Library.GetValue   ${total_amount_autoreceipt}
  sleep   2 

  click element   ${input_cash_autoreceipt}
  SikuliLibrary.Press Special Key           BACKSPACE  

  sleep   1
  Selenium2Library.input text   ${input_cash_autoreceipt}   ${total_amount_autoreceipt} 
  Wait Until Element Is Visible   ${verifydata_autoreceipt}       
  click element   ${verifydata_autoreceipt}

  sleep  2
  Page should Contain     เลือกรหัสโครงข่ายไม่ตรงกับไฟล์ข้อมูล
  Capture Page Screenshot      
  
*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Creare Brief Receipt

    Goto Bill Payment Page PRE_Production Shop_10 Melon             
    Selenium2Library.input text         ${SEARCH_FIELD}       10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    Wait Until Element Is Visible       ${clear_checkbox}                   10s
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_2}
    Capture Page Screenshot
    
    Wait Until Element Is Visible       //i[@class='fa fa-pencil-square-o']
    click element                       //i[@class='fa fa-pencil-square-o']
    Wait Until Element Is Visible       //div[@class='form-input-group receiptFormat']//i[@class='fa fa-circle-o']
    click element                       //div[@class='form-input-group receiptFormat']//i[@class='fa fa-circle-o']
                                       
    sleep    2s
    click element                       //button[@class='btn-submit full']

  
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Capture Page Screenshot
    Page should Contain         พิมพ์ใบเสร็จ  
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    FOR    ${index}    IN RANGE    5

           Loop create brief receipt
    END



***keywords***
Loop create brief receipt
    Wait Until Element Is Visible       ${SEARCH_FIELD} 
    Selenium2Library.input text         ${SEARCH_FIELD}       10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    Wait Until Element Is Visible       ${clear_checkbox}                   10s
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
     click element                      ${checkbox_2}
    Capture Page Screenshot

    Wait Until Element Is Visible       //i[@class='fa fa-pencil-square-o']
    click element                       //i[@class='fa fa-pencil-square-o']
    Wait Until Element Is Visible       //div[@class='form-input-group receiptFormat']//i[@class='fa fa-circle-o']
    click element                       //div[@class='form-input-group receiptFormat']//i[@class='fa fa-circle-o']
                                       
    sleep    2s
    click element                       //button[@class='btn-submit full']

  
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Capture Page Screenshot
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
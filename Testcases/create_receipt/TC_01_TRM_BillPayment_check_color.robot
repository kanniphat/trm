*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Check color Yellow [Direct Debit]

    Goto Bill Payment Page PRE_Production Shop_10 Melon             
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    ${bgcolor}    Execute Javascript    return document.defaultView.getComputedStyle(document.getElementsByClassName("text-detail-w")[4],null)['background-color']
    Should Be Equal     ${bgcolor}      rgb(246, 247, 201)
    Capture Page Screenshot


# Check color Green [Due Date]
#     Goto Bill Payment Page PRE_Production Shop_10 Melon             
#     Selenium2Library.input text         ${SEARCH_FIELD}        200058323
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     ${bgcolor}    Execute Javascript    return document.defaultView.getComputedStyle(document.getElementsByClassName("text-detail-w")[2],null)['background-color']
#     Should Be Equal     ${bgcolor}      rgb(226, 239, 212)
#     Capture Page Screenshot


# Check color Blue [Parent Band]
#     Goto Bill Payment Page PRE_Production Shop_10 Melon             
#     Selenium2Library.input text         ${SEARCH_FIELD}        200057753
#     press key                           ${SEARCH_FIELD}         \\13
#     sleep   3
#     ${bgcolor}    Execute Javascript    return document.defaultView.getComputedStyle(document.getElementsByClassName("text-detail-w")[2],null)['background-color']
#     Should Be Equal     ${bgcolor}      rgb(222, 232, 234)
#     Capture Page Screenshot
#     sleep   5


# Check color Yellow,Green,Blue [3 color]

#     Goto Bill Payment Page PRE_Production Shop_10 Melon             
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     ${bgcolor}    Execute Javascript    return document.defaultView.getComputedStyle(document.getElementsByClassName("text-detail-w")[4],null)['background-color']
#     Should Be Equal     ${bgcolor}      rgb(246, 247, 201)
#     Capture Page Screenshot

#     sleep   2s
#     Selenium2Library.input text         ${SEARCH_FIELD}        200058323
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     ${bgcolor}    Execute Javascript    return document.defaultView.getComputedStyle(document.getElementsByClassName("text-detail-w")[2],null)['background-color']
#     Should Be Equal     ${bgcolor}      rgb(226, 239, 212)
#     Capture Page Screenshot



#     sleep   2s
#     Selenium2Library.input text         ${SEARCH_FIELD}        200057753
#     press key                           ${SEARCH_FIELD}         \\13
#     sleep   3
#     ${bgcolor}    Execute Javascript    return document.defaultView.getComputedStyle(document.getElementsByClassName("text-detail-w")[2],null)['background-color']
#     Should Be Equal     ${bgcolor}      rgb(222, 232, 234)
#     Capture Page Screenshot
#     sleep   4s
     
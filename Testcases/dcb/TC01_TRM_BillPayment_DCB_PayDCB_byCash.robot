*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

# Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Pay DCB only 1 Bill from multi Bill
    Goto DCB Page Shop_10  
    Wait Until Element Is Visible       ${SEARCH_FIELD}        50    
    Selenium2Library.input text         ${SEARCH_FIELD}        200072891
    press key                           ${SEARCH_FIELD}         \\13  

    Wait Until Element Is Visible       ${clear_checkbox}      50 
    click element                       ${clear_checkbox}
    click element                       ${clear_checkbox}
    Page Should Contain                 ค่าสินค้าหรือค่าบริการเรียกเก็บแทน
    Page Should Contain                 ค่าใช้บริการ
    Set Focus To Element                ${checkbox_6_TUC} 

    sleep       2     
    Capture Page Screenshot

    click element       ${checkbox_6_TUC} 
    sleep       1     
    Capture Page Screenshot
   

   #--------จ่ายด้วยเงินสด--------------

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    ${input_amount}       Remove String     ${input_amount}    ,


    ${input_amount}       Convert To Number     ${input_amount}         0
    ${input_amount}       Evaluate        "%.2f" % ${input_amount}
    ${10}                 Set Variable     10.00
    ${input_amount}       Evaluate    ${input_amount}+${10}   
    ${input_amount}       Evaluate        "%.2f" % ${input_amount}

    
    LOG   ${input_amount} 


    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
   #------------------------------------------------------

    Sleep    2
    click element   ${print_button} 
    Sleep    6
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    Sleep    2
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 


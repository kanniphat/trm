*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         Dialogs

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Pay only Bill by Cash
    Goto DCB Page Shop_10  
    Wait Until Element Is Visible       ${SEARCH_FIELD}        20    
    Selenium2Library.input text         ${SEARCH_FIELD}        200072891
    press key                           ${SEARCH_FIELD}         \\13  

    Wait Until Element Is Visible       ${clear_checkbox}      50 
    click element                       ${clear_checkbox}
    click element                       ${clear_checkbox}
    Page Should Contain                 ค่าสินค้าหรือค่าบริการเรียกเก็บแทน
    Page Should Contain                 ค่าใช้บริการ
    Set Focus To Element                ${checkbox_2_TVG} 

    sleep       2     
    Capture Page Screenshot

    click element       ${checkbox_2_TVG} 
    sleep       1     
    Capture Page Screenshot


# ************************* Pay by Cheque **********************************************************************   
    click element   css=div.tab-tax > label:nth-of-type(3)
    sleep   2s
    Selenium2Library.input text           css=input[maxlength='8']          12345678
    Selenium2Library.input text           css=input[maxlength='3']          001
    Selenium2Library.input text           css=.cheque-branch-no             0001
    Selenium2Library.input text           css=[placeholder='__/__/____']            02022019   

# ----------------------------------------------------------------------------------------------------------------   
    Sleep    2
    click element   ${print_button} 
    Sleep    5
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    Sleep    2
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
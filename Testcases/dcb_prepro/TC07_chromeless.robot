*** Settings ***
Library           Selenium2Library

Test Teardown     Close Browser   

*** Variables ***
${url}            https://medium.com/@nottyo/
@{chrome_arguments}    --disable-infobars    --headless    --disable-gpu
${page_text}      Software Engineer
${timeout}        10s

*** Test Cases ***
TC_01_Chrome Headless
    [Documentation]    Sample Test For Chrome Headless
    [Tags]    chrome    headless
    ${chrome_options}=    Set Chrome Options
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Go To    ${url}
    Wait Until Page Contains    ${page_text}    ${timeout}
    Capture Page Screenshot
    


*** Keywords ***
Set Chrome Options
    [Documentation]    Set Chrome options for headless mode
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    : FOR    ${option}    IN    @{chrome_arguments}
    \    Call Method    ${options}    add_argument    ${option}
    [Return]    ${options}

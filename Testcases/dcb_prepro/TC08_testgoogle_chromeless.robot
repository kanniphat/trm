*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String

Test Teardown     Close Browser   


                
*** Variables ***
${website}  https://www.google.co.th/
${browser}  gc
@{chrome_arguments}    --disable-infobars    --headless    --disable-gpu
${page_text}      robot
${timeout}        10s





*** Test Cases ***
Testname Open browser

    ${chrome_options}=    Set Chrome Options
    Create Webdriver    Chrome    chrome_options=${chrome_options}

    Go To         ${website}      
    maximize browser window
    Input Text          name=q          robot
    sleep   2
    Capture Page Screenshot
    press Key          name=btnK           \\13
    sleep   2
    Wait Until Page Contains         robot
    Capture Page Screenshot







*** Keywords ***
Set Chrome Options
    [Documentation]    Set Chrome options for headless mode
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    : FOR    ${option}    IN    @{chrome_arguments}
    \    Call Method    ${options}    add_argument    ${option}
    [Return]    ${options}

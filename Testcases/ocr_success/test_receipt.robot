*** Settings ***
Library     Selenium2Library
Library         OperatingSystem
Library     test_ocr.py

*** Variables ***
${pdf_file}     C:\\TRM\\Receipt\\mAL100356.pdf

*** Test Cases ***
Test PDF To Text
    ${text}=    extract pdf text       ${pdf_file}
    log  ${text}
    should contain          ${text}     mAL100356
    # should contain        ${text}     PERDATAENVS
    # should contain        ${text}     ดวงกมล
    # should contain        ${text}     1,762.38
    Remove File           C:\\TRM\\Testcases\\ocr_success\\receipt.jpeg   
  

*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_shop116.robot
Resource        ..\\..\\Variables\\Variables_shop116.robot

Suite Teardown     Close All Browsers
                

*** Test Cases ***

# *************************************************************************************************************************************
Test Open shop 116
    Goto Shop 116 by Manager
    Open Shop 
    sleep       2
    Select window           title=Cash Management
    Close Window
    

Test Allocate Cash for Change
    Allocate Cash for Change Ratthanasuda for Open
    Sleep       3
    Select window           title=Cash Management
    Close Window

    Allocate Cash for Change Jithaporn for Open
    Sleep       3
    Select window           title=Cash Management
    Close Window













*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_shop116.robot
Resource        ..\\..\\Variables\\Variables_shop116.robot


# Suite Teardown    Close All Browsers
                

*** Test Cases ***

# *************************************************************************************************************************************
Test Close shop 116
    Goto Shop 116 by Manager
    # Allocate Cash for Change Ratthanasuda for Close
    # Allocate Cash for Change Jithaporn for Close
    
    # sleep       2
    # Select window           title=Cash Management
    # Close Window

    Confirm Reconcile Ratanasuda
    # Confirm Reconcile Jittaporn
    sleep       2
    Select window           title=Cash Management
    Close Window


    End shift Ratthanasuda
    Wait Until Page Contains            ปิดกะการทำงานสิ้นวัน            20s
    sleep       2
    Select window           title=Cash Management
    Close Window

    End shift Jithaporn  
    Wait Until Page Contains            ปิดกะการทำงานสิ้นวัน            20s
    sleep       2
    Select window           title=Cash Management
    Close Window
    

    End Day Reconcile
    sleep       2
    Select window           title=Cash Management
    Close Window

    
    Close Shop
    sleep       2
    Select window           title=Cash Management
    Close Window
    sleep       2
    Select window           title=TSM-Lite








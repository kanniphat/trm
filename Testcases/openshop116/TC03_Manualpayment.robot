*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_Manualpayment.robot
Resource        ..\\..\\Variables\\Variables_Manualpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

# Test Teardown     Close Browser
                

*** Test Cases ***

# *************************************************************************************************************************************
# Test Manual Payment by Manager
#     Goto Manual Payment Page by Manager 
#     Wait Until Element Is Visible               ${findproduct_and_service_button}        10s              
#     click element                               ${findproduct_and_service_button}
#     Wait Until Element Is Visible               ${find_button}                                      
#     click element                               ${find_button} 
#     Wait Until Element Is Visible               ${product_1}                                          
#     click element                               ${product_1}     
#     click element                               ${addproduct} 
#     Wait Until Element Is Visible               ${ref_no1} 
#     Selenium2Library.input text                 ${ref_no1}                  1111
#     Selenium2Library.input text                 ${ref_no2}                  2222
#     Selenium2Library.input text                 ${amount}                   500

#     Wait Until Element Is Visible               ${cash}   
#     Selenium2Library.input text                 ${cash}                     300
#     Wait Until Element Is Visible               ${credit_tab}   
#     click element                               ${credit_tab}   
#     Wait Until Element Is Visible               ${add_credit_card}    
#     click element                               ${add_credit_card} 
#     Wait Until Element Is Visible               ${cardnumber}         
#     Selenium2Library.input text                 ${cardnumber}               4999999999999999
#     Selenium2Library.input text                 ${approvenumber}            001    
    #   sleep   2                                
#     Press Special Key                           ENTER                 
#     Wait Until Element Is Visible               ${print_receipt}                       
#     click element                               ${print_receipt} 
#     Wait Until Element Is Visible               ${select_AdobePDF}                        
#     click element                               ${select_AdobePDF}                        
#     Wait Until Element Is Visible               ${confirm}                        
#     click element                               ${confirm}                            
                           
#     sleep    2
#     Wait For Active Window             Save PDF File As 
#     ControlFocus        Save PDF File As      ${EMPTY}        Edit1
#     Control Send        Save PDF File As      ${EMPTY}        Edit1       Slip_Manual_Payment1
#     Sleep    1
#     ControlFocus      Save PDF File As       &Save          Button2
#     ControlClick      Save PDF File As       &Save          Button2
#     sleep       6s
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Slip_Manual_Payment1.pdf              C:\\TRM\\Receipt


# -----------------------------------------------------------------------------------------------------------------------

Test Manual Payment by Cashier
    Goto Manual Payment Page by Casiher
    Wait Until Element Is Visible               ${findproduct_and_service_button}          10s           
    click element                               ${findproduct_and_service_button}
    Wait Until Element Is Visible               ${find_button}                                      
    click element                               ${find_button} 
    Wait Until Element Is Visible               ${product_1}                                          
    click element                               ${product_1}     
    click element                               ${addproduct} 
    Wait Until Element Is Visible               ${ref_no1} 
    Selenium2Library.input text                 ${ref_no1}                  1111
    Selenium2Library.input text                 ${ref_no2}                  2222
    Selenium2Library.input text                 ${amount}                   800

    Wait Until Element Is Visible               ${cash}   
    Selenium2Library.input text                 ${cash}                     500
    Wait Until Element Is Visible               ${credit_tab}   
    click element                               ${credit_tab}   
    Wait Until Element Is Visible               ${add_credit_card}    
    click element                               ${add_credit_card} 
    Wait Until Element Is Visible               ${cardnumber}         
    Selenium2Library.input text                 ${cardnumber}               4999999999999999
    Wait Until Element Is Visible               ${approvenumber}
    sleep   1
    Selenium2Library.input text                 ${approvenumber}            001  
    sleep   2                                  
    Press Special Key                           ENTER                 
    Wait Until Element Is Visible               ${print_receipt}                       
    click element                               ${print_receipt} 
    Wait Until Element Is Visible               ${select_AdobePDF}                        
    click element                               ${select_AdobePDF}                        
    Wait Until Element Is Visible               ${confirm}                        
    click element                               ${confirm}                            
                           
    sleep    2
    Wait For Active Window             Save PDF File As 
    ControlFocus        Save PDF File As      ${EMPTY}        Edit1
    Control Send        Save PDF File As      ${EMPTY}        Edit1       Slip_Manual_Payment2
    Sleep    1
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    sleep       6s
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Slip_Manual_Payment2.pdf              C:\\TRM\\Receipt

            
                        
                                

   


















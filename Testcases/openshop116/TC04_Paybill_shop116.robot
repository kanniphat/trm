*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_shop116.robot
Resource        ..\\..\\Variables\\Variables_shop116.robot
Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown     Close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Paybill shop 116 by trmpay18
    
    open browser    ${URL}          ie
    maximize browser window
    click element   xpath=id("moreInfoContainer")/a[1]
    click element   id=overridelink
    Wait Until Element Is Visible    id=IDToken1     10s   
    Selenium2Library.input text      id=IDToken1     trmpay18
    click element                    id=IDToken2   
    Clear Element Text               id=IDToken2   
    sleep   1
    Selenium2Library.input text      id=IDToken2     P@ssw0rdTRM     
    click element   xpath=//*[@id="loginDetails2"]/table[2]/tbody/tr/td/input
    Wait Until Page Contains        Shop                10s

    Wait Until Element Is Visible       //i[@class='z-combobox-btn'] 
    click element       //i[@class='z-combobox-btn']       
    sleep   1
    click element        //td[contains(.,'80000116')]
    click image          css=div.z-toolbarbutton-cnt > img
    Wait Until Page Contains     Information            20s
    Wait Until Element Is Visible       css=div.z-toolbarbutton[title="Payment"]
    click element                       css=div.z-toolbarbutton[title="Payment"]
    Wait Until Element Is Visible       //a[contains(.,'Smart TRM (=Production)')] 
    click element                       //a[contains(.,'Smart TRM (=Production)')] 
    Wait Until Element Is Visible       (//a[contains(.,'ชำระค่าบริการ')])[1]
    click element                       (//a[contains(.,'ชำระค่าบริการ')])[1]
    Wait Until Element Is Visible       (//a[contains(.,'ชำระค่าบริการ')])[2]
    click element                       (//a[contains(.,'ชำระค่าบริการ')])[2]
    sleep       3s
    Select window           title=TRM Billpayment
    maximize browser window 
    Wait Until Element Is Visible   //tr[3]//div[@class='table-radio']
    click element                   //tr[3]//div[@class='table-radio']
    Wait Until Page Contains       TRM Billpayment         10s
    Select window                  title=TRM Billpayment 
# ---------------------------------------------------------------------------------------------------------------------------------------  
    Wait Until Element Is Visible       ${SEARCH_FIELD}        50s
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${clear_checkbox}                   80s
    click element                       ${clear_checkbox}
    click element                       ${clear_checkbox}
    Wait Until Element Is Visible       ${checkbox_2}                       80s
    click element   ${checkbox_2} 

# ************************* Click Credit card *************************************
    click element   ${creditcard_option} 
    click element   ${creditcard_button}
    sleep   2s
    click element   ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}         4921418000471099      
    Selenium2Library.input text      ${input_secretnumber}              569
    ${BankName1}             Get Value               ${BankName}   
    should be Equal     ${BankName1}      ธนาคารกสิกรไทย
# ************************************************************************************     
    click element   ${print_button} 
    sleep       3s
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}

    # sleep       5s
    # Right Click     ${pic_acrobat} 
    # Click           ${pic_close acrobat}      
    # sleep       1s
    # Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    # ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    # Log             ${receiptnumber[1]}
    # Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    




  
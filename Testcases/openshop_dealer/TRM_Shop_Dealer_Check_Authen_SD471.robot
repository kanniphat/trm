﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String

Resource        ..\\..\\Keywords\\Keywords_Openshop_dealer.robot
Resource        ..\\..\\Variables\\Variables_Openshop_dealer_SD471.robot
Test Teardown   close browser
                

*** Test Cases ***

Check Menu Shop Dealer Page by Manager
   Shop Dealer SD119 Page by Manager
   Wait Until Element Is Visible      //div[@class='div-menu']/div[@class='row']/div[contains(.,'Retail Sales')]      10             
   click element                      //div[@class='div-menu']/div[@class='row']/div[contains(.,'Retail Sales')]
   Wait Until Page Contains           รายการหลัก
   Capture Page Screenshot

   Wait Until Element Is Visible      //div[@class='div-menu']/div[@class='row']/div[contains(.,'Bill Payment')]     10      
   click element                      //div[@class='div-menu']/div[@class='row']/div[contains(.,'Bill Payment')] 
   Wait Until Page Contains           รายการหลัก
   Capture Page Screenshot

   Wait Until Element Is Visible      //div[@class='div-menu']/div[@class='row']/div[contains(.,'Tax Invoice Management')]   10        
   click element                      //div[@class='div-menu']/div[@class='row']/div[contains(.,'Tax Invoice Management')] 
   Wait Until Page Contains           รายการหลัก
   Capture Page Screenshot

   Wait Until Element Is Visible      //div[@class='div-menu']//div[@class='list-view-item']/div[.='ออกรายงานREPORT']          10
   click element                      //div[@class='div-menu']//div[@class='list-view-item']/div[.='ออกรายงานREPORT']   
   Wait Until Page Contains           ออกรายงาน
   Sleep          2                   
   Capture Page Screenshot
   Click element                           ${Back_Button}

   Wait Until Element Is Visible      //div[@class='div-menu']//div[@class='list-view-item']/div[.='การจัดการร้านShop Administration']     10       
   click element                      //div[@class='div-menu']//div[@class='list-view-item']/div[.='การจัดการร้านShop Administration']  
   Wait Until Page Contains           การจัดการร้าน
   Sleep          2  
   Capture Page Screenshot
   Click element                      ${Back_Button}

   Wait Until Element Is Visible      //span[.='Cash Management']          10
   click element                      //span[.='Cash Management'] 
   Wait Until Page Contains           การบริหารการเงิน
   Sleep          2  
   Capture Page Screenshot
   Click element                      ${Back_Button}

   
Check Menu Shop Dealer Page by Cashier
   Shop Dealer SD119 Page by Cashier
   Wait Until Element Is Visible      //div[@class='div-menu']//div[@class='D-5']/div[1]/div[.='ขายสินค้า และบริการRetail Sales']              
   click element                      //div[@class='div-menu']//div[@class='D-5']/div[1]/div[.='ขายสินค้า และบริการRetail Sales']
   Wait Until Page Contains           ขายสินค้า และบริการ
   Sleep          2  
   Capture Page Screenshot
   Click element                      ${Back_Button}

   Wait Until Element Is Visible      //div[@class='div-menu']//div[@class='D-5']/div[2]/div[.='ชำระค่าบริการBill Payment']           
   click element                      //div[@class='div-menu']//div[@class='D-5']/div[2]/div[.='ชำระค่าบริการBill Payment']
   Wait Until Page Contains           ชำระค่าบริการ
   Sleep          2  
   Capture Page Screenshot
   Click element                      ${Back_Button}

   Wait Until Element Is Visible      //div[.='การแก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี']          
   click element                      //div[.='การแก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี']
   Wait Until Page Contains           การแก้ไขใบเสร็จรับเงิน/ใบกำกับภาษี
   Sleep          2  
   Capture Page Screenshot
   Click element                      ${Back_Button}

   Wait Until Element Is Visible      //div[.='ออกรายงาน']          
   click element                      //div[.='ออกรายงาน']  
   Wait Until Page Contains           ออกรายงาน
   Sleep    2
   Capture Page Screenshot
   Click element                      ${Back_Button}

   Wait Until Element Is Visible      //*[@id="root"]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div[2]          
   Wait Until Page Contains           รายการหลัก
   Capture Page Screenshot
  
   Wait Until Element Is Visible      //span[.='Cash Management']          
   click element                      //span[.='Cash Management'] 
   Wait Until Page Contains           การบริหารการเงิน
   Sleep          2  
   Capture Page Screenshot
   Click element                      ${Back_Button}



# -----นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน) แต่ใช้ User Manager-----#
Goto Send Cash Close Shitf by Manager [Nagative]
  Shop Dealer SD119 Page by Manager
  Send Cash Close Shitf by Manager
  Wait Until Page Contains        ท่านไม่สามารถเข้าสู่ระบบเพื่อนำส่งเงินได้ เนื่องจากไม่ได้รับสิทธิ์ในการนำส่งเงินตอนปิดกะ 
  Sleep    1
  Capture Page Screenshot
  Click element                           ${OK_Button} 


# -----นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)แต่ใช้ User Cashier-----#
Goto Send Cash Endday by Cashier [Nagative]
  Shop Dealer SD119 Page by Manager
  Send Cash Endday by Cashier
  Wait Until Page Contains        ท่านไม่สามารถเข้าสู่ระบบเพื่อนำส่งเงินตอนสิ้นวันได้ เนื่องจากไม่ได้รับสิทธิ์ในการนำส่งเงินตอนสิ้นวัน  
  Sleep    1
  Capture Page Screenshot
  Click element                           ${OK_Button} 



# -----ปิดร้าน (โดยผู้จัดการร้าน) แต่ใช้ User Cashier-----#
Goto Close Shop Dealer by Cashier
   Close Shop Dealer by Cashier




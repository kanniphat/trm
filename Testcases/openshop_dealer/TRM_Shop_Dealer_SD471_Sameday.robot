﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         OperatingSystem

Resource        ..\\..\\Keywords\\Keywords_Openshop_dealer.robot
Resource        ..\\..\\Variables\\Variables_Openshop_dealer_SD471.robot
# Test Teardown   close browser
                

*** Test Cases ***

# -----ตรวจสอบว่าร้านยังเปิดอยู่ ในกรณีที่ร้านของเมื่อวานยังไม่ได้ปิด ต้องปิดร้านของเมื่อวานก่อน-----#
Check Shop Dealer Still Opened by Manager
  Shop Dealer SD119 Page by Manager   
  Check Shop Dealer Still Opend 



# # -----นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)  ถ้าปิดกะภายในวันต้องไปปิดกะก่อน-----#
# Goto Send Cash before Close Shitf by Cashier
#   Shop Dealer SD119 Page by Manager
#   Send Cash before Close Shitf by Cashier



# # -----ปิดกะ (โดยพนักงานรับเงิน) -----#
# Goto Close Shitf by Cashier
#       Close Shitf by Cashier



# # -----นำส่งเงินตอนปิดกะ (โดยพนักงานรับเงิน)-----#
# Goto Send Cash Close Shitf by Cashier
#   Shop Dealer SD119 Page by Manager
#   Send Cash Close Shitf by Cashier





# # -----นำส่งเงินตอนสิ้นวัน(โดยผู้จัดการร้าน)-----#
# Goto Send Cash Endday by Manager
#   Shop Dealer SD119 Page by Manager
#   Send Cash Endday by Manager




# # -----ปิดร้าน (โดยผู้จัดการร้าน)-----#
# Goto Close Shop Dealer by Manager
#    Close Shop Dealer Sameday by Manager



# # -----เปิดร้าน (โดยผู้จัดการร้าน)-----#
# Goto Open Shop Dealer by Manager
#     Open Shop Dealer by Manager


# # -----เปิดกะ (โดยพนักงานรับเงิน)-----#
# Goto Open Shitf by Cashier
#     Open Shitf by Cashier


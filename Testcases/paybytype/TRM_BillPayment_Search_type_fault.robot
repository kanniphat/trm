*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Suite Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Search_type_fault 1
    Goto Bill Payment Page PRE_Production Shop_10             
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    FOR    ${index}    IN RANGE    2
           Press Special Key        C_DOWN
    END
    
    Capture Page Screenshot
    press key                           ${SEARCH_FIELD}         \\13
    sleep   3s
    Capture Page Screenshot
    Page should contain                 ไม่พบข้อมูลที่ค้นหา
    Page should contain                 กรุณาค้นหาใหม่อีกครั้ง
    
   


Search_type_fault 2
              
    Selenium2Library.input text         ${SEARCH_FIELD}        700038557
    FOR    ${index}    IN RANGE    2
           Press Special Key        C_DOWN
    END
    
    Capture Page Screenshot
    press key                           ${SEARCH_FIELD}         \\13
    sleep   3s
    Capture Page Screenshot
    Page should contain                 ไม่พบข้อมูลที่ค้นหา
    Page should contain                 กรุณาค้นหาใหม่อีกครั้ง
    
   

Search_type_fault 3
              
    Selenium2Library.input text         ${SEARCH_FIELD}        200050233
    FOR    ${index}    IN RANGE    2
           Press Special Key        C_DOWN
    END
    
    Capture Page Screenshot
    press key                           ${SEARCH_FIELD}         \\13
    sleep   3s
    Capture Page Screenshot
    Page should contain                 ไม่พบข้อมูลที่ค้นหา
    Page should contain                 กรุณาค้นหาใหม่อีกครั้ง
    


Search_type_fault 4
               
    Selenium2Library.input text         ${SEARCH_FIELD}       130004100073
    FOR    ${index}    IN RANGE    1
           Press Special Key        C_DOWN
    END
    
    Capture Page Screenshot
    press key                           ${SEARCH_FIELD}         \\13
    sleep   3s
    Capture Page Screenshot
    Page should contain                 ไม่พบข้อมูลที่ค้นหา
    Page should contain                 กรุณาค้นหาใหม่อีกครั้ง
    



Search_type_fault 5
                
    Selenium2Library.input text         ${SEARCH_FIELD}        2400487
    FOR    ${index}    IN RANGE    1
           Press Special Key        C_DOWN
    END
    
    Capture Page Screenshot
    press key                           ${SEARCH_FIELD}         \\13
    sleep   3s
    Capture Page Screenshot
    Page should contain                 ไม่พบข้อมูลที่ค้นหา
    Page should contain                 กรุณาค้นหาใหม่อีกครั้ง
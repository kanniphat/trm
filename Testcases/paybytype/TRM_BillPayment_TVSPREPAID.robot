*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
TVSNO

    Goto Bill Payment Page PRE_Production Shop_10             
    Selenium2Library.input text         ${SEARCH_FIELD}        2400487
    Capture Page Screenshot
    Press Special Key        C_DOWN
    Press Special Key        C_DOWN
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       //tr[1]//div[@class='table-radio']  10s
    click element                       //tr[1]//div[@class='table-radio']
    Wait Until Element Is Visible       //button[@class='btn-submit full']  10s
    click element                       //button[@class='btn-submit full']
     

    # Wait Until Element Is Visible       ${close_alert}                      10s
    # click element                       ${close_alert} 
    # Wait Until Element Is Visible       //button[@type="button"][@class="btn btn-default"]      10s
    
    # click element         //button[@type="button"][@class="btn btn-default"] 
    # click element         //button[@type="button"][@class="btn btn-default"]   
    # Wait Until Element Is Visible       //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[5]/td[1]/div/span          10s
    # click element                       //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[5]/td[1]/div/span
   
  

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

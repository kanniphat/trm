﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser


*** Test Cases ***
# *********************************************************************************************************************************
Testcase Pay by Cash withholdingtax 1 Bill
    [tags]      1%

    Goto Bill Payment Page PRE_Production Shop_10
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_null}
    click element       ${checkbox_1} 
   
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text        ${gettextwithholdingtax} 
    ${totalonlytax}      Selenium2Library.Get Text        ${gettexttotalonlytax}
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Press Special Key       ENTER

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${tax}
    # Should Contain    ${data}    ปวีณา
    # Should Contain    ${data}   ${totalonlytax} 
    # sleep   4s
# *********************************************************************************************************************************
Testcase Pay by Cash withholdingtax 3 Bill
    [tags]      1%

    Goto Bill Payment Page 
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_null}
    click element       ${checkbox_1} 
    click element       ${checkbox_2} 
    click element       ${checkbox_3} 
   
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}          Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Press Special Key       ENTER

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${tax}
    # Should Contain    ${data}    ปวีณา
    # Should Contain    ${data}   ${totalonlytax}
    # sleep   4s 
# *********************************************************************************************************************************

Testcase Pay by Cash withholdingtax 5 Bill
    [tags]      1%

    Goto Bill Payment Page 
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_null}
    click element       ${checkbox_1} 
    click element       ${checkbox_2} 
    click element       ${checkbox_3}
    click element       ${checkbox_4}
    click element       ${checkbox_5} 
   
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}          Selenium2Library.Get Text             ${gettextwithholdingtax} 
    ${totalonlytax}      Selenium2Library.Get Text        ${gettexttotalonlytax}
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Press Special Key       ENTER

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${tax}
    # Should Contain    ${data}    ปวีณา
    # Should Contain    ${data}   ${totalonlytax}
    # sleep   4s 
# *********************************************************************************************************************************
   
Testcase Pay by Cash withholdingtax 1 Bill tax3-5%
    [tags]      3-5%

    Goto Bill Payment Page 
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_null}
    click element       ${checkbox_1} 
   
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text        ${gettextwithholdingtax} 
    ${totalonlytax}      Selenium2Library.Get Text        ${gettexttotalonlytax}
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Press Special Key       ENTER

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${tax}
    # Should Contain    ${data}    ปวีณา
    # Should Contain    ${data}   ${totalonlytax} 
    # sleep   4s 
# *********************************************************************************************************************************

Testcase Pay by Cash withholdingtax 3 Bill tax3-5%
   [tags]      3-5%

    Goto Bill Payment Page 
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_null}
    click element       ${checkbox_1} 
    click element       ${checkbox_2} 
    click element       ${checkbox_3} 
   
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}          Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Press Special Key       ENTER

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${tax}
    # Should Contain    ${data}    ปวีณา
    # Should Contain    ${data}   ${totalonlytax}
    # sleep   4s 
# ********************************************************************************************************************************* 

Testcase Pay by Cash withholdingtax 5 Bill tax3-5%
    [tags]      3-5%

    Goto Bill Payment Page 
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_null}
    click element       ${checkbox_1} 
    click element       ${checkbox_2} 
    click element       ${checkbox_3}
    click element       ${checkbox_4}
    click element       ${checkbox_5} 
   
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}          Selenium2Library.Get Text             ${gettextwithholdingtax} 
    ${totalonlytax}      Selenium2Library.Get Text        ${gettexttotalonlytax}
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Press Special Key       ENTER

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${tax}
    # Should Contain    ${data}    ปวีณา
    # Should Contain    ${data}   ${totalonlytax}
    # sleep   4s 
# *********************************************************************************************************************************


   
                                                      
    


﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay by Cash withholdingtax 2 Ban 2 Bill 
    [tags]      1%

    Goto Bill Payment Page PRE_Production Shop_10           
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${checkbox_1} 
  
    Selenium2Library.input text         ${editcashbill}             200
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}

    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s

    
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${input_amount}
    # Should Contain    ${data}    ปวีณา
    # sleep   4s 
    
# ************************************************************************************************************************************* 

Testcase Pay by Cash withholdingtax 2 Ban 5 Bill 
    [tags]      1%

    Goto Bill Payment Page            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${checkbox_1} 
    click element                       ${checkbox_3}
    click element                       ${checkbox_5}
    click element                       ${checkbox_8}
  
    Selenium2Library.input text         ${editcashbill}             200
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}

    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s

    
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${input_amount}
    # Should Contain    ${data}    ปวีณา
    # sleep   4s  
    
# ************************************************************************************************************************************* 

Testcase Pay by Cash withholdingtax 2 Ban 8 Bill 
    [tags]      3-5%

    Goto Bill Payment Page            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${checkbox_1} 
    click element                       ${checkbox_2}
    click element                       ${checkbox_3}
    click element                       ${checkbox_4}
    click element                       ${checkbox_5}
    click element                       ${checkbox_6}
    click element                       ${checkbox_7}
  
    Selenium2Library.input text         ${editcashbill}             200

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}

    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s

    
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${input_amount}
    # Should Contain    ${data}    ปวีณา
    # sleep   4s   
    
# ************************************************************************************************************************************* 
Testcase Pay by Cash withholdingtax 2 Ban 10 Bill 
    [tags]      3-5%

    Goto Bill Payment Page            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${checkbox_1} 
    click element                       ${checkbox_2}
    click element                       ${checkbox_3}
    click element                       ${checkbox_4}
    click element                       ${checkbox_5}
    click element                       ${checkbox_6}
    click element                       ${checkbox_7}
    click element                       ${checkbox_8}
    click element                       ${checkbox_9}
  
    Selenium2Library.input text         ${editcashbill}             200
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}

    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s

    
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${input_amount}
    # Should Contain    ${data}    ปวีณา
    # sleep   4s

# *************************************************************************************************************************************    
    
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\Variables\\Variables_Billpayment.robot

Test Setup      set selenium speed          0.4s
Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************

Testcase Pay by Credit Card Other Bank 2Ban 4Bill 
   
    Goto Bill Payment Page PRE_Production Shop_10            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element   ${checkbox_1}
    click element   ${checkbox_2}
    click element   ${checkbox_3}
    Selenium2Library.Input Text         ${total_amount}        2800 

# ************************* Click Credit card *************************************
    click element                    ${creditcard_option} 
    click element                    ${creditcard_button} 
    sleep   2s
    click element                    ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}            4999999999999999      
    Selenium2Library.input text      ${input_secretnumber}            872
    ${BankName1}             Get Value               ${BankName}   
     should be Equal     ${BankName1}      Other Bank
# ************************************************************************************     
    click element   ${print_button} 
    sleep       4s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
   
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ปวีณา 
    Sleep   4s
# *************************************************************************************************************************************

Testcase Pay by Credit Card KBANK 2Ban 6Bill 

    Goto Bill Payment Page            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element   ${checkbox_1}
    click element   ${checkbox_2}
    click element   ${checkbox_3}
    click element   ${checkbox_4}
    click element   ${checkbox_8}
    
    Selenium2Library.Input Text         ${total_amount}        8500 

# ************************* Click Credit card *************************************
    click element   ${creditcard_option} 
    click element   ${creditcard_button}
    sleep   2s
    click element   ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}         4921418000471099      
    Selenium2Library.input text      ${input_secretnumber}              569
    ${BankName1}             Get Value               ${BankName}   
    should be Equal     ${BankName1}      ธนาคารกสิกรไทย
# ************************************************************************************     
    click element   ${print_button} 
    sleep       4s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ปวีณา 
    Sleep   4s


# *************************************************************************************************************************************
Testcase Pay by Credit Card KTC 2Ban 7Bill 

    Goto Bill Payment Page            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element   ${checkbox_1}
    click element   ${checkbox_2}
    click element   ${checkbox_3}
    click element   ${checkbox_4}
    click element   ${checkbox_8}
    click element   ${checkbox_9}
    Selenium2Library.input text         ${editcashbill}             200

# ************************* Click Credit card *************************************
    click element   ${creditcard_option}
    click element   ${creditcard_button} 
    sleep   2s
    click element   ${close_popup_creditcard} 
    Selenium2Library.input text      ${input_creditnumber}       5407169900989900     
    Selenium2Library.input text      ${input_secretnumber}               888
    ${BankName1}             Get Value               ${BankName}   
    should be Equal     ${BankName1}      ธนาคารกรุงไทย
# ************************************************************************************     
    click element   ${print_button} 
    sleep       4s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       4s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
   
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ปวีณา 
    Sleep   4s
# *************************************************************************************************************************************


Testcase Pay by Credit Card CITYBANK 2Ban 9Bill  

    Goto Bill Payment Page            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element   ${checkbox_1}
    click element   ${checkbox_2}
    click element   ${checkbox_3}
    click element   ${checkbox_4}
    click element   ${checkbox_5}
    click element   ${checkbox_6}
    click element   ${checkbox_7}
    click element   ${checkbox_8}
    Selenium2Library.input text         ${editcashbill}             900
# ************************* Click Credit card *************************************
    click element   ${creditcard_option}
    click element   ${creditcard_button} 
    sleep   2s
    click element   ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}       4386799099999990     
    Selenium2Library.input text      ${input_secretnumber}              999
    ${BankName1}             Get Value               ${BankName}   
    should be Equal     ${BankName1}      ธนาคารซิตี้แบงก์ เอ็น.เอ.
# ************************************************************************************     
    click element   ${print_button} 
    sleep       4s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ปวีณา 
    Sleep   4s
# *************************************************************************************************************************************






















































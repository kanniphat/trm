*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         OperatingSystem
Library         DatabaseLibrary


*** Variables***
 ${G_DB_DBNamer}                TRMUAT 
 ${G_DB_UserName}               trmowner
 ${G_DB_Password}               freewill
 ${G_Mysql_Server_IP}           172.19.192.114
 ${G_Mysql_Server_PORT}         1553     

                
*** Test Cases ***
Testcase DB1

    # Connect To Database  cx_Oracle  ${G_DB_DBNamer}   ${G_DB_UserName}  ${G_DB_Password}   ${G_Mysql_Server_IP}  ${G_Mysql_Server_PORT}          
    # ${queryResults}    Query  select d.customerfirstname from TRMOWN201903.document d where documentNO = 'ICG100845'        
    # ${queryResults}          Set Variable     ${queryResults[0][0]}
    # ${queryResults1}         Decode Bytes To String        ${queryResults}    	iso-8859-11                
    # Log to console              ${queryResults1}


    connect to database using custom params  cx_Oracle  'trmowner/freewill@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.19.192.114)(PORT=1553))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=TRMUAT)))'       
    ${queryResults}    Query  select d.documentno,d.customertitlename, d.customerfirstname,d.customeraddress, d.totalamount,d.cashiername from TRMOWN201905.document d where documentNO = 'SAL100043'      
    
    ${queryResults0}         Set Variable                 ${queryResults[0][0]}
    ${queryResults0}         Decode Bytes To String       ${queryResults0}    	    iso-8859-11  

    ${queryResults1}         Set Variable                 ${queryResults[0][1]}
    ${queryResults1}         Decode Bytes To String       ${queryResults1}     	    iso-8859-11

    ${queryResults2}         Set Variable                 ${queryResults[0][2]}
    ${queryResults2}         Decode Bytes To String       ${queryResults2}     	    iso-8859-11

    ${queryResults3}         Set Variable                 ${queryResults[0][3]}
    ${queryResults3}         Decode Bytes To String       ${queryResults3}     	    iso-8859-11

    ${queryResults4}           Set Variable                 ${queryResults[0][4]}
    ${queryResults4}           Convert To Number            ${queryResults4}            
    ${queryResults4}           Evaluate        "%.2f" % ${queryResults4} 
    

    ${queryResults5}         Set Variable                 ${queryResults[0][5]}
    ${queryResults5}         Decode Bytes To String       ${queryResults5}     	    iso-8859-11

    Log               ${queryResults0}
    Log               ${queryResults1}
    Log               ${queryResults2}
    Log               ${queryResults3}
    Log               ${queryResults4}
    Log               ${queryResults5}





#   -------------------------------------------------------Verify  Database-------------------------------------------------------------------------------------------   
#    should be equal      ${queryResults0} 
#    should be equal      ${queryResults1}  
#    should be equal      ${queryResults2}
#    should be equal      ${queryResults3}
#    should be equal      ${queryResults4}
#    should be equal      ${queryResults5}







 
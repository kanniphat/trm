﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase verify info button 
    Goto Bill Payment Page PRE_Production Shop_10           
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_21}
    click element       ${checkbox_1}
    click element       ${checkbox_16}
    click element       ${checkbox_18} 


# ----------------------------------------------Verify Info Button 1-----------------------------------------------------------------------

    click element       //tr[1]//button[@class='btn btn-xs btn-default']
    ${attapasri}        Selenium2Library.Get text        css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(1) > td:nth-child(2)
    Should be equal       ${attapasri}                       7

     ${price_before_tax}        Selenium2Library.Get text             css=table.bill-list > tr:nth-of-type(1) > td:nth-of-type(9)
     ${price_before_tax}        Convert To Number                     ${price_before_tax}    2
     ${price_before_tax1}       Evaluate         ${price_before_tax}/1.07
     ${price_before_tax1}       Evaluate        "%.2f" % ${price_before_tax1}
     ${price_before_tax2}       Selenium2Library.Get text        css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(2) > td:nth-child(2)
     Should be equal       ${price_before_tax1}           ${price_before_tax2} 

    ${tax1}        Selenium2Library.Get text        css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(3) > td:nth-child(2) 
    ${tax1}        Convert To String        ${tax1}   
    ${tax2}            Evaluate  ${price_before_tax}- ${price_before_tax1} 
    ${tax2}        Convert To String       ${tax2} 
    Should Match         ${tax1}      ${tax2}

    ${period_date}        Selenium2Library.Get text        css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(4) > td:nth-child(2)
    Should be equal       ${period_date}           12/06/2017  
    ${start_date}        Selenium2Library.Get text        css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(5) > td:nth-child(2)
    Should be equal       ${start_date}           01/05/2017  
    ${end_date}        Selenium2Library.Get text        css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(6) > td:nth-child(2)
    Should be equal       ${end_date}            31/05/2017 
   ${receipt_number}       Selenium2Library.Get text        css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(8) > td:nth-child(2)
    Should be equal        ${receipt_number}            070520171900060202
    ${Invoice Type}     Selenium2Library.Get text         css=#popover-362789927445110002052070520171900060202 > div.popover-content > table > tbody > tr:nth-child(9) > td:nth-child(2)
    Should be equal     ${Invoice Type}                 BILL
    # click element       //tr[1]//button[@class='btn btn-xs btn-default']
    # Selenium2Library.input text    //table[@class='table bill-list react-draggable']/tr[1]//input[@class='text-input']                ทดสอบพิมพ์หมายเหตุช่องที่หนึ่ง





# ----------------------------------------------Verify Info Button 2-----------------------------------------------------------------------
  
    click element       //tr[16]//button[@class='btn btn-xs btn-default']
    ${attapasri}        Selenium2Library.Get text        css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(1) > td:nth-child(2)
    Should be equal       ${attapasri}                       7

     ${price_before_tax}        Selenium2Library.Get text             css=table.bill-list > tr:nth-of-type(16) > td:nth-of-type(9)
     ${price_before_tax}        Convert To Number                     ${price_before_tax}    2
     ${price_before_tax1}       Evaluate         ${price_before_tax}/1.07
     ${price_before_tax1}       Evaluate        "%.2f" % ${price_before_tax1}
     ${price_before_tax2}        Selenium2Library.Get text        css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(2) > td:nth-child(2)
     Should be equal       ${price_before_tax1}           ${price_before_tax2} 

    ${tax1}        Selenium2Library.Get text        css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(3) > td:nth-child(2) 
    ${tax1}        Convert To String        ${tax1}   
    ${tax2}            Evaluate  ${price_before_tax}- ${price_before_tax1} 
    ${tax2}        Convert To String       ${tax2} 
    Should Match         ${tax1}      ${tax2}

    ${period_date}        Selenium2Library.Get text        css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(4) > td:nth-child(2)
    Should be equal       ${period_date}           12/11/2018 
    ${start_date}        Selenium2Library.Get text        css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(5) > td:nth-child(2)
    Should be equal       ${start_date}           	01/10/2018  
    ${end_date}        Selenium2Library.Get text        css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(6) > td:nth-child(2)
    Should be equal       ${end_date}         31/10/2018  
    ${receipt_number}       Selenium2Library.Get text        css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(8) > td:nth-child(2)
    Should be equal        ${receipt_number}            071020181900105718  
    ${Invoice Type}     Selenium2Library.Get text         css=#popover-362789927445110002052071020181900105718 > div.popover-content > table > tbody > tr:nth-child(9) > td:nth-child(2)
    Should be equal     ${Invoice Type}                 BILL
    # click element       //tr[16]//button[@class='btn btn-xs btn-default']
    # Selenium2Library.input text    //table[@class='table bill-list react-draggable']/tr[16]//input[@class='text-input']                ทดสอบพิมพ์หมายเหตุช่องที่สิบหก

# ----------------------------------------------Verify Info Button 3-----------------------------------------------------------------------

    click element       //tr[18]//button[@class='btn btn-xs btn-default']
    ${attapasri}        Selenium2Library.Get text        css=#popover-362789927445110002052071220181900112318> div.popover-content > table > tbody > tr:nth-child(1) > td:nth-child(2)
    Should be equal       ${attapasri}                       7

     ${price_before_tax}        Selenium2Library.Get text             css=table.bill-list > tr:nth-of-type(18) > td:nth-of-type(9)
     ${price_before_tax}        Convert To Number                     ${price_before_tax}    
     ${price_before_tax1}       Evaluate         ${price_before_tax}/1.07
     ${price_before_tax1}       Evaluate        "%.2f" % ${price_before_tax1}
     ${price_before_tax2}        Selenium2Library.Get text        css=#popover-362789927445110002052071220181900112318> div.popover-content > table > tbody > tr:nth-child(2) > td:nth-child(2)
     Should be equal       ${price_before_tax1}           ${price_before_tax2} 

    ${tax1}        Selenium2Library.Get text        css=#popover-362789927445110002052071220181900112318 > div.popover-content > table > tbody > tr:nth-child(3) > td:nth-child(2) 
    ${tax1}        Convert To String        ${tax1}   
    ${tax2}            Evaluate  ${price_before_tax}- ${price_before_tax1} 
    ${tax2}        Convert To String       ${tax2} 
    Should Match         ${tax1}      ${tax2}

    ${period_date}        Selenium2Library.Get text        css=#popover-362789927445110002052071220181900112318 > div.popover-content > table > tbody > tr:nth-child(4) > td:nth-child(2)
    Should be equal       ${period_date}          	12/01/2019  
    ${start_date}        Selenium2Library.Get text        css=#popover-362789927445110002052071220181900112318 > div.popover-content > table > tbody > tr:nth-child(5) > td:nth-child(2)
    Should be equal       ${start_date}           	01/12/2018 
    ${end_date}        Selenium2Library.Get text        css=#popover-362789927445110002052071220181900112318 > div.popover-content > table > tbody > tr:nth-child(6) > td:nth-child(2)
    Should be equal       ${end_date}           	31/12/2018
    ${receipt_number}         Selenium2Library.Get text        css=#popover-362789927445110002052071220181900112318 > div.popover-content > table > tbody > tr:nth-child(8) > td:nth-child(2)
    Should be equal        ${receipt_number}            071220181900112318 
    ${Invoice Type}     Selenium2Library.Get text         css=#popover-362789927445110002052071220181900112318 > div.popover-content > table > tbody > tr:nth-child(9) > td:nth-child(2)
    Should be equal     ${Invoice Type}                 BILL
    # click element       //tr[18]//button[@class='btn btn-xs btn-default']
    # Selenium2Library.input text    //table[@class='table bill-list react-draggable']/tr[18]//input[@class='text-input']                ทดสอบพิมพ์หมายเหตุช่องที่สิบแปด

# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${input_amount}
    # Should Contain    ${data}    ปวีณา
   

# ------------------------------------------------------------------------------------------------------------------------------------
    












 
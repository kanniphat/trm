﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase verify edit customer
    Goto Bill Payment Page PRE_Production Shop_10            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    

#  --------------------------------------------------------edit customer-----------------------------------------------------------------   
    click element                       ${edit_customer_button}
    click element                       css=.Select-control
    Press Special Key           	    C_DOWN   
    Press Special Key           	    ENTER
    
    Selenium2Library.input text         css=input[maxlength='255']              สำราญโภคทรัพย์ มั่งมีศรีรัตนโนรมย์  
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='00000']                47904
    Selenium2Library.input text         css=input[maxlength='55']               108/45
    Selenium2Library.input text         css=input[maxlength='30']               27
    Selenium2Library.input text         css=input[maxlength='60']               พญาคชสาร
    Selenium2Library.input text         css=input[maxlength='110']              ตึกทุเรียน
    Selenium2Library.input text         css=div.floor .form-input               5
    Selenium2Library.input text         css=div.room_no .form-input             687
    Selenium2Library.input text         css=div.soi .form-input                 98
    Selenium2Library.input text         css=div.street .form-input              ธรรมชาติ
    click element                       css=div.all-input-wrapper div:nth-of-type(1) > .form-value
    Selenium2Library.input text         //input[@class='form-control']          เชียงใหม่
    sleep           3s
    Press Special Key           	    C_DOWN
    Press Special Key           	    C_DOWN
    Press Special Key           	    ENTER
    click Button                       //button[@class='btn-submit full']



  ${Name}     Selenium2Library.Get text         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div[1]
  should contain                ${Name}     สำราญโภคทรัพย์


  ${address}    Selenium2Library.Get text       //div[@class='D-4 M-10']//div[@class='dt-value break-word']
  should contain   ${address}   เชียงใหม่    
  should contain   ${address}   ตึกทุเรียน

# -------------------------------------------------------------------------------------------------------------------------------------------------------
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${input_amount}
    # Should Contain    ${data}    ปวีณา
    

# ------------------------------------------------------------------------------------------------------------------------------------
    








 
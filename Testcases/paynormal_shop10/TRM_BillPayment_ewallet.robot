﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Test Setup      set selenium speed  0.2s
Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Pay by e-Wallet success

    Goto Bill Payment Page PRE_Production Shop_10          
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}          10s
    click element                       ${close_alert}
#     click element   ${checkbox_null}
#     click element   ${checkbox_1}
    Wait Until Element Is Visible       ${editcashbill2}             10s
    Selenium2Library.input text         ${editcashbill2}             200



# ************************* Click e-Wallet *************************************
    click element                    ${creditcard_option} 
    click element                    ${creditcard_button} 
    sleep   2s
    click element                       ${close_popup_creditcard}
    Wait Until Element Is Visible       ${input_creditnumber} 


#********************************รอกรอกรหัสด้วยตนเอง 100 วินาที****************************************************                     
    Wait Until Page Does Not Contain       กรุณากรอกตัวเลขให้ครบ 16 หลัก             100s 
    Wait Until Page Does Not Contain       กรุณากรอกข้อมูล                          100s
    sleep   2     
#************************************************************************************     
  
    Click        C:\\TRM\\sikulipicture\\pic_print_receipt.png
    sleep       10
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf      
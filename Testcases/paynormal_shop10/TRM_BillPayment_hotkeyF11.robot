﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

# Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay by Cash F11 2times

    Goto Bill Payment Page PRE_Production Shop_10             
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${clear_checkbox}
    click element       ${clear_checkbox}
    click element       ${checkbox_2}
   
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}


#     FOR    ${index}    IN RANGE     2
#            Press Special Key        F11
#     END

    
#     sleep       3s
#     SikuliLibrary.Press Special Key           ENTER
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 

#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
#     sleep   4S
 

# # *************************************************************************************************************************************
 
# Testcase Pay by Cash F11 4times

#     Goto Bill Payment Page PRE_Production Shop_10              
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element       ${clear_checkbox}
#     click element       ${clear_checkbox}
#     click element       ${checkbox_2} 
   
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}


#     FOR    ${index}    IN RANGE     4
#            Press Special Key        F11
#     END

    
#     sleep       3s
#     SikuliLibrary.Press Special Key           ENTER
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 

#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
#     sleep   4S

# # *************************************************************************************************************************************
 
# # Testcase Pay by Cash F11 8times

# #     Goto Bill Payment Page PRE_Production Shop_10              
# #     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
# #     press key                           ${SEARCH_FIELD}         \\13
# #     Wait Until Element Is Visible       ${close_alert}                      10s
# #     click element                       ${close_alert} 
# #     click element       ${clear_checkbox}
# #     click element       ${clear_checkbox}
# #     click element       ${checkbox_2}
   
# #     Wait Until Element Is Visible       ${total_amount}
# #     ${input_amount}       Get Value     ${total_amount}
# #     Wait Until Element Is Visible       ${fill_input_amount}
# #     Click Element                       ${fill_input_amount}
# #     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}


# #     FOR    ${index}    IN RANGE     8
# #            Press Special Key        F11
# #     END

    
# #     sleep       3s
# #     SikuliLibrary.Press Special Key           ENTER
    
# #     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
# #     Page should Contain         พิมพ์ใบเสร็จ 

# #     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
# #     Right Click     ${pic_acrobat} 
# #     Click           ${pic_close acrobat}      
# #     sleep       3s
# #     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

# #     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
# #     Log             ${receiptnumber[1]}
# #     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
# #     sleep   4S


# # # ************************************************************************************************************************************* 


 
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\Variables\\Variables_Billpayment.robot

# Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay by Cash 1 Bill
    
    Goto Bill Payment Page PRE_Production Shop_10  
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${checkbox_null}
    click element       ${checkbox_1} 
   
  
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    ${data}     Decode Bytes To String       ${data}       UTF-8 
    Should Contain    ${data}    ${receiptnumber[1]}  
    Should Contain    ${data}    ${input_amount}
    Should Contain    ${data}    ดวงกมล หงษ์ทอง 
    Sleep   4s

# # *************************************************************************************************************************************

# Testcase Pay by Cash 3 Bill

#     Goto Bill Payment Page shop10            
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element       ${checkbox_null}
#     click element       ${checkbox_1} 
#     click element       ${checkbox_2} 
#     click element       ${checkbox_3} 
    
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
#     click element   ${print_button} 
#     sleep       3s
#     SikuliLibrary.Press Special Key           ENTER
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
#     ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
#     ${data}     Decode Bytes To String       ${data}       UTF-8 
#     Should Contain    ${data}    ${receiptnumber[1]}  
#     Should Contain    ${data}    ${input_amount}
#     Should Contain    ${data}    ดวงกมล หงษ์ทอง
    

# # *************************************************************************************************************************************

# Testcase Pay by Cash 5 Bill

#     Goto Bill Payment Page shop10            
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element       ${checkbox_null}
#     click element       ${checkbox_1} 
#     click element       ${checkbox_2} 
#     click element       ${checkbox_3} 
#     click element       ${checkbox_4} 
#     click element       ${checkbox_5} 
  
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
#     click element   ${print_button} 
#     sleep       3s
#     SikuliLibrary.Press Special Key           ENTER
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
#     ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
#     ${data}     Decode Bytes To String       ${data}       UTF-8 
#     Should Contain    ${data}    ${receiptnumber[1]}  
#     Should Contain    ${data}    ${input_amount}
#     Should Contain    ${data}    ดวงกมล หงษ์ทอง
  
# # *************************************************************************************************************************************  
 





   
                                                      
    

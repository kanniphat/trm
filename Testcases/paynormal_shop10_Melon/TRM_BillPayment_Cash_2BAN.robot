﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***
# Testcase Pay by Cash 2 Ban 2 Bill

#     Goto Bill Payment Page PRE_Production Shop_10 Melon             
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element       ${clear_checkbox}
#     click element       ${clear_checkbox}
#     click element       ${checkbox_1} 
#     click element       ${checkbox_2} 
#     Selenium2Library.input text         ${editcashbill}             200
   
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
#     click element   ${print_button} 
#     sleep       4
#     ControlFocus    Save PDF File As       &Save          Button2
#     ControlClick    Save PDF File As       &Save          Button2
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

# # *************************************************************************************************************************************
# Testcase Pay by Cash 2 Ban 2 Bill edit bill

#     Goto Bill Payment Page PRE_Production Shop_10 Melon             
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element       ${clear_checkbox}
#     click element       ${clear_checkbox}
#     click element       ${checkbox_1} 
#     click element       ${checkbox_2} 
#     Selenium2Library.input text         ${editcashbill}             200
   
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
#     click element   ${print_button} 
#     sleep       4
#     ControlFocus    Save PDF File As       &Save          Button2
#     ControlClick    Save PDF File As       &Save          Button2
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 


# # ************************************************************************************************************************************* 

# Testcase Pay by Cash 2 Ban 2 Bill Pay advance

#     Goto Bill Payment Page PRE_Production Shop_10 Melon             
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element       ${clear_checkbox}
#     click element       ${clear_checkbox}
#     click element       ${checkbox_1} 
#     click element       ${checkbox_2} 
#     Selenium2Library.input text         ${editcashbill}             200
   
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        5000
    

#     ${fill_input_amount}    Convert To Integer     5000
#     ${change_cash_Beer}     Evaluate   ${fill_input_amount} - ${input_amount}
#     ${change_cash_system}   Selenium2Library.Get Text   ${change_cash_locator}
#     ${change_cash_Beer}     Convert To String           ${change_cash_Beer} 
#     ${change_cash_system}        Split String          ${change_cash_system}      ,
#     ${change_cash_system1}       set variable          ${change_cash_system[0]}${change_cash_system[1]}
#     ${change_cash_system1}       Convert To String     ${change_cash_system1}
#     Should Be Equal   ${change_cash_Beer}     ${change_cash_system1}   
    
#     click element   ${print_button} 
#      sleep       4
#     ControlFocus    Save PDF File As       &Save          Button2
#     ControlClick    Save PDF File As       &Save          Button2
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 



# # # # *************************************************************************************************************************************
# Testcase Pay by Cash 2 Ban 5 Bill Pay advance

#     Goto Bill Payment Page PRE_Production Shop_10 Melon             
#     Selenium2Library.input text         ${SEARCH_FIELD}        10002067
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element       ${clear_checkbox}
#     click element       ${clear_checkbox}
#     click element       ${checkbox_1} 
#     click element       ${checkbox_2}
#     click element       ${checkbox_3} 
#     click element       ${checkbox_4} 
#     click element       ${checkbox_5}  

#     Selenium2Library.input text         ${editcashbill}             200
   
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     ${input_amount}       Remove String         ${input_amount}         ,  
    

#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        9000
    

#     ${fill_input_amount}    Convert To Integer     9000
#     ${change_cash_Beer}     Evaluate   ${fill_input_amount} - ${input_amount}
#     ${change_cash_system}   Selenium2Library.Get Text   ${change_cash_locator}
#     ${change_cash_Beer}     Convert To String           ${change_cash_Beer} 
#     ${change_cash_system}        Split String          ${change_cash_system}      ,
#     ${change_cash_system1}       set variable          ${change_cash_system[0]}${change_cash_system[1]}
#     ${change_cash_system1}       Convert To String     ${change_cash_system1}
#     Should Be Equal   ${change_cash_Beer}     ${change_cash_system1}   
    
#     click element   ${print_button} 
#     sleep       4
#     ControlFocus    Save PDF File As       &Save          Button2
#     ControlClick    Save PDF File As       &Save          Button2
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 



# # # *************************************************************************************************************************************

Testcase Pay by Cash 2 Ban 2 Bill insufficient cash

    Goto Bill Payment Page PRE_Production Shop_10 Melon           
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element       ${clear_checkbox}
    click element       ${clear_checkbox}
    click element       ${checkbox_2} 
    
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        500
    Page Should Contain                 คุณจ่ายเงินยังไม่ครบ
    Wait Until Element Is Visible       //div[@class='D-10']
    Set Focus To Element                //div[@class='D-10']     
    sleep   1
    Capture Page Screenshot

# # *************************************************************************************************************************************






















   
                                                      
    

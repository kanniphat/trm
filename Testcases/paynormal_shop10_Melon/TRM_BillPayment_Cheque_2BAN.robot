﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay by cheque success 2Ban 2Bill

    Goto Bill Payment Page PRE_Production Shop_10 Melon          
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}          10s
    click element                       ${close_alert}
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    click element                       ${checkbox_2}
    Selenium2Library.input text         ${editcashbill}             200

# ************************* Pay by Cheque **********************************************************************   
    click element   css=div.tab-tax > label:nth-of-type(3)
    sleep   2s
    Selenium2Library.input text           css=input[maxlength='8']          12345678
    Selenium2Library.input text           css=input[maxlength='3']          001
    Selenium2Library.input text           css=.cheque-branch-no             0001
    Selenium2Library.input text           css=[placeholder='__/__/____']            02022019

# ****************************************************************************************************************  
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Wait Until Element Is Visible       ${close_alert}          10s
    click element                       ${close_alert}
    Sleep   4
# *************************************************************************************************************************************

Testcase Pay by cheque success 2Ban 5Bill

    Goto Bill Payment Page PRE_Production Shop_10 Melon          
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}          10s
    click element                       ${close_alert}
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    click element                       ${checkbox_2}
    click element                       ${checkbox_3}
    click element                       ${checkbox_4}
    click element                       ${checkbox_5}
    Selenium2Library.input text         ${editcashbill}             200

# ************************* Pay by Cheque **********************************************************************   
    click element   css=div.tab-tax > label:nth-of-type(3)
    sleep   2s
    Selenium2Library.input text           css=input[maxlength='8']          12345678
    Selenium2Library.input text           css=input[maxlength='3']          001
    Selenium2Library.input text           css=.cheque-branch-no             0001
    Selenium2Library.input text           css=[placeholder='__/__/____']            02022019

# ****************************************************************************************************************  
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Wait Until Element Is Visible       ${close_alert}          10s
    click element                       ${close_alert}
    Sleep   4
# *************************************************************************************************************************************
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************

Testcase Pay by Credit Card Other Bank

    Goto Bill Payment Page PRE_Production Shop_10 Melon            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    click element                       ${checkbox_2}
    Selenium2Library.input text         ${editcashbill}             200
 

# ************************* Click Credit card *************************************
    click element                    ${creditcard_option} 
    click element                    ${creditcard_button} 
    sleep   2s
    click element                    ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}            4999999999999999      
    Selenium2Library.input text      ${input_secretnumber}            872
    ${BankName1}             Get Value               ${BankName}   
     should be Equal     ${BankName1}      Other Bank
# ************************************************************************************     
    click element   ${print_button} 
    sleep       3
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2

    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    sleep       4
    
    
# *************************************************************************************************************************************

Testcase Pay by Credit Card KBANK      

    Goto Bill Payment Page PRE_Production Shop_10 Melon           
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    click element                       ${checkbox_2}
    Selenium2Library.input text         ${editcashbill}             200


# ************************* Click Credit card *************************************
    click element   ${creditcard_option} 
    click element   ${creditcard_button}
    sleep   2s
    click element   ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}         4921418000471099      
    Selenium2Library.input text      ${input_secretnumber}              569
    ${BankName1}             Get Value               ${BankName}   
    should be Equal     ${BankName1}      ธนาคารกสิกรไทย
# ************************************************************************************     
    click element   ${print_button} 
    sleep       3
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    sleep       4
    

# *************************************************************************************************************************************
Testcase Pay by Credit Card KTC

    Goto Bill Payment Page PRE_Production Shop_10 Melon            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    click element                       ${checkbox_2}
    Selenium2Library.input text         ${editcashbill}             200
 

# ************************* Click Credit card *************************************
    click element   ${creditcard_option}
    click element   ${creditcard_button} 
    sleep   2s
    click element   ${close_popup_creditcard} 
    Selenium2Library.input text      ${input_creditnumber}       5407169900989900     
    Selenium2Library.input text      ${input_secretnumber}               888
    ${BankName1}             Get Value               ${BankName}   
    should be Equal     ${BankName1}      ธนาคารกรุงไทย
# ************************************************************************************     
    click element   ${print_button} 
    sleep       3
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    sleep       4

# *************************************************************************************************************************************


Testcase Pay by Credit Card CITYBANK

    Goto Bill Payment Page PRE_Production Shop_10 Melon            
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    click element                       ${checkbox_2}
    Selenium2Library.input text         ${editcashbill}             200

# ************************* Click Credit card *************************************
    click element   ${creditcard_option}
    click element   ${creditcard_button} 
    sleep   2s
    click element   ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}       4386799099999990     
    Selenium2Library.input text      ${input_secretnumber}              999
    ${BankName1}             Get Value               ${BankName}   
    should be Equal     ${BankName1}      ธนาคารซิตี้แบงก์ เอ็น.เอ.
# ************************************************************************************     
    click element   ${print_button} 
    sleep       3
    ControlFocus      Save PDF File As       &Save          Button2
    ControlClick      Save PDF File As       &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    sleep       4
    
# *************************************************************************************************************************************






















































*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         OperatingSystem
Library         DatabaseLibrary


*** Variables***
 ${G_DB_DBNamer}                TRMUAT 
 ${G_DB_UserName}               trmowner
 ${G_DB_Password}               freewill
 ${G_Mysql_Server_IP}           172.19.192.114
 ${G_Mysql_Server_PORT}         1553     

                
*** Test Cases ***
Testcase DB Receipt 1
    connect to database using custom params  cx_Oracle  'trmowner/freewill@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.19.192.114)(PORT=1553))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=TRMUAT)))'       
    ${queryResults}    Query  select d.documentno, d.customerfirstname, d.totalamount,d.cashiername from TRMOWN201905.document d where documentNO = 'SAL100043'      
    

    ${queryResults0}         Set Variable                 ${queryResults[0][0]}
    ${queryResults0}         Decode Bytes To String       ${queryResults0}    	    iso-8859-11  

    ${queryResults1}         Set Variable                 ${queryResults[0][1]}
    ${queryResults1}         Decode Bytes To String       ${queryResults1}     	    iso-8859-11

  
    ${queryResults2}           Set Variable                 ${queryResults[0][2]}
    ${queryResults2}           Convert To Number            ${queryResults2}            
    ${queryResults2}           Evaluate                     "%.2f" % ${queryResults2} 
    
    ${queryResults3}         Set Variable                 ${queryResults[0][3]}
    ${queryResults3}         Decode Bytes To String       ${queryResults3}     	    iso-8859-11

    Log               ${queryResults0}
    Log               ${queryResults1}
    Log               ${queryResults2}
    Log               ${queryResults3}
    
#  -------------------------------------------------------------------------------------------------------------------------------------------- 

Testcase DB Receipt 2
    connect to database using custom params  cx_Oracle  'trmowner/freewill@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.19.192.114)(PORT=1553))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=TRMUAT)))'       
    ${queryResults}    Query  select d.documentno, d.customerfirstname, d.totalamount,d.cashiername from TRMOWN201905.document d where documentNO = 'SAL100044'      
    

    ${queryResults0}         Set Variable                 ${queryResults[0][0]}
    ${queryResults0}         Decode Bytes To String       ${queryResults0}    	    iso-8859-11  

    ${queryResults1}         Set Variable                 ${queryResults[0][1]}
    ${queryResults1}         Decode Bytes To String       ${queryResults1}     	    iso-8859-11

  
    ${queryResults2}           Set Variable                 ${queryResults[0][2]}
    ${queryResults2}           Convert To Number            ${queryResults2}            
    ${queryResults2}           Evaluate                     "%.2f" % ${queryResults2} 
    
    ${queryResults3}         Set Variable                 ${queryResults[0][3]}
    ${queryResults3}         Decode Bytes To String       ${queryResults3}     	    iso-8859-11

    Log               ${queryResults0}
    Log               ${queryResults1}
    Log               ${queryResults2}
    Log               ${queryResults3}
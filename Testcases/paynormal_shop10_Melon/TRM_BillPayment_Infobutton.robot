﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary


Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

# Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase verify info button 
    Goto Bill Payment Page PRE_Production Shop_10 Melon           
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_2}
# ----------------------------------------------Verify Info Button -----------------------------------------------------------------------

    click element           //tr[2]//button[@class='btn btn-xs btn-default']
    Capture Page Screenshot
    ${attapasri}            Selenium2Library.Get text        //td[.='7']
    Should be equal         ${attapasri}                       7
    Wait Until Element Is Visible                //input[@class='ipt-text']  
    ${price_before_tax}        Selenium2Library.Getvalue             //input[@class='ipt-text']
    ${price_before_tax}        Convert To Number                     ${price_before_tax}    2
    ${price_before_tax1}       Evaluate         ${price_before_tax}/1.07
    ${price_before_tax1}       Evaluate        "%.2f" % ${price_before_tax1}

    # Should be equal         ${price_before_tax}         ${price_before_tax1} 


    ${tax1}        Selenium2Library.Get text        //td[.='48.93']
    ${tax1}        Convert To String        ${tax1}   
    ${tax2}        Evaluate  ${price_before_tax}- ${price_before_tax1} 
    ${tax2}        Convert To String       ${tax2} 
    Should Match         ${tax1}      ${tax2}

    ${total_amount}         Evaluate    ${price_before_tax1}+${tax2}           
    Should be equal         ${price_before_tax}     ${total_amount}          




    ${period_date}        Selenium2Library.Get text         //td[.='14/04/2017']
    Should be equal       ${period_date}                    14/04/2017 
    ${start_date}         Selenium2Library.Get text         //td[.='01/03/2017']
    Should be equal       ${start_date}                     01/03/2017  
    ${end_date}           Selenium2Library.Get text        //td[.='31/03/2017']
    Should be equal       ${end_date}                      31/03/2017 
   ${receipt_number}      Selenium2Library.Get text        //td[.='070320171900055382']
    Should be equal       ${receipt_number}                070320171900055382
    ${Invoice Type}      Selenium2Library.Get text         //td[.='BILL']
    Should be equal      ${Invoice Type}                    BILL
  



 
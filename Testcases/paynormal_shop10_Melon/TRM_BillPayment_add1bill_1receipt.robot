﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         DatabaseLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase verify add bill
    Goto Bill Payment Page PRE_Production Shop_10 Melon 
         
#  --------------------------------------------------------add bill Page 1-----------------------------------------------------------------  

    click element                       //a[@class='btn btn-trmadd ']
    click element                       //div[@class='table-radio']
    selenium2Library.input text         css=[name='card']                 1103700432678
    click element                       css=.Select-placeholder
    FOR    ${index}    IN RANGE    23
           Press Special Key        C_DOWN
    END
    
    Press Special Key           	    ENTER
    sleep       2s
    click element                       //button[@class='btn-submit full']
# --------------------------------------------------------------------------------------------------------------------------------------    


# #  --------------------------------------------------------add bill Page 2 edit customer------------------------------------------------   
    
    click element                       css=.Select-control
    Press Special Key           	    C_DOWN   
    Press Special Key           	    ENTER
    
    Selenium2Library.input text         css=input[maxlength='255']              จักรพันธ์ นครนครา
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='.']                    47904
    Selenium2Library.input text         css=input[maxlength='55']               108/45
    Selenium2Library.input text         css=input[maxlength='30']               27
    Selenium2Library.input text         css=input[maxlength='60']               รุ่งกิจวิลลา
    Selenium2Library.input text         css=input[maxlength='110']              ใบหยก
    Selenium2Library.input text         css=div.floor .form-input               5
    Selenium2Library.input text         css=div.room_no .form-input             687
    Selenium2Library.input text         css=div.soi .form-input                 98
    Selenium2Library.input text         css=div.street .form-input              คนเดิน
    click element                       css=div.all-input-wrapper div:nth-of-type(1) > .form-value
    Selenium2Library.input text         //input[@class='form-control']          เชียงใหม่
    sleep           3s
    Press Special Key           	    C_DOWN
    Press Special Key           	    C_DOWN
    Press Special Key           	    ENTER
    click Button                       //button[@class='btn-submit full']
    sleep               2s


#  --------------------------------------------------------add bill Page 3 ----------------------------------------------- 
    click element                       css=.form-select
    Press Special Key           	    C_DOWN   
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.banNo .form-input                               10002067                                  
    Selenium2Library.input text         css=div.select-nopadding .form-input                    033681023IPI
    Press Special Key           	    ENTER
    Press Special Key           	    ENTER
    Selenium2Library.input text         css=div.box-body .invoiceNo > div > .form-input         123456678
    Selenium2Library.input text         css=div.startDate [placeholder='วัน/เดือน/ปี']             02/03/2019 
    Selenium2Library.input text         css=div.endDate [placeholder='วัน/เดือน/ปี']               31/03/2019 
    Selenium2Library.input text         css=div.D-offset-8 .form-input                          200
    click Button                       //button[@class='btn-submit full']

# --------------------------------------------------edit bill + print -----------------------------------------------------------------------------                   

    Selenium2Library.input text         ${editcashbill}             200
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    SikuliLibrary.Press Special Key           ENTER
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
     sleep       3s
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

 
    # ${data}     Convert Pdf To Txt              C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # ${data}     Decode Bytes To String       ${data}       UTF-8 
    # Should Contain    ${data}    ${receiptnumber[1]}  
    # Should Contain    ${data}    ${input_amount}
    # Should Contain    ${data}    ปวีณา
   
# --------------------------------------------------------- Database---------------------------------------------------------------------------
    

#     connect to database using custom params  cx_Oracle  'trmowner/freewill@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.19.192.114)(PORT=1553))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=TRMUAT)))'       
#     ${queryResults}    Query  select d.documentno,d.customertitlename, d.customerfirstname,d.customeraddress, d.totalamount,d.cashiername from TRMOWN201903.document d where documentNO = '${receiptnumber[1]}'      
    
#     ${queryResults0}         Set Variable                 ${queryResults[0][0]}
#     ${queryResults0}         Decode Bytes To String       ${queryResults0}    	    iso-8859-11  

#     ${queryResults1}         Set Variable                 ${queryResults[0][1]}
#     ${queryResults1}         Decode Bytes To String       ${queryResults1}     	    iso-8859-11

#     ${queryResults2}         Set Variable                 ${queryResults[0][2]}
#     ${queryResults2}         Decode Bytes To String       ${queryResults2}     	    iso-8859-11

#     ${queryResults3}         Set Variable                 ${queryResults[0][3]}
#     ${queryResults3}         Decode Bytes To String       ${queryResults3}     	    iso-8859-11

#     ${queryResults4}           Set Variable                 ${queryResults[0][4]}
#     ${queryResults4}           Convert To Number            ${queryResults4}            
#     ${queryResults4}           Evaluate        "%.2f" % ${queryResults4} 
    

#     ${queryResults5}         Set Variable                 ${queryResults[0][5]}
#     ${queryResults5}         Decode Bytes To String       ${queryResults5}     	    iso-8859-11

#     Log               ${queryResults0}
#     Log               ${queryResults1}
#     Log               ${queryResults2}
#     Log               ${queryResults3}
#     Log               ${queryResults4}
#     Log               ${queryResults5}



# #   -------------------------------------------------------Verify  Database-------------------------------------------------------------------------------------------   
#    should be equal      ${queryResults0}         ${receiptnumber[1]} 
#    should be equal      ${queryResults1}         คุณ
#    should be equal      ${queryResults2}         จักรพันธ์ นครนครา
#    should contain       ${queryResults3}         อาคาร ใบหยก ชั้น 5 ห้อง 687 หมู่บ้าน รุ่งกิจวิลลา 
#    should be equal      ${queryResults4}         ${input_amount} 
#    should contain       ${queryResults5}         ปวีณา  หาเส็น









 
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary
Library         enter.py


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
# Suite Teardown   close browser
                


*** Variables ***
${application}           C:\\Users\\Beer\\Desktop\\GenCode.exe
${Document_number1}        ALA1F0287
  


*** Test Cases ***

# *************************************************************************************************************************************
Cancel Cashcard Receipt 1
  Goto Cancel Sales PRE_Production Shop_10  
  Wait Until Element Is Visible             ${Input_document_number}          
  Selenium2Library.input text               ${Input_document_number}                ${Document_number1}
  Wait Until Element Is Visible             ${Find_document_cancel} 
  click element                             ${Find_document_cancel}
  Wait Until Element Is Visible     ${serial}  
  ${get_serial}           Selenium2Library.GetValue         ${serial}
  LOG     ${get_serial}

# *******************************************open application******************************************************************************************
 
  AutoItLibrary.Run    ${application}  
  Sleep    1
  Wait For Active Window    Form1
  Capture Page Screenshot
  Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
  Sleep    1
  Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
  Sleep    1
  ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
  log    ${PIN}
  Win Close     Form1
  
# *******************************************Cancel cash card***************************************************
   Wait Until Element Is Visible             ${click_input_password} 
   Sleep  2        
   click element                             ${click_input_password}
   Selenium2Library.input text               ${input_password}                ${PIN}
   Press Special Key        ENTER
   click element                             ${reason_cancel} 
   Press Special Key        C_DOWN
   Press Special Key        ENTER
   click element                            ${comfirm_cancel}
   Sleep    2
   SikuliLibrary.Press Special Key        ENTER
   Sleep    2
   
# *******************************************select printer********************************************************************
   Wait Until Element Is Visible             ${select_printer}   
   click element                             ${select_printer} 
   FOR    ${index}    IN RANGE    5
           Press Special Key        C_DOWN
   END
   SikuliLibrary.Press Special Key        ENTER
   Sleep    1
   click element            ${print_cancel_receipt} 
   sleep   4s
   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       Serial_Cash_Card_50
   SikuliLibrary.Press Special Key           ENTER
   sleep     3s
   Right Click     ${pic_acrobat} 
   Click           ${pic_close acrobat} 
   





   # Sleep    3
   # Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
   # Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2
   # Mouse Move              nX=632, nY=575, nSpeed=-1
   # Mouse Click             strButton=LEFT, nX=632, nY=575, nClicks=1, nSpeed=-1
   













#   Sleep    8
#   Click             ${pic_save_cancel}  
  # Control Send    Save PDF File As    ${EMPTY}         Edit1             ${Document_number1}
  # Sleep    3
#   WinWaitActive  ("Save PDF File As")
#   Control Click    Save PDF File As    &Save    Button2
  

   # Select window           title=Save PDF File As
#   Sleep    2
#   SikuliLibrary.Press Special Key        ENTER
  
#   Click             ${pic_save_cancel}   
#   sleep     3s  
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
  # sleep   3s
  # Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Cancel_${Document_number1}.pdf
  # sleep       3s



﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                


*** Variables ***
${application}           C:\\Users\\Beer\\Desktop\\GenCode.exe
${Document_number1}          nAL100068
# ${Document_number2}        nAL100059
# ${Document_number3}        nAL100060
# ${Document_number4}        nAL100061
# ${Document_number5}        nAL100062


*** Test Cases ***
Cancel Cashcard Receipt 1
  Goto Cancel Sales PRE_Production Shop_10  
  Wait Until Element Is Visible             ${Input_document_number}          
  Selenium2Library.input text               ${Input_document_number}                ${Document_number1}
  Wait Until Element Is Visible             ${Find_document_cancel} 
  click element                             ${Find_document_cancel}
  Sleep   2 
  click element                             ${reason_cancel} 
  Press Special Key        C_DOWN
  Press Special Key        ENTER
  click element                            ${comfirm_cancel}
  Sleep    2
  SikuliLibrary.Press Special Key        ENTER
  Sleep    2
  SikuliLibrary.Press Special Key         ESC
  SikuliLibrary.Press Special Key         F2 

# ----------------------------------------------------------------------------------------------------------------------------------
# Cancel Cashcard Receipt 2 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number5}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Sleep   2 
#   click element                             ${reason_cancel} 
#   Press Special Key        C_DOWN
#   Press Special Key        ENTER
#   click element                            ${comfirm_cancel}
#   Sleep    2
#   SikuliLibrary.Press Special Key        ENTER
#   Sleep    2
#   SikuliLibrary.Press Special Key         ESC
#   SikuliLibrary.Press Special Key         F2 

# # ----------------------------------------------------------------------------------------------------------------------------------

# Cancel Cashcard Receipt 3 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number2}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Sleep   2 
#   click element                             ${reason_cancel} 
#   Press Special Key        C_DOWN
#   Press Special Key        ENTER
#   click element                            ${comfirm_cancel}
#   Sleep    2
#   SikuliLibrary.Press Special Key        ENTER
#   Sleep    2
#   SikuliLibrary.Press Special Key         ESC
#   SikuliLibrary.Press Special Key         F2 

# # ----------------------------------------------------------------------------------------------------------------------------------
# Cancel Cashcard Receipt 4 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number3}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Sleep   2 
#   click element                             ${reason_cancel} 
#   Press Special Key        C_DOWN
#   Press Special Key        ENTER
#   click element                            ${comfirm_cancel}
#   Sleep    2
#   SikuliLibrary.Press Special Key        ENTER
#   Sleep    2
#   SikuliLibrary.Press Special Key         ESC
#   SikuliLibrary.Press Special Key         F2 

# # ----------------------------------------------------------------------------------------------------------------------------------
# Cancel Cashcard Receipt 5 
#   Wait Until Element Is Visible             ${Input_document_number}          
#   Selenium2Library.input text               ${Input_document_number}                ${Document_number4}
#   Wait Until Element Is Visible             ${Find_document_cancel} 
#   click element                             ${Find_document_cancel}
#   Sleep   2 
#   click element                             ${reason_cancel} 
#   Press Special Key        C_DOWN
#   Press Special Key        ENTER
#   click element                            ${comfirm_cancel}
#   Sleep    2
#   SikuliLibrary.Press Special Key        ENTER
#   Sleep    2
#   SikuliLibrary.Press Special Key         ESC
#   SikuliLibrary.Press Special Key         F2 
﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
# Sales Cash Card 50 Baht
#   Goto Sales PRE_Production Shop_10 
#   click element                     ${select_product}
#   Wait Until Element Is Visible     ${type_product}   60s
#   click element                     ${type_product} 
#     FOR    ${index}    IN RANGE    2
#            Press Special Key        C_DOWN
#     END
#   Wait Until Element Is Visible    ${Find_data} 
#   click element                    ${Find_data} 
#   Wait Until Element Is Visible    ${cash_card_50}        60s
#   click element                    ${cash_card_50}
#   click element                    ${select_product_fromlist}
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${check_stock} 
#   click element                             ${check_stock} 
#   sleep     1s
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${get_total}      
#   ${get_total}           Selenium2Library.GetValue         ${get_total}
#   Selenium2Library.input text       ${input_cash}          ${get_total}
#   sleep     2s
  
#   click element                     ${print_button_sales} 

#   sleep   3s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       Serial_Cash_Card_50
#   SikuliLibrary.Press Special Key           ENTER
#   sleep     3s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep   4s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                      Receipt_Cash_Card_50
#   SikuliLibrary.Press Special Key           ENTER
#   sleep   3s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Serial_Cash_Card_50.pdf              C:\\TRM\\Receipt\\Serial_Cash_Card_50.pdf
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Receipt_Cash_Card_50.pdf              C:\\TRM\\Receipt\\Receipt_Cash_Card_50.pdf
  

# Sales Cash Card 90 Baht
#   Wait Until Element Is Visible     ${select_product}     60s
#   click element                     ${select_product}
#   Wait Until Element Is Visible     ${type_product}       60s
#   click element                     ${type_product} 
#     FOR    ${index}    IN RANGE    2
#            Press Special Key        C_DOWN
#     END
#   Wait Until Element Is Visible    ${Find_data} 
#   click element                    ${Find_data} 
#   Wait Until Element Is Visible    ${cash_card_90}
#   click element                    ${cash_card_90}
#   click element                    ${select_product_fromlist}
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${check_stock} 
#   click element                             ${check_stock} 
#   sleep     1s
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${get_total}      60s
#   ${get_total}           Selenium2Library.GetValue         ${get_total}
#   Selenium2Library.input text       ${input_cash}          ${get_total}
#   sleep     2s
#   click element                     ${print_button_sales} 

#   sleep   3s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       Serial_Cash_Card_90
#   SikuliLibrary.Press Special Key           ENTER
#   sleep     3s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep   3s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                      Receipt_Cash_Card_90
#   SikuliLibrary.Press Special Key           ENTER
#   sleep   3s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Serial_Cash_Card_90.pdf              C:\\TRM\\Receipt\\Serial_Cash_Card_90.pdf
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Receipt_Cash_Card_90.pdf              C:\\TRM\\Receipt\\Receipt_Cash_Card_90.pdf
  


# Sales Cash Card 150 Baht
#   Wait Until Element Is Visible     ${select_product}   60s
#   click element                     ${select_product}
#   Wait Until Element Is Visible     ${type_product}     60s
#   click element                     ${type_product} 
#     FOR    ${index}    IN RANGE    2
#            Press Special Key        C_DOWN
#     END
#   Wait Until Element Is Visible    ${Find_data} 
#   click element                    ${Find_data} 
  
#   Wait Until Element Is Visible    ${cash_card_150}
#   click element                    ${cash_card_150}
#   click element                    ${select_product_fromlist}
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${check_stock} 
#   click element                             ${check_stock} 
#   sleep     1s
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${get_total}        60s
#   ${get_total}           Selenium2Library.GetValue         ${get_total}
#   Selenium2Library.input text       ${input_cash}          ${get_total}
#   sleep     2s
#   click element                     ${print_button_sales} 

#   sleep   3s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       Serial_Cash_Card_150
#   SikuliLibrary.Press Special Key           ENTER
#   sleep     2s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep   3s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                      Receipt_Cash_Card_150
#   SikuliLibrary.Press Special Key           ENTER
#   sleep   3s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Serial_Cash_Card_150.pdf              C:\\TRM\\Receipt\\Serial_Cash_Card_150.pdf
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Receipt_Cash_Card_150.pdf              C:\\TRM\\Receipt\\Receipt_Cash_Card_150.pdf
  


# Sales Cash Card 500 Baht
#   Wait Until Element Is Visible     ${select_product}   60s
#   click element                     ${select_product}
#   Wait Until Element Is Visible     ${type_product}     60s
#   click element                     ${type_product} 
#     FOR    ${index}    IN RANGE    2
#            Press Special Key        C_DOWN
#     END
#   Wait Until Element Is Visible    ${Find_data} 
#   click element                    ${Find_data} 
  
#   Wait Until Element Is Visible    ${cash_card_500}
#   click element                    ${cash_card_500}
#   click element                    ${select_product_fromlist}
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${check_stock} 
#   click element                             ${check_stock} 
#   sleep     1s
#   Capture Page Screenshot 
#   Wait Until Element Is Visible             ${get_total}    60s
#   ${get_total}           Selenium2Library.GetValue         ${get_total}
#   Selenium2Library.input text       ${input_cash}          ${get_total}
#   sleep     2s
#   click element                     ${print_button_sales} 

#   sleep   3s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       Serial_Cash_Card_500
#   SikuliLibrary.Press Special Key           ENTER
#   sleep     2s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep   3s
#   SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                      Receipt_Cash_Card_500
#   SikuliLibrary.Press Special Key           ENTER
#   sleep   3s
#   Right Click     ${pic_acrobat} 
#   Click           ${pic_close acrobat} 
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Serial_Cash_Card_500.pdf              C:\\TRM\\Receipt\\Serial_Cash_Card_500.pdf
#   sleep       3s
#   Move File       C:\\Users\\Beer\\Documents\\Receipt_Cash_Card_500.pdf              C:\\TRM\\Receipt\\Receipt_Cash_Card_500.pdf
  




  
Sales Cash Card 1000 Baht
  Goto Sales PRE_Production Shop_10
  Wait Until Element Is Visible     ${select_product}   60s  
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}     60s
  click element                     ${type_product} 
    FOR    ${index}    IN RANGE    2
           Press Special Key        C_DOWN
    END
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
  Wait Until Element Is Visible    ${row} 
  click element                    ${row}
  sleep     1
  SikuliLibrary.Press Special Key         C_DOWN
  sleep     1
  click element                    ${select_product_fromlist}
  Wait Until Element Is Visible    ${cash_card_1000}
  click element                    ${cash_card_1000}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 
  Wait Until Element Is Visible             ${get_total}    60s
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 

  sleep   3s
  SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                       Serial_Cash_Card_1000
  SikuliLibrary.Press Special Key           ENTER
  sleep     3s
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat} 
  sleep   3s
  SikuliLibrary.input text                  C:\\TRM\\sikulipicture\\javaprinting.png                      Receipt_Cash_Card_1000
  SikuliLibrary.Press Special Key           ENTER
  sleep   3s
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat} 
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Serial_Cash_Card_1000.pdf              C:\\TRM\\Receipt\\Serial_Cash_Card_1000.pdf
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Receipt_Cash_Card_1000.pdf              C:\\TRM\\Receipt\\Receipt_Cash_Card_1000.pdf
  
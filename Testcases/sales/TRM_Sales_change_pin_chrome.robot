﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary
Library         test_ocr.py


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Test Teardown   close browser
                


*** Variables ***
${application}             C:\\Users\\Beer\\Desktop\\GenCode.exe
${Document_number1}        ALA1F0368




*** Test Cases ***

# *************************************************************************************************************************************
Change PIN 1

  Goto change pin PRE_Production Shop_10 Chrome 
  Wait Until Element Is Visible             ${input_numdoc_changepin}      60     
  Selenium2Library.input text               ${input_numdoc_changepin}                 ${Document_number1}
  click element                             ${search_F8_changepin} 
  Wait Until Element Is Visible             ${checkbox_changepin}          60
  click element                             ${checkbox_changepin}
  Wait Until Element Is Visible     ${serial}                              60
  ${get_serial}           Selenium2Library.GetValue         ${serial}
  LOG     ${get_serial}

# *******************************************open application******************************************************************************************
  sleep   2
  OperatingSystem.Run               C:\Users\Beer\Desktop\\On-Screen Keyboard.lnk
  sleep   2 
  AutoItLibrary.Run    ${application}  
  Sleep    1
  Wait For Active Window    Form1
  Capture Page Screenshot
  Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
  Sleep    1
  Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
  Sleep    1
  ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
  log    ${PIN}
  Win Close     Form1
  
# # *******************************************change pin***************************************************
   Wait Until Element Is Visible             ${click_input_password} 
   Sleep  2        
   click element                             ${click_input_password}
   Selenium2Library.input text               ${input_password}                ${PIN}
   click element                             ${confirm_changepin} 
   Sleep  1
   Press Special Key        ENTER

# ------------------------------------------------------Select Printer----------------------------------------------------------------------------  
   Wait Until Element Is Visible             ${select_printer}        60s
   click element                             ${select_printer} 
   FOR    ${index}    IN RANGE    4
           Press Special Key        C_DOWN
   END
   sleep    2
   SikuliLibrary.Press Special Key        ENTER
   Wait Until Element Is Visible    ${print_cancel_receipt}          60s
   click element                    ${print_cancel_receipt} 
   sleep    2
# ----------------------------------------------------------------------------------------------------------------------------------  
   SikuliLibrary.Press Special Key        ENTER
   sleep    3
   Right Click     ${pic_acrobat} 
   Click           ${pic_close acrobat}      
   sleep       5s
   Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\ALA1F0368_changePIN_1.pdf

   # ----------------------------------------------------------Verify Receipt--------------------------------------------------------------------------------
    ${text}=                extract pdf text       C:\\TRM\\Receipt\\ALA1F0368_changePIN_1.pdf
    log  ${text}
    should contain          ${text}     ALA1F0368
    Remove File             C:\\TRM\\Testcases\\sales\\receipt.jpeg
    

# ----------------------------------------------------------------------------------------------------------------------------------    


Change PIN 2 
  Goto change pin PRE_Production Shop_10 Chrome
  Wait Until Element Is Visible             ${input_numdoc_changepin}      60     
  Selenium2Library.input text               ${input_numdoc_changepin}                 ${Document_number1}
  click element                             ${search_F8_changepin} 
  Wait Until Element Is Visible             ${checkbox_changepin}          60
  click element                             ${checkbox_changepin}
  Wait Until Element Is Visible     ${serial}                              60
  ${get_serial}           Selenium2Library.GetValue         ${serial}
  LOG     ${get_serial}

# *******************************************open application******************************************************************************************
  sleep   2
  OperatingSystem.Run               C:\Users\Beer\Desktop\\On-Screen Keyboard.lnk
  sleep   2 
  AutoItLibrary.Run    ${application}  
  Sleep    1
  Wait For Active Window    Form1
  Capture Page Screenshot
  Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
  Sleep    1
  Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
  Sleep    1
  ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
  log    ${PIN}
  Win Close     Form1
  
# # *******************************************change pin***************************************************
   Wait Until Element Is Visible             ${click_input_password} 
   Sleep  2        
   click element                             ${click_input_password}
   Selenium2Library.input text               ${input_password}                ${PIN}
   click element                             ${confirm_changepin} 
   Sleep  1
   Press Special Key        ENTER

# ------------------------------------------------------Select Printer----------------------------------------------------------------------------  
   Wait Until Element Is Visible             ${select_printer}        60s
   click element                             ${select_printer} 
   FOR    ${index}    IN RANGE    4
           Press Special Key        C_DOWN
   END
   sleep    2
   SikuliLibrary.Press Special Key        ENTER
   Wait Until Element Is Visible    ${print_cancel_receipt}          60s
   click element                    ${print_cancel_receipt} 
   sleep    2
# ----------------------------------------------------------------------------------------------------------------------------------  
   SikuliLibrary.Press Special Key        ENTER
   sleep    3
   Right Click     ${pic_acrobat} 
   Click           ${pic_close acrobat}      
   sleep       5s
   Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\ALA1F0368_changePIN_2.pdf

# ----------------------------------------------------------Verify Receipt--------------------------------------------------------------------------------
    ${text}=                extract pdf text       C:\\TRM\\Receipt\\ALA1F0368_changePIN_2.pdf
    log  ${text}
    should contain          ${text}     ALA1F0368
    Remove File             C:\\TRM\\Testcases\\sales\\receipt.jpeg
     
# ----------------------------------------------------------------------------------------------------------------------------------     

Change PIN 3
  Goto change pin PRE_Production Shop_10 Chrome 
  Wait Until Element Is Visible             ${input_numdoc_changepin}      60     
  Selenium2Library.input text               ${input_numdoc_changepin}                 ${Document_number1}
  click element                             ${search_F8_changepin} 
  Wait Until Element Is Visible             ${checkbox_changepin}          60
  click element                             ${checkbox_changepin}
  Wait Until Element Is Visible     ${serial}                              60
  ${get_serial}           Selenium2Library.GetValue         ${serial}
  LOG     ${get_serial}

# *******************************************open application******************************************************************************************
  sleep   2
  OperatingSystem.Run               C:\Users\Beer\Desktop\\On-Screen Keyboard.lnk
  sleep   2 
  AutoItLibrary.Run    ${application}  
  Sleep    1
  Wait For Active Window    Form1
  Capture Page Screenshot
  Control Send    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a2             ${get_serial}
  Sleep    1
  Control Click    Form1    button     WindowsForms10.BUTTON.app.0.378734a1
  Sleep    1
  ${PIN}    Control Get Text    Form1    ${EMPTY}    WindowsForms10.EDIT.app.0.378734a1
  log    ${PIN}
  Win Close     Form1
  
# # *******************************************change pin***************************************************
   Wait Until Element Is Visible             ${click_input_password} 
   Sleep  2        
   click element                             ${click_input_password}
   Selenium2Library.input text               ${input_password}                ${PIN}
   click element                             ${confirm_changepin} 
   Sleep  1
   Press Special Key        ENTER

# ------------------------------------------------------Select Printer----------------------------------------------------------------------------  
   Wait Until Element Is Visible             ${select_printer}        60s
   click element                             ${select_printer} 
   FOR    ${index}    IN RANGE    4
           Press Special Key        C_DOWN
   END
   sleep    2
   SikuliLibrary.Press Special Key        ENTER
   Wait Until Element Is Visible    ${print_cancel_receipt}          60s
   click element                    ${print_cancel_receipt} 
   sleep    2
# ----------------------------------------------------------------------------------------------------------------------------------  
   SikuliLibrary.Press Special Key        ENTER
   sleep    3
   Right Click     ${pic_acrobat} 
   Click           ${pic_close acrobat}      
   sleep       5s
   Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\ALA1F0368_changePIN_3.pdf

# ----------------------------------------------------------Verify Receipt-----------------------------------------------------------
    ${text}=                extract pdf text       C:\\TRM\\Receipt\\ALA1F0368_changePIN_3.pdf
    log  ${text}
    should contain          ${text}     ALA1F0368
    Remove File             C:\\TRM\\Testcases\\sales\\receipt.jpeg
    

   


  
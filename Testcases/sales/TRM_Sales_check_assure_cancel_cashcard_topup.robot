﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         AutoItLibrary


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                


*** Variables ***
${application}           C:\\Users\\Beer\\Desktop\\GenCode.exe
${Document_number1}        nAL100063
${Document_number2}        nAL100064
${Document_number3}        nAL100065
${Document_number4}        nAL100066
${Document_number5}        nAL100067


*** Test Cases ***

# *************************************************************************************************************************************
check assure cancel 1   
  Goto Cancel Sales PRE_Production Shop_10  
  Wait Until Element Is Visible             ${Input_document_number}          
  Selenium2Library.input text               ${Input_document_number}                ${Document_number1}
  Wait Until Element Is Visible             ${Find_document_cancel} 
  click element                             ${Find_document_cancel}
  Sleep    2
  Capture page Screenshot  
  Page Should Contain                       ถูกยกเลิกไปแล้ว     
  SikuliLibrary.Press Special Key           F2

check assure cancel 2     
  Wait Until Element Is Visible             ${Input_document_number}          
  Selenium2Library.input text               ${Input_document_number}                ${Document_number2}
  Wait Until Element Is Visible             ${Find_document_cancel} 
  click element                             ${Find_document_cancel}
  Sleep    2
  Capture page Screenshot   
  Page Should Contain                       ถูกยกเลิกไปแล้ว     
  SikuliLibrary.Press Special Key           F2 

check assure cancel 3   
  Wait Until Element Is Visible             ${Input_document_number}          
  Selenium2Library.input text               ${Input_document_number}                ${Document_number3}
  Wait Until Element Is Visible             ${Find_document_cancel} 
  click element                             ${Find_document_cancel}
  Sleep    2
  Capture page Screenshot   
  Page Should Contain                       ถูกยกเลิกไปแล้ว     
  SikuliLibrary.Press Special Key           F2  

check assure cancel 4   
  Wait Until Element Is Visible             ${Input_document_number}          
  Selenium2Library.input text               ${Input_document_number}                ${Document_number4}
  Wait Until Element Is Visible             ${Find_document_cancel} 
  click element                             ${Find_document_cancel}
  Sleep    2
  Capture page Screenshot   
  Page Should Contain                       ถูกยกเลิกไปแล้ว     
  SikuliLibrary.Press Special Key           F2 

check assure cancel 5   
  Wait Until Element Is Visible             ${Input_document_number}          
  Selenium2Library.input text               ${Input_document_number}                ${Document_number4}
  Wait Until Element Is Visible             ${Find_document_cancel} 
  click element                             ${Find_document_cancel}
  Sleep    2
  Capture page Screenshot   
  Page Should Contain                       ถูกยกเลิกไปแล้ว     
  SikuliLibrary.Press Special Key           F2 
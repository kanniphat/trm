﻿*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem


Resource        ..\\..\\Keywords\\Keywords_Sales.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Sales.robot
Suite Teardown   close browser
                

*** Test Cases ***

E-Wallet 50 Baht
  Goto Sales PRE_Production Shop_10 
  click element                     ${select_product}
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   2
  Click                             ${select_e-Wallet}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${e-Wallet}
  click element                    ${e-Wallet}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}   
  Selenium2Library.input text      ${cost}            50 
  Selenium2Library.input text      ${mobile}          0891039020

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}  
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  Wait Until Element Is Visible     ${confirm_eWallet} 
  click element                     ${confirm_eWallet} 
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_E-Wallet_50Baht.pdf
  sleep   3
    
# ----------------------------------------------------------------------------------------------------------------------------------
E-Wallet 1000 Baht
  click                             ${pic_ewallet_F8} 
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   2
  Click                             ${select_e-Wallet}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${e-Wallet}
  click element                    ${e-Wallet}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}   
  Selenium2Library.input text      ${cost}            1000 
  Selenium2Library.input text      ${mobile}          0891039020

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}  
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  Wait Until Element Is Visible     ${confirm_eWallet} 
  click element                     ${confirm_eWallet} 
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_E-Wallet_1000Baht.pdf
  sleep   3


    
# ----------------------------------------------------------------------------------------------------------------------------------
E-Wallet 3000 Baht
  click                             ${pic_ewallet_F8} 
  Wait Until Element Is Visible     ${type_product}       10s
  click element                     ${type_product} 
  sleep   2
  Click                             ${select_e-Wallet}   
        
  Wait Until Element Is Visible    ${Find_data} 
  click element                    ${Find_data} 
 
  Wait Until Element Is Visible    ${e-Wallet}
  click element                    ${e-Wallet}
  click element                    ${select_product_fromlist}
  Capture Page Screenshot 
  Wait Until Element Is Visible    ${cost}   
  Selenium2Library.input text      ${cost}            3000
  Selenium2Library.input text      ${mobile}          0891039020

  Wait Until Element Is Visible             ${check_stock} 
  click element                             ${check_stock} 
  sleep     1s
  Capture Page Screenshot 

  Wait Until Element Is Visible             ${get_total}  
  ${get_total}           Selenium2Library.GetValue         ${get_total}
  Selenium2Library.input text       ${input_cash}          ${get_total}
  sleep     2s
  click element                     ${print_button_sales} 
  Wait Until Element Is Visible     ${confirm_eWallet} 
  click element                     ${confirm_eWallet} 
  sleep   5s
  SikuliLibrary.Press Special Key           ENTER
  Right Click     ${pic_acrobat} 
  Click           ${pic_close acrobat}  
  sleep       3s
  Move File       C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt\\Receipt_E-Wallet_3000Baht.pdf
  sleep   3
    
    
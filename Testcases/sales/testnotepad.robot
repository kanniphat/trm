﻿*** Settings ***
Library         AutoItLibrary


*** Variables ***
${msg}    Australia doesn't really like either its Prime Minister, Scott Morrison, or Opposition Labor leader Bill Shorten.          

${filename}  beer


*** Test Cases ***

# *************************************************************************************************************************************
Test Open Notepad
  Run             notepad.exe  
  WinWaitActive   Untitled - Notepad
  Send            ${msg} 
  WinClose        Untitled - Notepad
  WinWaitActive   Notepad    &Save
  ControlClick    Notepad    &Save     Button1
  Sleep   2
  WinWaitActive   Save As 
  Control Send    Save As       ${EMPTY}       Edit1             ${filename} 
  ControlClick    Save As       ${EMPTY}         ComboBox3  
  Send     ("{DOWN}{DOWN}{DOWN}{ENTER}")

  # Send  ("{ALTDOWN}{ALTUP}{RIGHT}{ENTER}{DOWN}{DOWN}{ENTER}")
  # Win Menu Select Item      Save As      ${EMPTY}     UTF-8
  
  Sleep   2
  ControlClick    Save As       &Save          Button2
  WinClose        Untitled - Notepad

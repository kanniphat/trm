*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Creare Brief Receipt

    Goto Bill Payment Page PRE_Production Shop_10
    Wait Until Element Is Visible       ${SEARCH_FIELD}        30s       
    Selenium2Library.input text         ${SEARCH_FIELD}        10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    Wait Until Element Is Visible       //div[@class='bill-amount']/input[@class='text-input input-bill-amount-request text-right']
    clear element text                  //div[@class='bill-amount']/input[@class='text-input input-bill-amount-request text-right']
    Selenium2Library.input text         //div[@class='bill-amount']/input[@class='text-input input-bill-amount-request text-right']            200
    click element                       //i[@class='fa fa-pencil-square-o']
    sleep    2s
    click element                       //div[@class='form-input-group receiptFormat']//i[@class='fa fa-circle-o']
                                       
    sleep    2s
    click element                       //button[@class='btn-submit full']

  
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       4s
    Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

# ----------------------------------------------------------Verify Receipt--------------------------------------------------------------------------------

     ${text}=                extract pdf text       C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    log  ${text}
    
    should contain          ${text}     ${receiptnumber[1]} 
    Remove File             C:\\TRM\\Testcases\\paynormal\\receipt.jpeg   
  
   
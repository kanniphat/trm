*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         DateTime

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Suite Teardown    close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Show Hint ORDERID
    Goto Bill Payment Page PRE_Production Shop_10 Melon             
    Selenium2Library.input text         ${SEARCH_FIELD}        o#12345678   
    Page should contain                 ORDER
    sleep   1
    Capture Page Screenshot

Show Hint CERTIFICATE          
    Selenium2Library.input text         ${SEARCH_FIELD}        1234567890123   
    Page should contain                 CERTIFICATE
    sleep   1
    Capture Page Screenshot


Show Hint TMVBAN 
    Selenium2Library.input text         ${SEARCH_FIELD}        123456789 
    Page should contain                 TMVBAN
    sleep   1 
    Capture Page Screenshot

Show Hint CCBSBAN 
    Selenium2Library.input text         ${SEARCH_FIELD}        12345678
    Page should contain                 CCBSBAN 
    sleep   1
    Capture Page Screenshot

Show Hint BCID_1
    Selenium2Library.input text         ${SEARCH_FIELD}        1234567
    Page should contain                 BCID 
    sleep   1
    Capture Page Screenshot

Show Hint BCID_2
    Selenium2Library.input text         ${SEARCH_FIELD}        71234567
    Page should contain                 BCID 
    sleep   1
    Capture Page Screenshot



Show Hint TRUEBAN and BCBAN
    Selenium2Library.input text         ${SEARCH_FIELD}        123456789012
    Page should contain                 TRUEBAN 
    Page should contain                 BCBAN 
    sleep   1
    Capture Page Screenshot


Show Hint Mobile
    Selenium2Library.input text         ${SEARCH_FIELD}        0123456789
    Page should contain                 MOBILE 
    sleep   1
    Capture Page Screenshot

Show Hint PRODUCTID_1
    Selenium2Library.input text         ${SEARCH_FIELD}        025439715
    Page should contain                 PRODUCTID
    sleep   1 
    Capture Page Screenshot


Show Hint PRODUCTID_2
    Selenium2Library.input text         ${SEARCH_FIELD}        123456789IPD
    Page should contain                 PRODUCTID 
    sleep   1
    Capture Page Screenshot

    
Show Hint PRODUCTID_3
    Selenium2Library.input text         ${SEARCH_FIELD}        012345678R
    Page should contain                 PRODUCTID 
    sleep   1
    Capture Page Screenshot



Show Hint TVSNO and TVSPREPAID_1
    Selenium2Library.input text         ${SEARCH_FIELD}        1234567
    Page should contain                 TVSNO 
    Page should contain                 TVSPREPAID
    sleep   1
    Capture Page Screenshot


Show Hint TVSNO and TVSPREPAID_2
    Selenium2Library.input text         ${SEARCH_FIELD}        12345678
    Page should contain                 TVSNO 
    Page should contain                 TVSPREPAID
    sleep   1
    Capture Page Screenshot


Show Hint TVSNO and TVSPREPAID_3
    Selenium2Library.input text         ${SEARCH_FIELD}        12345679
    Page should contain                 TVSNO 
    Page should contain                 TVSPREPAID
    sleep   1
    Capture Page Screenshot


# Show Date
#     ${date} =	Get Current Date


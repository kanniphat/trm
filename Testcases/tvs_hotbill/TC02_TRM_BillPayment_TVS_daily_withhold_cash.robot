*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay TVS Daily withhold by cash 

    Goto Bill Payment Page Pre_Production Shop10
    Wait Until Element Is Visible       ${SEARCH_FIELD}        60s         
    Selenium2Library.input text         ${SEARCH_FIELD}        2400487
    sleep  1
    click                               C:\\TRM\\sikulipicture\\tvsprepaid.png
    Wait Until Element Is Visible       //tbody[1]/tr[1]//div[@class='table-radio']             20s
    click element                       //tbody[1]/tr[1]//div[@class='table-radio']
    click element                       //button[contains(.,'ตกลง')]
    Wait Until Page Contains            True Knowledge รายวัน                   20s
    Capture Page ScreenShot
     
    Wait Until Element Is Visible       ${clear_checkbox}      50 
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1_TVG}
 
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text        ${gettextwithholdingtax} 
    ${totalonlytax}      Selenium2Library.Get Text        ${gettexttotalonlytax}
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax} 


    

    click element   ${print_button} 
    sleep    2 
    Wait For Active Window        Save Print Output As
    ControlFocus        Save Print Output As      ${EMPTY}        Edit1
    Control Send        Save Print Output As       ${EMPTY}       Edit1       1
    Sleep    1
    ControlFocus        Save Print Output As        &Save          Button2
    ControlClick        Save Print Output As        &Save          Button2

   
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
         
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 




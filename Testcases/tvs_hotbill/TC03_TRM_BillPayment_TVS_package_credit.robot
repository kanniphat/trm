*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay TVS Package by credit

    Goto Bill Payment Page Pre_Production Shop10
    Wait Until Element Is Visible       ${SEARCH_FIELD}        60s         
    Selenium2Library.input text         ${SEARCH_FIELD}        2400487
    sleep  1
    click                               C:\\TRM\\sikulipicture\\tvsprepaid.png
    Wait Until Element Is Visible       //tr[2]//div[@class='table-radio']             20s
    click element                       //tr[2]//div[@class='table-radio']
    click element                       //button[contains(.,'ตกลง')]
    Wait Until Page Contains            True Knowledge Package                   20s
    Capture Page ScreenShot
     

  

# ************************* Click Credit card *************************************
    Wait Until Element Is Visible    ${creditcard_option}      10s
    click element                    ${creditcard_option} 
    click element                    ${creditcard_button} 
    sleep   2s
    click element                    ${close_popup_creditcard}
    Selenium2Library.input text      ${input_creditnumber}            4999999999999999      
    Selenium2Library.input text      ${input_secretnumber}            872
    ${BankName1}             Get Value               ${BankName}   
     should be Equal     ${BankName1}      Other Bank
# ************************************************************************************     
    
    sleep    1
    click element    ${print_button} 
    sleep    2 
    Wait For Active Window        Save Print Output As
    ControlFocus        Save Print Output As      ${EMPTY}        Edit1
    Control Send        Save Print Output As       ${EMPTY}       Edit1       1
    Sleep    1
    ControlFocus        Save Print Output As        &Save          Button2
    ControlClick        Save Print Output As        &Save          Button2

    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
        
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
   
  
*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser
                

*** Test Cases ***

# *************************************************************************************************************************************
Testcase Pay TVS truesport by cheque

    Goto Bill Payment Page Pre_Production Shop10
    Wait Until Element Is Visible       ${SEARCH_FIELD}        60s         
    Selenium2Library.input text         ${SEARCH_FIELD}        2400487
    sleep  1
    click                               C:\\TRM\\sikulipicture\\tvsprepaid.png
    Wait Until Element Is Visible       //tr[10]//div[@class='table-radio']             20s
    click element                       //tr[10]//div[@class='table-radio']
    click element                       //button[contains(.,'ตกลง')]
    Wait Until Page Contains            ทรูสปอร์ต                    20s
    Capture Page ScreenShot
     

# ************************* Pay by Cheque **********************************************************************   
    click element   css=div.tab-tax > label:nth-of-type(3)
    sleep   2s
    Selenium2Library.input text           css=input[maxlength='8']          12345678
    Selenium2Library.input text           css=input[maxlength='3']          001
    Selenium2Library.input text           css=.cheque-branch-no             0001
    Selenium2Library.input text           css=[placeholder='__/__/____']            02072019

# ****************************************************************************************************************  
    sleep    2 
    Click element   ${print_button} 
    Wait For Active Window        Save Print Output As
    ControlFocus        Save Print Output As      ${EMPTY}        Edit1
    Control Send        Save Print Output As       ${EMPTY}       Edit1       1
    Sleep    1
    ControlFocus        Save Print Output As        &Save          Button2
    ControlClick        Save Print Output As        &Save          Button2
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
        
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
   

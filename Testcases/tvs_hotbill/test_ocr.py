from wand.image import Image as wand_image
from PIL import Image, ImageFilter
import pytesseract

img_filename = 'receipt.jpeg'


def extract_pdf_text(pdf_file):
    with(wand_image(filename=pdf_file, resolution=600)) as source:
        images = source.sequence
        pages = len(images)
        wand_image(images[0]).save(filename=img_filename)

    # --- convert image (jpeg) to text
    text = extract_image_text(img_filename)
    return text


def extract_image_text(filename):
    img = Image.open(filename)
    img_sharp = img.filter(ImageFilter.SHARPEN)
    text = pytesseract.image_to_string(img_sharp, lang='eng+tha')
    text = text.replace(' ','')
    
    print(text)
    return text


if __name__ == '__main__':
    pdf_file = 'receipt.pdf'
    text = extract_pdf_text(img_filename)
    print(text)

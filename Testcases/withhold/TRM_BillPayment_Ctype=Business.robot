*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser


*** Test Cases ***

Ctype=B Flag=Y Default=Uncheck

    Goto Bill Payment Page PRE_Production Shop_10
    Selenium2Library.input text         ${SEARCH_FIELD}       200060473
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    sleep   2s
    # Wait Until Page Contains Element            //div[@class="tax-description "]        10s
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s

    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[1]/td[1]/div/span  
    
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    Click       ${pic_savebutton} 
   
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    sleep       3s

# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=B Flag=N Default=Check

    Goto Bill Payment Page PRE_Production Shop_10 
    Selenium2Library.input text         ${SEARCH_FIELD}       200051632
    press key                           ${SEARCH_FIELD}         \\13
    sleep   2s
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[4]/td[1]/div/span  

    sleep   2s
    Wait Until Page Contains Element            //div[@class="tax-description "]        10s
    
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Click       ${pic_savebutton} 

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    sleep       3s 
    
# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=B Flag=Y Check_Manual

    Goto Bill Payment Page PRE_Production Shop_10
    Selenium2Library.input text         ${SEARCH_FIELD}       200060473
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
  
    sleep   2s
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[1]/td[1]/div/span 
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Click       ${pic_savebutton} 

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    sleep       3s 
    
# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=B Flag=N Uncheck_Manual

    Goto Bill Payment Page PRE_Production Shop_10 
    Selenium2Library.input text         ${SEARCH_FIELD}       200051632
    press key                           ${SEARCH_FIELD}         \\13
    sleep   2s
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[4]/td[1]/div/span  

    sleep   2s
    click element   ${taxcheckbox_billpayment}
    sleep   2s

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3s
    Click       ${pic_savebutton} 
   
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      

    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    sleep       3s 
# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=B Flag=Y,Flag=N Check_Manual

    Goto Bill Payment Page PRE_Production Shop_10 
    Selenium2Library.input text         ${SEARCH_FIELD}       200061757
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
  
    sleep   2s
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[1]/td[1]/div/span 
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[4]/td[1]/div/span 
    
   
    # click element   css=#app > div > div:nth-child(2) > div > div.bill-container > div:nth-child(1) > div.row > div.D-4.right-padding-reset > div > div > div.transaction-panel.animated.metroInRight > div.transaction-card > div.transaction-body > div > div.section-footer.props-tax-doc > div:nth-child(2) > div.row > div.D-1 > div
    # click element   //div[@class='tax-description ']//i[@class='fa fa-circle-o']
    # click element   //div[@class='tax-description ']/div[@class='row']//span[1]
    # Selenium2Library.input text         css=.error.default-input            001
   
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}
    Press Special Key        F10
    sleep  3s
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='.']                    47904
    click Button                       //button[@class='btn-submit full']

    click element   ${print_button}
    sleep   3s
    Click       ${pic_savebutton} 

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    sleep       3s 
    

# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=B Flag=Y,Flag=N Default=Uncheck

    Goto Bill Payment Page PRE_Production Shop_10
    Selenium2Library.input text         ${SEARCH_FIELD}       200061757
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
  
    sleep   2s
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[1]/td[1]/div/span 
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[4]/td[1]/div/span 
    

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    

    Press Special Key        F10
    sleep  3s
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    Selenium2Library.input text         css=input[value='.']                    47904
    click Button                       //button[@class='btn-submit full']
    click element   ${print_button}
    sleep  3s
    SikuliLibrary.input text            C:\\TRM\\sikulipicture\\javaprinting.png                1
    Click       ${pic_savebutton} 
    sleep       3s
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
    SikuliLibrary.input text            C:\\TRM\\sikulipicture\\javaprinting.png                2
    Click       ${pic_savebutton} 

    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
    SikuliLibrary.Press Special Key           ENTER

    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\1.pdf              C:\\TRM\\Receipt
    ${receiptnumber}      Remove String         ${receiptnumber}       ,   
    ${receiptnumber}      Split String          ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       C:\\TRM\\Receipt\\1.pdf         C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\2.pdf              C:\\TRM\\Receipt 
    Log             ${receiptnumber[3]}
    Move File       C:\\TRM\\Receipt\\2.pdf         C:\\TRM\\Receipt\\${receiptnumber[3]}.pdf 

    

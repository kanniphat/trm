*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser


*** Test Cases ***

Ctype=I_Invoice_type=DPST[Not_Pay_DPST]

    Goto Bill Payment Page PRE_Production Shop_10
    Selenium2Library.input text         ${SEARCH_FIELD}       200058660
    press key                           ${SEARCH_FIELD}         \\13
    
    sleep   2s
    # Wait Until Page Contains Element            //div[@class="tax-description "]        10s
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    sleep   2s
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //button[@type="button"][@class="btn btn-default"]
    click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[4]/td[1]/div/span

    
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       6s
    Click       ${pic_savebutton} 
   
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

# ------------------------------------------------------------------------------------------------------------------------------
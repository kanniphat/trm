*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser


*** Test Cases ***


Ctype=B Invoice_type=BILL and Invoice_type=IMCG

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       200049511
    press key                           ${SEARCH_FIELD}         \\13
    # Wait Until Element Is Visible       ${close_alert}                      10s
    # click element                       ${close_alert} 

    Wait Until Page Contains Element    //div[@class="tax-description hidden"]        10s
  
    click element                       ${clear_checkbox}
    click element                       ${checkbox_5_TVG}
    click element                       ${checkbox_9_TVG}
    Capture Page Screenshot  
    
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}
    Press Special Key        F10
    sleep  3s
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    click Button                       //button[@class='btn-submit full']

    click element   ${print_button}
    sleep   3s
    Click          ${pic_savebutton} 

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    

# ------------------------------------------------------------------------------------------------------------------------------
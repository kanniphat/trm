*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser


*** Test Cases ***

Ctype=B_Subtype=ZRR and Subtype=Bill

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       200050233
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${clear_checkbox}        10
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1_TVG}
    Capture Page Screenshot

    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    sleep   2s

    Selenium2Library.input text         ${SEARCH_FIELD}       200060473
    press key                           ${SEARCH_FIELD}         \\13
    sleep   4s
    Wait Until Element Is Visible       ${clear_checkbox}        10
    click element                       ${clear_checkbox}
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1_TVG}
    Capture Page Screenshot
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert}


    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       3
    Control Focus           Save PDF File As        ${EMPTY}       Edit1                  
    Control Send            Save PDF File As        ${EMPTY}       Edit1        {BACKSPACE} 
    sleep       2
    Control Send            Save PDF File As        ${EMPTY}       Edit1             1
    Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    sleep       3
    # ---------------------------------------------------------------------------------------------------------                
    Control Send            Save PDF File As        ${EMPTY}       Edit1        {BACKSPACE} 
    sleep       2
    Control Send            Save PDF File As        ${EMPTY}       Edit1             2
    Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2

    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
    SikuliLibrary.Press Special Key           ENTER


    sleep       3s
    ${receiptnumber}      Remove String         ${receiptnumber}       ,   
    ${receiptnumber}      Split String          ${receiptnumber}      ${SPACE}   
   
   
    LOG     ${receiptnumber[1]}
    LOG     ${receiptnumber[3]}

  
#---------------------------------------------------------------------Read PDF----------------------------------------------------------------------------------

   ${pdf1}=                extract pdf text       C:\\Users\\Beer\\Documents\\1.pdf
   log  ${pdf1}    

   ${pdf2}=                extract pdf text       C:\\Users\\Beer\\Documents\\2.pdf
   log  ${pdf2}   

#Invoice from popup#
    ${Invoice1}     Create List        ${receiptnumber[1]}
    ${Invoice2}     Create List        ${receiptnumber[3]}  
# --------------------------------------------------ใบที่ 1----------------------------------------------------------------------------------
#Find InvoiceNo in file#
    ${PAGE1}=   Get Regexp Matches     ${Pdf1}     ${Invoice1[0]}
    ${PAGE2}=   Get Regexp Matches     ${Pdf1}     ${Invoice2[0]}
   
#Condition match invoiceno with #
    Run Keyword If  ${PAGE1} == ${Invoice1}   Move File      C:\\Users\\Beer\\Documents\\1.pdf      C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Run Keyword If  ${PAGE2} == ${Invoice2}   Move File      C:\\Users\\Beer\\Documents\\1.pdf      C:\\TRM\\Receipt\\${receiptnumber[3]}.pdf 
   
# --------------------------------------------------ใบที่ 2-----------------------------------------------------------------------------------
#Find InvoiceNo in file#
    ${PAGE1}=   Get Regexp Matches     ${Pdf2}     ${Invoice1[0]}
    ${PAGE2}=   Get Regexp Matches     ${Pdf2}     ${Invoice2[0]}
   
#Condition match invoiceno with #
    Run Keyword If  ${PAGE1} == ${Invoice1}   Move File      C:\\Users\\Beer\\Documents\\2.pdf      C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Run Keyword If  ${PAGE2} == ${Invoice2}   Move File      C:\\Users\\Beer\\Documents\\2.pdf      C:\\TRM\\Receipt\\${receiptnumber[3]}.pdf


# --------------------------------------------------------------------------------------------------------------------------------------------
   Remove File             C:\\TRM\\Testcases\\withhold_Melon\\receipt.jpeg   









 
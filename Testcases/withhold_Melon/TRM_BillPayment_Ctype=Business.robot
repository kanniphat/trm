*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary
Library         AutoItLibrary
Library         test_ocr.py

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser


*** Test Cases ***

# Ctype=B Flag=Y Default=Uncheck

#     Goto Bill Payment Page PRE_Production Shop_10 Melon
#     Selenium2Library.input text         ${SEARCH_FIELD}       200060473
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element                       ${clear_checkbox}
#     click element                       ${clear_checkbox} 
#     Wait Until Element Is Visible       ${checkbox_1_TVG} 
#     click element                       ${checkbox_1_TVG} 
#     Capture Page Screenshot

#     Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s

#     click element         //button[@type="button"][@class="btn btn-default"]
#     click element         //button[@type="button"][@class="btn btn-default"]
#     click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[1]/td[1]/div/span  
    
#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
#     click element   ${print_button} 
#     sleep       3s
#     Click       ${pic_savebutton} 
   
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
#     sleep       3s

# # -----------------------------------------------------------------------------------------------------------------------------------

# Ctype=B Flag=N Default=Check

#     Goto Bill Payment Page PRE_Production Shop_10 Melon
#     Selenium2Library.input text         ${SEARCH_FIELD}       200060473
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element                       ${clear_checkbox}
#     click element                       ${clear_checkbox}      
#     click element                       ${checkbox_34}
#     Capture Page Screenshot
#     Wait Until Page Contains Element            //div[@class="tax-description "]        10s
    
#     Selenium2Library.input text      ${companynumber}            001
#     sleep       3s
#     ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
#     ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
#     sleep       1s
#     Click Element  ${fill_input_amount}
#     sleep       2s
#     Selenium2Library.input text      ${fill_input_amount}       ${tax}  
#     click element   ${print_button}
#     sleep   3s
#     Click       ${pic_savebutton} 

#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
#     sleep       3s 
    
# # -----------------------------------------------------------------------------------------------------------------------------------

# Ctype=B Flag=Y Check_Manual

#     Goto Bill Payment Page PRE_Production Shop_10 Melon
#     Selenium2Library.input text         ${SEARCH_FIELD}       200060473
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}                      10s
#     click element                       ${close_alert} 
#     click element                       ${clear_checkbox}
#     click element                       ${clear_checkbox}        
#     click element                       ${checkbox_1_TVG}
#     Capture Page Screenshot
#     Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
#     click element         //button[@type="button"][@class="btn btn-default"]
#     click element         //button[@type="button"][@class="btn btn-default"]
#     click element         //*[@id="app"]/div/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/div[3]/table/tr[1]/td[1]/div/span 
#     sleep   2s
#     click element   ${taxcheckbox_billpayment}
#     click element   ${taxonepercent}
#     click element   ${checkcompany}
#     Selenium2Library.input text      ${companynumber}            001
#     sleep       3s
#     ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
#     ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
#     sleep       1s
#     Click Element  ${fill_input_amount}
#     sleep       2s
#     Selenium2Library.input text      ${fill_input_amount}       ${tax}  
#     click element   ${print_button}
#     sleep   3s
#     Click       ${pic_savebutton} 

#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      
#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
#     sleep       3s 
    
# # -----------------------------------------------------------------------------------------------------------------------------------

# Ctype=B Flag=N Uncheck_Manual

#     Goto Bill Payment Page PRE_Production Shop_10 Melon 
#     Selenium2Library.input text         ${SEARCH_FIELD}       200060473
#     press key                           ${SEARCH_FIELD}         \\13
#     Wait Until Element Is Visible       ${close_alert}          10s
#     click element                       ${clear_checkbox}
#     click element                       ${clear_checkbox}      
#     click element                       ${checkbox_35}
#     Capture Page Screenshot  

#     sleep   2s
#     click element   ${taxcheckbox_billpayment}
#     sleep   2s

#     Wait Until Element Is Visible       ${total_amount}
#     ${input_amount}       Get Value     ${total_amount}
#     Wait Until Element Is Visible       ${fill_input_amount}
#     Click Element                       ${fill_input_amount}
#     Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
#     click element   ${print_button} 
#     sleep       3s
#     Click       ${pic_savebutton} 
   
    
#     Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
#     Page should Contain         พิมพ์ใบเสร็จ 
#     ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
#     Right Click     ${pic_acrobat} 
#     Click           ${pic_close acrobat}      

#     sleep       3s
#     Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

#     ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
#     Log             ${receiptnumber[1]}
#     Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf
    # sleep       3s 
# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=B Flag=Y,Flag=N Check_Manual

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       200060473
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${clear_checkbox} 
    click element                       ${clear_checkbox}      
    click element                       ${checkbox_1_TVG}
    click element                       ${checkbox_36}
    Capture Page Screenshot
    sleep   2s
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s

   
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text          ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}
    Press Special Key        F10
    sleep  3s
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790
    # Selenium2Library.input text         css=input[value='.']                    47904
    click Button                       //button[@class='btn-submit full']

    click element                      ${print_button} 
    sleep       3
    Control Focus           Save PDF File As        ${EMPTY}       Edit1                  
    Control Send            Save PDF File As        ${EMPTY}       Edit1        {BACKSPACE} 
    sleep       2
    Control Send            Save PDF File As        ${EMPTY}       Edit1             1
    Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    sleep       3
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
    sleep       2
  
# ---------------------------------------------------------------------------------------------------------                
    Control Send            Save PDF File As        ${EMPTY}       Edit1        {BACKSPACE} 
    sleep       2
    Control Send            Save PDF File As        ${EMPTY}       Edit1             2
    Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2

    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
  


    sleep       3s
    ${receiptnumber}      Remove String         ${receiptnumber}       ,   
    ${receiptnumber}      Split String          ${receiptnumber}      ${SPACE}   
   
   
    LOG     ${receiptnumber[1]}
    LOG     ${receiptnumber[3]}

  
#---------------------------------------------------------------------Read PDF----------------------------------------------------------------------------------

   ${pdf1}=                extract pdf text       C:\\Users\\Beer\\Documents\\1.pdf
   log  ${pdf1}    

   ${pdf2}=                extract pdf text       C:\\Users\\Beer\\Documents\\2.pdf
   log  ${pdf2}   

#Invoice from popup#
    ${Invoice1}     Create List        ${receiptnumber[1]}
    ${Invoice2}     Create List        ${receiptnumber[3]}  
# --------------------------------------------------ใบที่ 1----------------------------------------------------------------------------------
#Find InvoiceNo in file#
    ${PAGE1}=   Get Regexp Matches     ${Pdf1}     ${Invoice1[0]}
    ${PAGE2}=   Get Regexp Matches     ${Pdf1}     ${Invoice2[0]}
   
#Condition match invoiceno with #
    Run Keyword If  ${PAGE1} == ${Invoice1}   Move File      C:\\Users\\Beer\\Documents\\1.pdf      C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Run Keyword If  ${PAGE2} == ${Invoice2}   Move File      C:\\Users\\Beer\\Documents\\1.pdf      C:\\TRM\\Receipt\\${receiptnumber[3]}.pdf 
   
# --------------------------------------------------ใบที่ 2-----------------------------------------------------------------------------------
#Find InvoiceNo in file#
    ${PAGE1}=   Get Regexp Matches     ${Pdf2}     ${Invoice1[0]}
    ${PAGE2}=   Get Regexp Matches     ${Pdf2}     ${Invoice2[0]}
   
#Condition match invoiceno with #
    Run Keyword If  ${PAGE1} == ${Invoice1}   Move File      C:\\Users\\Beer\\Documents\\2.pdf      C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Run Keyword If  ${PAGE2} == ${Invoice2}   Move File      C:\\Users\\Beer\\Documents\\2.pdf      C:\\TRM\\Receipt\\${receiptnumber[3]}.pdf


# --------------------------------------------------------------------------------------------------------------------------------------------
   Remove File             C:\\TRM\\Testcases\\withhold_Melon\\receipt.jpeg   







    

# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=B Flag=Y,Flag=N Default=Uncheck

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       200060473
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${clear_checkbox}  
    click element                       ${clear_checkbox}    
    click element                       ${checkbox_1_TVG}
    click element                       ${checkbox_37}
    Capture Page Screenshot
    sleep   2s
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    

    Press Special Key        F10
    sleep  3s
    Selenium2Library.input text         css=input[maxlength='13']               7654908642790

    click Button                       //button[@class='btn-submit full']
    click element                      ${print_button} 
    sleep       3
    Control Focus           Save PDF File As        ${EMPTY}       Edit1                  
    Control Send            Save PDF File As        ${EMPTY}       Edit1        {BACKSPACE} 
    sleep       2
    Control Send            Save PDF File As        ${EMPTY}       Edit1             3
    Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    sleep       3
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
    sleep       2
  
    # ---------------------------------------------------------------------------------------------------------                
    Control Send            Save PDF File As        ${EMPTY}       Edit1        {BACKSPACE} 
    sleep       2
    Control Send            Save PDF File As        ${EMPTY}       Edit1             4
    Control Focus           strTitle=Save PDF File As     strText=&Save    strControl=Button2
    Control Click           strTitle=Save PDF File As     strText=&Save    strControl=Button2

    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat} 
  


    sleep       3s
    ${receiptnumber}      Remove String         ${receiptnumber}       ,   
    ${receiptnumber}      Split String          ${receiptnumber}      ${SPACE}   
   
   
    LOG     ${receiptnumber[1]}
    LOG     ${receiptnumber[3]}

  
#---------------------------------------------------------------------Read PDF----------------------------------------------------------------------------------

   ${pdf1}=                extract pdf text       C:\\Users\\Beer\\Documents\\3.pdf
   log  ${pdf1}    

   ${pdf2}=                extract pdf text       C:\\Users\\Beer\\Documents\\4.pdf
   log  ${pdf2}   

#Invoice from popup#
    ${Invoice1}     Create List        ${receiptnumber[1]}
    ${Invoice2}     Create List        ${receiptnumber[3]}  
# --------------------------------------------------ใบที่ 1----------------------------------------------------------------------------------
#Find InvoiceNo in file#
    ${PAGE1}=   Get Regexp Matches     ${Pdf1}     ${Invoice1[0]}
    ${PAGE2}=   Get Regexp Matches     ${Pdf1}     ${Invoice2[0]}
   
#Condition match invoiceno with #
    Run Keyword If  ${PAGE1} == ${Invoice1}   Move File      C:\\Users\\Beer\\Documents\\3.pdf      C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Run Keyword If  ${PAGE2} == ${Invoice2}   Move File      C:\\Users\\Beer\\Documents\\3.pdf      C:\\TRM\\Receipt\\${receiptnumber[3]}.pdf 
   
# --------------------------------------------------ใบที่ 2-----------------------------------------------------------------------------------
#Find InvoiceNo in file#
    ${PAGE1}=   Get Regexp Matches     ${Pdf2}     ${Invoice1[0]}
    ${PAGE2}=   Get Regexp Matches     ${Pdf2}     ${Invoice2[0]}
   
#Condition match invoiceno with #
    Run Keyword If  ${PAGE1} == ${Invoice1}   Move File      C:\\Users\\Beer\\Documents\\4.pdf      C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    Run Keyword If  ${PAGE2} == ${Invoice2}   Move File      C:\\Users\\Beer\\Documents\\4.pdf      C:\\TRM\\Receipt\\${receiptnumber[3]}.pdf


# --------------------------------------------------------------------------------------------------------------------------------------------
   Remove File             C:\\TRM\\Testcases\\withhold_Melon\\receipt.jpeg   

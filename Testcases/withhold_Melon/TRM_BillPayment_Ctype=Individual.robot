*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

Test Teardown   close browser


*** Test Cases ***

Ctype=I Flag=Y and Default Withholding tax=Uncheck

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_2}
    Capture Page Screenshot

    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       6s
    Click       ${pic_savebutton} 
   
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=I Flag=N and Default Withholding tax=Uncheck

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    Selenium2Library.input text         ${editcashbill}             200
    Capture Page Screenshot

    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       6s
    Click       ${pic_savebutton} 
   
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 

# -----------------------------------------------------------------------------------------------------------------------------------

Ctype=I Flag=Y and Withholding tax=Check Manual
    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_2}
    
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Click       ${pic_savebutton} 

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 
    
# -----------------------------------------------------------------------------------------------------------------------------------
Ctype=I Flag=N and Withholding tax=Check Manual

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       10002067
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${close_alert}                      10s
    click element                       ${close_alert} 
    click element                       ${clear_checkbox}      
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1}
    Selenium2Library.input text         ${editcashbill}             200
    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    
    sleep   2s
    click element   ${taxcheckbox_billpayment}
    click element   ${taxonepercent}
    click element   ${checkcompany}
    Selenium2Library.input text      ${companynumber}            001
    sleep       3s
    ${tax}               Selenium2Library.Get Text         ${gettextwithholdingtax}
    ${totalonlytax}      Selenium2Library.Get Text    ${gettexttotalonlytax} 
    sleep       1s
    Click Element  ${fill_input_amount}
    sleep       2s
    Selenium2Library.input text      ${fill_input_amount}       ${tax}  
    click element   ${print_button}
    sleep   3s
    Click       ${pic_savebutton} 

    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 


# -------------------------------------------------------------------------------------------------------------------
*** Settings ***
Library         Selenium2Library
Library         BuiltIn
Library         String
Library         SikuliLibrary
Library         OperatingSystem
Library         Pdf2TextLibrary

Resource        ..\\..\\Keywords\\Keywords_Billpayment.robot
Resource        ..\\..\\Variables\\Variables_Billpayment.robot

# Test Teardown   close browser


*** Test Cases ***

Ctype=B Subtype=ZRR

    Goto Bill Payment Page PRE_Production Shop_10 Melon
    Selenium2Library.input text         ${SEARCH_FIELD}       200050233
    press key                           ${SEARCH_FIELD}         \\13
    Wait Until Element Is Visible       ${clear_checkbox}        10
    click element                       ${clear_checkbox}
    click element                       ${checkbox_1_TVG}
    Capture Page Screenshot

    Wait Until Page Contains Element             //div[@class="tax-description hidden"]        10s
    sleep   2s

    Click Element       ${taxcheckbox_billpayment}
    Element Should Not Be Visible                   //div[@class="tax-description "]
    Wait Until Page Contains Element                //div[@class="tax-description hidden"]        10s

    Wait Until Element Is Visible       ${total_amount}
    ${input_amount}       Get Value     ${total_amount}
    Wait Until Element Is Visible       ${fill_input_amount}
    Click Element                       ${fill_input_amount}
    Selenium2Library.input text         ${fill_input_amount}        ${input_amount}
    click element   ${print_button} 
    sleep       6s
    Click       ${pic_savebutton} 
   
    
    Wait Until Page Contains    พิมพ์ใบเสร็จ           10s
    Page should Contain         พิมพ์ใบเสร็จ 
    ${receiptnumber}            Selenium2Library.Get text         ${receiptnumber_system}
   
    Right Click     ${pic_acrobat} 
    Click           ${pic_close acrobat}      
    sleep       3s
    Move File      C:\\Users\\Beer\\Documents\\Java Printing.pdf              C:\\TRM\\Receipt

    ${receiptnumber}      Split String         ${receiptnumber}      ${SPACE}   
    Log             ${receiptnumber[1]}
    Move File       ${default_receipt}          C:\\TRM\\Receipt\\${receiptnumber[1]}.pdf 


*** Variables ***

${URL}                  http://tsm-uat.true.th:18080/TSM-Lite/index2.zul
${SEARCH_FIELD}         //input[@class='input-detail w-25']
${Checkbox}             //div[@class='rt-tr -odd']//input[@value='on']
${Edit_button}          //button[.='แก้ไขชื่อ/ที่อยู่']
${Edit_button2}          //button[.='ระบุชื่อที่อยู่']
${Edit_titlename}       //div[@class='changeAddress']//div[1]/div[1]//div[2]/div[@class='detail-col w-33-percent']//input[@class='input-detail']
${Edit_name}            //div[@class='changeAddress']//div[@class='detail-col w-66-percent']/div[@class='p-1 w-50 d-flex justify-content-start']/input[@class='input-detail']
${pic_acrobat}              C:\\TRM\\sikulipicture\\acrobat.png
${pic_close acrobat}        C:\\TRM\\sikulipicture\\close acrobat.png 
${receiptnumber_system}     //div[@class='s-alert-box-inner']
${Edit_building}           //div[@class='p-1 w-75 d-flex justify-content-start']/input[@class='input-detail']
${Edit_homenumber}         //div[@class='changeAddress']//div[1]/div[1]//div[4]/div[1]//input[@class='input-detail']
${Edit_soi}                //div[@class='changeAddress']//div[1]/div[1]//div[5]/div[1]//input[@class='input-detail']
${Edit_road}               //div[6]/div[1]//input[@class='input-detail']
${Edit_district}           //div[7]//div[1]/input[@class='input-detail']
${Edit_subdistrict}        //div[6]/div[2]//input[@class='input-detail']
${Edit_province}           //div[7]//div[2]/input[@class='input-detail']
${Edit_postalcode}         //div[8]//input[@class='input-detail']


# ---------------------------------------------------Brief to Full receipt------------------------------------------------------

${Edit_titlename2}          //div[@class='fullReceipt']//div[1]/div[1]/div[1]//div[2]/div[@class='detail-col w-33-percent']//input[@class='input-detail']
${Edit_name2}                //div[@class='fullReceipt']//div[@class='detail-col w-66-percent']/div[@class='p-1 w-50 d-flex justify-content-start']/input[@class='input-detail']
${Edit_building2}           //div[@class='p-1 w-75 d-flex justify-content-start']/input[@class='input-detail'] 
${Edit_homenumber2}         //div[@class='fullReceipt']//div[1]/div[1]/div[1]//div[4]/div[1]//input[@class='input-detail']
${Edit_soi2}                //div[@class='fullReceipt']//div[1]/div[1]/div[1]//div[5]/div[1]//input[@class='input-detail']            
${Edit_road2}               //div[6]/div[1]//input[@class='input-detail']
${Edit_district2}           //div[7]//div[1]/input[@class='input-detail'] 
${Edit_subdistrict2}        //div[6]/div[2]//input[@class='input-detail']   
${Edit_province2}           //div[7]//div[2]/input[@class='input-detail']   
${Edit_postalcode2}         //div[8]//input[@class='input-detail'] 
${ID_tax2}                  //div[@class='fullReceipt']//div[4]/div[3]//input[@class='input-detail']    
${ID_branch2}               //div[@class='fullReceipt']//div[5]/div[3]//input[@class='input-detail'] 


# ---------------------------------------------------Cancel receipt------------------------------------------------------
${Doc_typebutton}              //div[@class='search-content w-75']//div[2]/div[@class='d-flex justify-content-start']/button[.='...']
${Doc_type_selectreceipt02}      //body[@class='modal-open modal-open']/div[4]//div[.='ใบเสร็จรับเงิน/ใบกำกับภาษี']
${Doc_number}                  //div[@class='search-content w-75']/div[1]/div[4]//input[1]
${Checkbox2}                   //div[@class='rt-tr -odd']//input[@value='on']
${Checkbox_all}                //div[@class='rt-th']//input[@value='on']   
${Reason_cancel}               //select[@class='form-control']
${calendar_button}             //div[@class='react-datepicker__input-container']
${calendar_previous_month}    //button[@class='react-datepicker__navigation react-datepicker__navigation--previous']
${calendar_select_date_1Mar}  //div[@class='react-datepicker__day react-datepicker__day--fri'][@aria-label='day-1']
${ID_tax}                   //div[@class='changeAddress']//div[4]/div[3]//input[@class='input-detail']
${ID_branch}                //div[@class='changeAddress']//div[5]/div[3]//input[@class='input-detail']



# ---------------------------------------------------reprint receipt------------------------------------------------------
${SEARCH_FIELD_Reprint}     //div[@class='search-reprint']//div[@class='D-3 L-4 M-12 row-group']//div[2]/input[1]
${Checkbox_Reprint}         //div[@class='rt-tr -odd']//input[@value='on']
${Reason_Reprint}           //select[@class='form-control']
${Checkbox_Reprint_auto}    //input[@class='search-result-inline']
${Doc_typebutton_reprint}           //div[@class='search-reprint']/div[1]//button[@class='popup-button']
${Doc_type_selectreceipt04}         //body[@class='modal-open modal-open']/div[4]//div[.='ใบยืม/ฟรี']

${All_Document}             //div[@class='search-reprint']/div[1]//input[@value='on']
${All_Employee}             //div[@class='D-6 M-12 row-group']//input[@value='on']
${Row_reprint}              //span[@class='select-wrap -pageSizeOptions']/select[1]



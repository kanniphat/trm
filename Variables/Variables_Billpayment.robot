*** Variables ***

${URL}                  http://tsm-uat.true.th:18080/TSM-Lite/index2.zul
${SEARCH_FIELD}         css=input.input-select
${close_alert}          css=a.btn-close-alert > .fa

${clear_checkbox}       //label[@class='checkbox-ex-panel']/span[1]

                      
${checkbox_1}           //tr[1]/td[@class='bill-oper-color tu']//span[1]
${checkbox_2}           //tr[2]/td[@class='bill-oper-color tu']//span[1]
${checkbox_3}           //tr[3]/td[@class='bill-oper-color tu']//span[1]
${checkbox_4}           //tr[4]/td[@class='bill-oper-color tu']//span[1]
${checkbox_5}           //tr[5]/td[@class='bill-oper-color tu']//span[1]
${checkbox_6}           //tr[6]/td[@class='bill-oper-color tu']//span[1]
${checkbox_7}           //tr[7]/td[@class='bill-oper-color tu']//span[1]
${checkbox_8}           //tr[8]/td[@class='bill-oper-color tu']//span[1]
${checkbox_9}           //tr[9]/td[@class='bill-oper-color tu']//span[1]
${checkbox_10}          //tr[10]/td[@class='bill-oper-color tu']//span[1]
${checkbox_11}          //tr[11]/td[@class='bill-oper-color tu']//span[1]
${checkbox_12}          //tr[12]/td[@class='bill-oper-color tu']//span[1]
${checkbox_13}          //tr[13]/td[@class='bill-oper-color tu']//span[1]
${checkbox_14}          //tr[14]/td[@class='bill-oper-color tu']//span[1]
${checkbox_15}          //tr[15]/td[@class='bill-oper-color tu']//span[1]
${checkbox_16}          //tr[16]/td[@class='bill-oper-color tu']//span[1]
${checkbox_17}          //tr[17]/td[@class='bill-oper-color tu']//span[1]
${checkbox_18}          //tr[18]/td[@class='bill-oper-color tu']//span[1]
${checkbox_19}          //tr[19]/td[@class='bill-oper-color tu']//span[1]
${checkbox_20}          //tr[20]/td[@class='bill-oper-color tu']//span[1]
${checkbox_21}          //tr[21]/td[@class='bill-oper-color tu']//span[1]
${checkbox_22}          //tr[22]/td[@class='bill-oper-color tu']//span[1]
${checkbox_23}          //tr[23]/td[@class='bill-oper-color tu']//span[1]
${checkbox_24}          //tr[24]/td[@class='bill-oper-color tu']//span[1]
${checkbox_25}            //tr[25]/td[@class='bill-oper-color tu']//span[1]
${checkbox_26}            //tr[26]/td[@class='bill-oper-color tu']//span[1]
${checkbox_27}            //tr[27]/td[@class='bill-oper-color tu']//span[1]
${checkbox_28}            //tr[28]/td[@class='bill-oper-color tu']//span[1]
${checkbox_29}            //tr[29]/td[@class='bill-oper-color tu']//span[1]
${checkbox_30}            //tr[30]/td[@class='bill-oper-color tu']//span[1]
${checkbox_31}            //tr[31]/td[@class='bill-oper-color tu']//span[1]
${checkbox_32}            //tr[32]/td[@class='bill-oper-color tu']//span[1]
${checkbox_33}            //tr[33]/td[@class='bill-oper-color tu']//span[1]
${checkbox_34}            //tr[34]/td[@class='bill-oper-color tu']//span[1]
${checkbox_35}            //tr[35]/td[@class='bill-oper-color tu']//span[1]
${checkbox_36}            //tr[36]/td[@class='bill-oper-color tu']//span[1]
${checkbox_37}            //tr[37]/td[@class='bill-oper-color tu']//span[1]
${checkbox_38}            //tr[38]/td[@class='bill-oper-color tu']//span[1]
${checkbox_39}            //tr[39]/td[@class='bill-oper-color tu']//span[1]
${checkbox_40}            //tr[40]/td[@class='bill-oper-color tu']//span[1]


# --------------------------------------------------------------

${checkbox_1_TVG}           //tr[1]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_2_TVG}           //tr[2]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_3_TVG}           //tr[3]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_4_TVG}           //tr[4]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_5_TVG}           //tr[5]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_6_TVG}           //tr[6]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_7_TVG}           //tr[7]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_8_TVG}           //tr[8]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_9_TVG}           //tr[9]/td[@class='bill-oper-color tvg']//span[1]
${checkbox_10_TVG}          //tr[10]/td[@class='bill-oper-color tvg']//span[1]




${checkbox_1_RMV}           //tr[1]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_2_RMV}           //tr[2]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_3_RMV}           //tr[3]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_4_RMV}           //tr[4]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_5_RMV}           //tr[5]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_6_RMV}           //tr[6]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_7_RMV}           //tr[7]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_8_RMV}           //tr[8]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_9_RMV}           //tr[9]/td[@class='bill-oper-color rmv']//span[1]
${checkbox_10_RMV}          //tr[10]/td[@class='bill-oper-color rmv']//span[1]


${checkbox_1_TUC}           //tr[1]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_2_TUC}           //tr[2]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_3_TUC}           //tr[3]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_4_TUC}           //tr[4]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_5_TUC}           //tr[5]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_6_TUC}           //tr[6]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_7_TUC}           //tr[7]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_8_TUC}           //tr[8]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_9_TUC}           //tr[9]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_10_TUC}          //tr[10]/td[@class='bill-oper-color tuc']//span[1]


${checkbox_10_TUC}          //tr10]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_11_TUC}          //tr[11]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_12_TUC}          //tr[12]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_13_TUC}          //tr[13]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_14_TUC}          //tr[14]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_15_TUC}          //tr[15]/td[@class='bill-oper-color tuc']//span[1]

${checkbox_1_TUC1}           //div[@class='explorer-panel']/div[2]//tr[1]/td[@class='bill-oper-color tuc']//span[1]
${checkbox_2_TUC1}           //div[@class='explorer-panel']/div[2]//tr[2]/td[@class='bill-oper-color tuc']//span[1]



${total_amount}         css=input[class="ipt-text"]
${fill_input_amount}    //input[@value="0.00"]     
${print_button}         xpath=//*[@id="btnSubmit"]
${receiptnumber_system}     css=#app > div > div.box-alert-message.SUCCESS > div > div.message-tech

${pic_acrobat}              C:\\TRM\\sikulipicture\\acrobat.png
${pic_close acrobat}        C:\\TRM\\sikulipicture\\close acrobat.png 
${pic_savebutton}           C:\\TRM\\sikulipicture\\save.png 
${pic_more_button}          C:\\TRM\\sikulipicture\\more_button.png
${not_recom_button}         C:\\TRM\\sikulipicture\\not_recom_button.png
${expand_button}            C:\\TRM\\sikulipicture\\expand_button.png
${melonban}                 C:\\TRM\\sikulipicture\\melonban.png


${default_receipt}          C:\\TRM\\Receipt\\1.pdf 
# ${editcashbill}             //tr[1]//input[@class='text-input input-bill-amount-request text-right']
# ${editcashbill2}            //tr[2]//input[@class='text-input input-bill-amount-request text-right']
${editcashbill}              //table[@class='table bill-list react-draggable']/tr[1]/td[@class='bill-input text-right']//input[@class='text-input input-bill-amount-request text-right']
${editcashbill2}             //table[@class='table bill-list react-draggable']/tr[2]/td[@class='bill-input text-right']//input[@class='text-input input-bill-amount-request text-right']
${change_cash_locator}       //div[@class='D-6 text-right text-label']



# ----------------------------------------------------------------------------------
# # //withholdingtax

${taxcheckbox_billpayment}      css=#app > div > div:nth-child(2) > div > div.bill-container > div:nth-child(1) > div.row > div.D-4.right-padding-reset > div > div > div.transaction-panel.animated.metroInRight > div.transaction-card > div.transaction-body > div > div.section-footer.props-tax-doc > div.tax-document > div.row > div.D-1 > div
${taxonepercent}    css=#app > div > div:nth-child(2) > div > div.bill-container > div:nth-child(1) > div.row > div.D-4.right-padding-reset > div > div > div.transaction-panel.animated.metroInRight > div.transaction-card > div.transaction-body > div > div.section-footer.props-tax-doc > div.tax-document > div.tax-description > div:nth-child(1) > div > div.D-11.tax-rdo-area > div > div:nth-child(1) > a > i
${checkcompany}     css=#app > div > div:nth-child(2) > div > div.bill-container > div:nth-child(1) > div.row > div.D-4.right-padding-reset > div > div > div.transaction-panel.animated.metroInRight > div.transaction-card > div.transaction-body > div > div.section-footer.props-tax-doc > div.tax-document > div.tax-description > div.row > div > table > tbody > tr > td:nth-child(1) > div
${companynumber}    css=#app > div > div:nth-child(2) > div > div.bill-container > div:nth-child(1) > div.row > div.D-4.right-padding-reset > div > div > div.transaction-panel.animated.metroInRight > div.transaction-card > div.transaction-body > div > div.section-footer.props-tax-doc > div.tax-document > div.tax-description > div.row > div > table > tbody > tr > td:nth-child(3) > div > div.form-group-textinput > input
${gettextwithholdingtax}    css=div.calculate-footer .D-4
${gettexttotalonlytax}             //div[@class='inside-gray sumary-gray']//div[@class='D-4 text-right']



# ----------------------------------------------------------------------------------
# # //Pay Credit Card
${BankName}                 css=input.form-input.disabled

${creditcard_option}        css=div.tab-tax > label:nth-of-type(2)
${creditcard_button}        css=.btn-edc
${close_popup_creditcard}   css=a.btn-close-alert > .fa
${input_creditnumber}       css=input[maxlength='16']
${input_secretnumber}       css=div.form-content .D-4 .form-input




${edit_customer_button}     css=.button-edit-customer 

# ${DB_CONNECT_STRING}     'trmowner/freewill@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=172.19.192.114)(PORT=1553))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=TRMUAT)))' utf8 

${corporate_button}              //span[@class='checkbox-ex-panel input-checkbox']/span[1]


*** Variables ***

${URL}                                      http://tsm-uat.true.th:18080/TSM-Lite/index2.zul
${findproduct_and_service_button}           //button[.='เลือกสินค้าและบริการ']
${find_button}                              //button[.='ค้นหา']
${product_1}                                //div[@class='modal-body']//div[@class='rt-tbody']/div[1]//input[@class='checkbox']
${addproduct}                               //button[.='เพิ่มสินค้า']
${ref_no1}                                  //div[@class='w-50 p-0']/input[@class='w-100']
${ref_no2}                                  //div[@class='w-50 p-0 ml-1']/input[@class='w-100']
${amount}                                   id=netAmount
${cash}                                     id=cash
${credit_tab}                               id=react-tabs-2
${add_credit_card}                          //button[.='เพิ่มบัตรเครดิต']
${cardnumber}                               id=creditcardNo
${approvenumber}                            id=approvalCode
${print_receipt}                            //button[.='พิมพ์ใบเสร็จ']
${select_AdobePDF}                          //input[@value='Adobe PDF']
${confirm}                                  //button[.='ตกลง']




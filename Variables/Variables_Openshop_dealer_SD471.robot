*** Variables ***

# --------------------------------------------------------------------------------------------------------------------------
${URL_Shop_Dealer}      http://trm-dealercash-web-payment-dev.apps.true.th/login?goto=http://trm-dealercash-web-payment-dev.apps.true.th/
${User_Manager}         anyarat103_mas
${Password_Manager}     trmtrm123
${User_Cashier}         kornthip_tha    
${Password_Cashier}     trmtrm123


${Verify_send_cash}     //button[.='ตรวจสอบการนำส่งเงิน']
${Cash}                 //*[@id="root"]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/div[5]/div[6]/span/div/div/input
${Credit_card}          //*[@id="root"]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/div[6]/div[6]/span/div/div/input
${Cheque}               //*[@id="root"]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/div[7]/div[6]/span/div/div/input
${Voucher}              //*[@id="root"]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/div[8]/div[6]/span/div/div/input                          
${Tax}                  //*[@id="root"]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/div[9]/div[6]/span/div/div/input
${Ohter}                //*[@id="root"]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div/div[10]/div[6]/span/div/div/input


${Back_Button}          //button[@class='button is-dark']
${Save_Button}          //span[.='บันทึก']
${OK_Button}            //button[.='Ok']
${closeshift_Button}    //a[.='ปิดกะ']
${Reason_Closeshop_crossday}        //option[@value='SN1']
${Reason_Closeshop_Sameday}         //option[@value='SB1']
${Change_cash}                      //input[@id='amount']
${Save_Change_cash}                 //button[@class='button is-primary']
*** Variables ***

${URL}                          http://tsm-uat.true.th:18080/TSM-Lite/index2.zul
${User_Ratanasuda}              //div[@class='rt-tr -odd']//button[@class='main__button-styles-view___1Oqq2']
${User_Jittaporn}               //div[@class='rt-tr -even']//button[@class='main__button-styles-view___1Oqq2']
${onebaht}                      id=REF03
${cash_fromsystem}              css=div.rt-tbody > div:nth-of-type(3) div:nth-of-type(2) > .d-flex

${credit_TDS_fromsystem}            css=div.rt-tbody > div:nth-of-type(8) div:nth-of-type(2) > .d-flex
${credit_TDS_quantity}              css=div.rt-tbody > div:nth-of-type(8) div:nth-of-type(6) > .d-flex


${credit_TMN_fromsystem}            css=div.rt-tbody > div:nth-of-type(7) div:nth-of-type(2) > .d-flex
${credit_TMN_quantity}              css=div.rt-tbody > div:nth-of-type(7) div:nth-of-type(6) > .d-flex



${verify}                   //button[contains(.,'ตรวจสอบ')]
${back_button}              //button[contains(.,'Back')]

${credit_confirm_tab}       id=accordion__title-5

${TDS_quantity}             id=REF16amount
${TDS_amount}               id=REF16total_cash_row

${TMN_quantity}             id=REF15amount
${TMN_amount}               id=REF15total_cash_row


${confirm_send}             //button[contains(.,'ยืนยันการนำส่ง')]
${ok}                       //button[contains(.,'ตกลง')]
${Reconcile_button}         //button[contains(.,'Reconcile')]

${cash_fromsystem_endday}              css=div.rt-tbody > div:nth-of-type(2) div:nth-of-type(2) > .d-flex

${credit_TDS_fromsystem_endday}        css=div.rt-tbody > div:nth-of-type(7) div:nth-of-type(2) > .d-flex
${credit_TDS_quantity_endday}          css=div.rt-tbody > div:nth-of-type(7) div:nth-of-type(6) > .d-flex

${credit_TMN_fromsystem_endday}        css=div.rt-tbody > div:nth-of-type(6) div:nth-of-type(2) > .d-flex
${credit_TMN_quantity_endday}          css=div.rt-tbody > div:nth-of-type(6) div:nth-of-type(6) > .d-flex